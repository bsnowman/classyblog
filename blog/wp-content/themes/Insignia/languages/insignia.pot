msgid ""
msgstr ""
"Project-Id-Version: Insignia\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2012-02-03 01:02+0200\n"
"PO-Revision-Date: 2012-02-03 01:02+0200\n"
"Last-Translator: Dorian <alliase.union@gmail.com>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-KeywordsList: _;gettext;gettext_noop;_e;__\n"
"X-Poedit-Basepath: .\n"
"X-Poedit-SearchPath-0: C:\\Users\\Dorian\\Desktop\\insi_user_\n"

#: C:\Users\Dorian\Desktop\insi_user_/comments.php:9
msgid "Please do not load this page directly."
msgstr ""

#: C:\Users\Dorian\Desktop\insi_user_/comments.php:16
msgid "Password Protected"
msgstr ""

#: C:\Users\Dorian\Desktop\insi_user_/comments.php:19
msgid "Enter the password to view comments."
msgstr ""

#: C:\Users\Dorian\Desktop\insi_user_/framework.php:514
#: C:\Users\Dorian\Desktop\insi_user_/framework.php:525
msgid "Read More"
msgstr ""

#: C:\Users\Dorian\Desktop\insi_user_/framework.php:601
msgid "in&nbsp;"
msgstr ""

#: C:\Users\Dorian\Desktop\insi_user_/framework.php:629
msgid "<em>Tags:</em>&nbsp;"
msgstr ""

#: C:\Users\Dorian\Desktop\insi_user_/framework.php:632
msgid "<em>Categories:</em>&nbsp;"
msgstr ""

#: C:\Users\Dorian\Desktop\insi_user_/framework.php:729
#, php-format
msgid "%1$s"
msgstr ""

#: C:\Users\Dorian\Desktop\insi_user_/framework.php:735
msgid "Your comment is awaiting moderation."
msgstr ""

#: C:\Users\Dorian\Desktop\insi_user_/framework.php:742
#: C:\Users\Dorian\Desktop\insi_user_/page.php:29
#: C:\Users\Dorian\Desktop\insi_user_/single-portfolio.php:27
#: C:\Users\Dorian\Desktop\insi_user_/single.php:27
msgid "Edit"
msgstr ""

#: C:\Users\Dorian\Desktop\insi_user_/page.php:28
#: C:\Users\Dorian\Desktop\insi_user_/single-portfolio.php:26
#: C:\Users\Dorian\Desktop\insi_user_/single.php:26
msgid "Pages:"
msgstr ""

#: C:\Users\Dorian\Desktop\insi_user_/searchform.php:7
msgid "Search"
msgstr ""

#: C:\Users\Dorian\Desktop\insi_user_/lib/classes/contact.php:32
#, php-format
msgid "Form Field %1$s"
msgstr ""

#: C:\Users\Dorian\Desktop\insi_user_/lib/classes/contact.php:33
msgid "Your Website"
msgstr ""

#: C:\Users\Dorian\Desktop\insi_user_/lib/classes/contact.php:34
#, php-format
msgid "Message sent from your %1$s website"
msgstr ""

#: C:\Users\Dorian\Desktop\insi_user_/lib/classes/contact.php:35
#, php-format
msgid "%1$s Auto Responder"
msgstr ""

#: C:\Users\Dorian\Desktop\insi_user_/lib/classes/contact.php:36
msgid "Your captcha result is wrong."
msgstr ""

#: C:\Users\Dorian\Desktop\insi_user_/lib/classes/contact.php:37
msgid "You have entered an invalid e-mail address."
msgstr ""

#: C:\Users\Dorian\Desktop\insi_user_/lib/classes/contact.php:38
#, php-format
msgid "&rdquo;%1$s&rdquo; is required."
msgstr ""

#: C:\Users\Dorian\Desktop\insi_user_/lib/classes/contact.php:39
msgid "Failed to send your message. Please try later or contact administrator by other way."
msgstr ""

#: C:\Users\Dorian\Desktop\insi_user_/lib/classes/contact.php:40
msgid "Your message was sent successfully. Thanks."
msgstr ""

#: C:\Users\Dorian\Desktop\insi_user_/lib/classes/tabs_widget.php:41
msgid "Popular"
msgstr ""

#: C:\Users\Dorian\Desktop\insi_user_/lib/classes/tabs_widget.php:42
msgid "Comments"
msgstr ""

#: C:\Users\Dorian\Desktop\insi_user_/lib/classes/tabs_widget.php:43
msgid "Tags"
msgstr ""

#: C:\Users\Dorian\Desktop\insi_user_/lib/classes/tabs_widget.php:87
msgid "says"
msgstr ""

#: C:\Users\Dorian\Desktop\insi_user_/lib/classes/widget-contact-form.php:12
msgid "Quickly add a contact form to your sidebar"
msgstr ""

#: C:\Users\Dorian\Desktop\insi_user_/lib/classes/widget-contact-form.php:14
#, php-format
msgid "%1$s - Contact Form"
msgstr ""

#: C:\Users\Dorian\Desktop\insi_user_/lib/classes/widget-contact-form.php:23
#: C:\Users\Dorian\Desktop\insi_user_/lib/classes/widget-contact.php:22
msgid "Contact Info"
msgstr ""

#: C:\Users\Dorian\Desktop\insi_user_/lib/classes/widget-contact-form.php:65
#: C:\Users\Dorian\Desktop\insi_user_/lib/classes/widget-contact.php:84
#: C:\Users\Dorian\Desktop\insi_user_/lib/classes/widget-flickr.php:71
#: C:\Users\Dorian\Desktop\insi_user_/lib/classes/widget-popular.php:118
#: C:\Users\Dorian\Desktop\insi_user_/lib/classes/widget-recent.php:118
#: C:\Users\Dorian\Desktop\insi_user_/lib/classes/widget-subnav.php:61
#: C:\Users\Dorian\Desktop\insi_user_/lib/classes/widget-twitter.php:71
msgid "Title:"
msgstr ""

#: C:\Users\Dorian\Desktop\insi_user_/lib/classes/widget-contact-form.php:68
#: C:\Users\Dorian\Desktop\insi_user_/lib/classes/widget-contact.php:105
msgid "Email:"
msgstr ""

#: C:\Users\Dorian\Desktop\insi_user_/lib/classes/widget-contact-form.php:72
msgid "Enable Captcha?"
msgstr ""

#: C:\Users\Dorian\Desktop\insi_user_/lib/classes/widget-contact-form.php:75
msgid "Enable Akismet?"
msgstr ""

#: C:\Users\Dorian\Desktop\insi_user_/lib/classes/widget-contact.php:12
msgid "Quickly add contact info to your sidebar (e.g. address, phone #, email)"
msgstr ""

#: C:\Users\Dorian\Desktop\insi_user_/lib/classes/widget-contact.php:14
#, php-format
msgid "%1$s - Contact Us"
msgstr ""

#: C:\Users\Dorian\Desktop\insi_user_/lib/classes/widget-contact.php:87
msgid "Name:"
msgstr ""

#: C:\Users\Dorian\Desktop\insi_user_/lib/classes/widget-contact.php:90
msgid "Address:"
msgstr ""

#: C:\Users\Dorian\Desktop\insi_user_/lib/classes/widget-contact.php:93
msgid "City:"
msgstr ""

#: C:\Users\Dorian\Desktop\insi_user_/lib/classes/widget-contact.php:96
msgid "State:"
msgstr ""

#: C:\Users\Dorian\Desktop\insi_user_/lib/classes/widget-contact.php:99
msgid "Zip:"
msgstr ""

#: C:\Users\Dorian\Desktop\insi_user_/lib/classes/widget-contact.php:102
msgid "Phone:"
msgstr ""

#: C:\Users\Dorian\Desktop\insi_user_/lib/classes/widget-flickr.php:12
msgid "Pulls in images from your Flickr account"
msgstr ""

#: C:\Users\Dorian\Desktop\insi_user_/lib/classes/widget-flickr.php:14
#, php-format
msgid "%1$s - Flickr"
msgstr ""

#: C:\Users\Dorian\Desktop\insi_user_/lib/classes/widget-flickr.php:22
msgid "Photos on flickr"
msgstr ""

#: C:\Users\Dorian\Desktop\insi_user_/lib/classes/widget-flickr.php:74
msgid "Flickr ID (<a href=\"http://www.idgettr.com\" target=\"_blank\">idGettr</a>):"
msgstr ""

#: C:\Users\Dorian\Desktop\insi_user_/lib/classes/widget-flickr.php:77
msgid "Number of photos:"
msgstr ""

#: C:\Users\Dorian\Desktop\insi_user_/lib/classes/widget-flickr.php:80
msgid "Display:"
msgstr ""

#: C:\Users\Dorian\Desktop\insi_user_/lib/classes/widget-flickr.php:87
msgid "Size:"
msgstr ""

#: C:\Users\Dorian\Desktop\insi_user_/lib/classes/widget-flickr.php:89
msgid "Square"
msgstr ""

#: C:\Users\Dorian\Desktop\insi_user_/lib/classes/widget-flickr.php:90
msgid "Thumbnail"
msgstr ""

#: C:\Users\Dorian\Desktop\insi_user_/lib/classes/widget-flickr.php:91
msgid "Medium"
msgstr ""

#: C:\Users\Dorian\Desktop\insi_user_/lib/classes/widget-popular.php:12
msgid "Custom popular post widget with post preview image"
msgstr ""

#: C:\Users\Dorian\Desktop\insi_user_/lib/classes/widget-popular.php:14
#, php-format
msgid "%1$s - Popular Post"
msgstr ""

#: C:\Users\Dorian\Desktop\insi_user_/lib/classes/widget-popular.php:26
msgid "Popular Posts"
msgstr ""

#: C:\Users\Dorian\Desktop\insi_user_/lib/classes/widget-popular.php:121
msgid "Enter the number of popular posts you'd like to display:"
msgstr ""

#: C:\Users\Dorian\Desktop\insi_user_/lib/classes/widget-popular.php:125
#: C:\Users\Dorian\Desktop\insi_user_/lib/classes/widget-recent.php:125
msgid "Disable Post Thumbnail?"
msgstr ""

#: C:\Users\Dorian\Desktop\insi_user_/lib/classes/widget-recent.php:12
msgid "Custom recent post widget with post preview image"
msgstr ""

#: C:\Users\Dorian\Desktop\insi_user_/lib/classes/widget-recent.php:14
#, php-format
msgid "%1$s - Recent Post"
msgstr ""

#: C:\Users\Dorian\Desktop\insi_user_/lib/classes/widget-recent.php:26
msgid "Recent Posts"
msgstr ""

#: C:\Users\Dorian\Desktop\insi_user_/lib/classes/widget-recent.php:121
msgid "Enter the number of recent posts you'd like to display:"
msgstr ""

#: C:\Users\Dorian\Desktop\insi_user_/lib/classes/widget-subnav.php:12
msgid "Displays a list of SubPages"
msgstr ""

#: C:\Users\Dorian\Desktop\insi_user_/lib/classes/widget-subnav.php:14
#, php-format
msgid "%1$s - Sub Navigation"
msgstr ""

#: C:\Users\Dorian\Desktop\insi_user_/lib/classes/widget-twitter.php:12
msgid "Pulls in your most recent tweet from Twitter"
msgstr ""

#: C:\Users\Dorian\Desktop\insi_user_/lib/classes/widget-twitter.php:14
#, php-format
msgid "%1$s - Twitter"
msgstr ""

#: C:\Users\Dorian\Desktop\insi_user_/lib/classes/widget-twitter.php:25
msgid "Recent Tweets"
msgstr ""

#: C:\Users\Dorian\Desktop\insi_user_/lib/classes/widget-twitter.php:74
msgid "Enter your twitter username:"
msgstr ""

#: C:\Users\Dorian\Desktop\insi_user_/lib/classes/widget-twitter.php:77
msgid "Enter the number of tweets you'd like to display:"
msgstr ""

#: C:\Users\Dorian\Desktop\insi_user_/lib/extensions/breadcrumbs-plus/breadcrumbs-plus.php:54
msgid "Home"
msgstr ""

#: C:\Users\Dorian\Desktop\insi_user_/lib/extensions/breadcrumbs-plus/breadcrumbs-plus.php:168
#: C:\Users\Dorian\Desktop\insi_user_/lib/extensions/breadcrumbs-plus/breadcrumbs-plus.php:171
#: C:\Users\Dorian\Desktop\insi_user_/lib/extensions/breadcrumbs-plus/breadcrumbs-plus.php:174
msgid "Archives for "
msgstr ""

#: C:\Users\Dorian\Desktop\insi_user_/lib/extensions/breadcrumbs-plus/breadcrumbs-plus.php:178
msgid "Archives by: "
msgstr ""

#: C:\Users\Dorian\Desktop\insi_user_/lib/extensions/breadcrumbs-plus/breadcrumbs-plus.php:183
msgid "Search results for \""
msgstr ""

#: C:\Users\Dorian\Desktop\insi_user_/lib/extensions/breadcrumbs-plus/breadcrumbs-plus.php:187
msgid "Page Not Found"
msgstr ""

#: C:\Users\Dorian\Desktop\insi_user_/lib/functions/twitter.php:47
msgid "Twitter not configured."
msgstr ""

#: C:\Users\Dorian\Desktop\insi_user_/lib/functions/twitter.php:82
#, php-format
msgid "<small> (%1$s&nbsp;ago)</small>"
msgstr ""

