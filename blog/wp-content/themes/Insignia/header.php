<?php
/**
 * Header Template
 *
 * @package Alliase
 * @subpackage Template
 */
?><!DOCTYPE html>
<!--[if !IE]><!--> <html <?php language_attributes(); ?>> <!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />

	<title><?php alli_document_title(); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
    <link rel="stylesheet" type="text/css" href="http://21f90d9e9860092b65d5-ce647a8b55ca793d6ab67ea8f274b2b0.r20.cf2.rackcdn.com/blog/wp-content/themes/Insignia/styles/blog-font-styles.css" media="screen">

	<?php alli_head(); ?>
	<?php wp_head(); ?>
	
	<?php


?>

<meta property="fb:app_id" content="346591576229" >
<meta property="og:title" content="<?php the_title(); ?>"/>
<meta property="og:description" content="<?php echo get_pagedesc(); ?>"/>
<meta property="og:url" content="<?php the_permalink(); ?>"/>
<meta property="og:image" content="<?php echo get_fbimage(); ?>"/>
<meta property="og:type" content="<?php
  if (is_single() || is_page()) { echo "article"; } else { echo "website";}
?>"/>
<meta property="og:site_name" content="StayClassy.org"/>

<?php 
function get_fbimage() {
	$src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), '', '' );
	if ( has_post_thumbnail($post->ID) ) {
		$fbimage = $src[0];
	} else {
		global $post, $posts;
		$fbimage = '';
		$output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i',
				$post->post_content, $matches);
		$fbimage = $matches [1] [0];
	}
	if(empty($fbimage)) {
		$fbimage = "/stories/wp-content/themes/Frailespatique/images/sc-icon.png";
	}
	return $fbimage;
}
function get_pagedesc() {
	if(is_home()) {
		return bloginfo('description');
	}
	else
	{
		global $post, $posts;
		if($post->post_excerpt<>"")
		{
			return $post->post_excerpt;
		}
		else
		{
			return substr(str_replace(array("\n","\r"), "", strip_tags($post->post_content)), 0, 200);
		}
	}
}
?>


	<!-- meta tags -->
	<script type='text/javascript' language='javascript'>/* <![CDATA[ */
   HubSpotFormSpamCheck_LeadGen_ContactForm_141744_m0 = function() {
       var key = document.getElementById('LeadGen_ContactForm_141744_m0spam_check_key').value;
	   var sig = '';
	   for (var x = 0; x< key.length; x++ ) {
				sig += key.charCodeAt(x)+13;
	   }
	   document.getElementById('LeadGen_ContactForm_141744_m0spam_check_sig').value = sig; 
       /* Set the hidden field to contain the user token */
       var results = document.cookie.match ( '(^|;) ?hubspotutk=([^;]*)(;|$)' );
        if (results && results[2]) {
            document.getElementById('LeadGen_ContactForm_141744_m0submitter_user_token').value =  results[2];
        } else if (window['hsut']) {
            document.getElementById('LeadGen_ContactForm_141744_m0submitter_user_token').value = window['hsut'];
	    }
        return true;
   };
/*]]>*/</script>

	<meta name="description" content="<?php echo get_pagedesc(); ?>" />
</head>

<body class="<?php alli_body_class(); ?>">

<div id="masthead">
	<div id="masthead_inner">
        <a href="/blog/" id="logo_link"><h1>StayClassy Blog</h1></a>
        <a href="http://www.stayclassy.org" target="_blank" id="sc">StayClassy.org &#8594;</a>
		<h2 id="fundraising_reimagined">Advancing <span>Philanthropy</span></h2>
    </div>
</div>

<div id="top_nav">
    <div id="body_inner">
    
        <?php alli_after_header();
        
        ?>
     </div>  
</div>
		<div id="body_inner">

<div id="content">
                    <div id="content_inner">
                        
                        <?php alli_before_main();
                        
                        ?><div id="main">
                            <div id="main_inner">
                                <?php alli_before_page_content(); ?>
                                            <!-- Google Code for  Landed on Blog Remarketing List -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1001342251;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "JYWHCPXvjAMQq4q93QM";
var google_conversion_value = 0;
/* ]]> */
</script>
<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="http://www.googleadservices.com/pagead/conversion/1001342251/?value=0&amp;label=JYWHCPXvjAMQq4q93QM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
