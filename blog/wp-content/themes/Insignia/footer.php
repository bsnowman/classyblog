<?php
/**
 * Footer Template
 *
 * @package Alliase
 * @subpackage Template
 */
?>

<?php alli_after_main();

?><div class="clearboth"></div>

	</div><!-- #content_inner -->
</div><!-- #content -->

<?php alli_before_footer();

?>

</div><!-- #body_inner -->

<div id="footer_wrapper">
    <div id="footer" class="container">
         <div id="footer_left">
            <div id="what_is">
                <span>What is</span>
                <span class="classy-awards">StayClassy?</span>
            </div>
             <p id="tag">Over 2,000 Nonprofits use our software to make their lives easier. How can we help your organization?</p> 
            <div id="sc_icons">
				<a href="http://www.stayclassy.org/charity/organize-fundraising-events" class="icon_sxn">
					<img src="http://www.stayclassy.org/images/mail/footIcon1.png" border="0"/>
					<div class="info">
                        <strong>Event Management.</strong>
                        <span>Organize any type of event.</span>
					</div>
				</a>
				<a href="http://www.stayclassy.org/charity/organize-fundraising-events" class="icon_sxn">
					<img src="http://www.stayclassy.org/images/mail/footIcon2.png" border="0"/>
					<div class="info">
                        <strong>Online Donations.</strong>
                        <span>Accept donations on your site.</span>
					</div>
				</a>
				<a href="http://www.stayclassy.org/charity/organize-fundraising-events" class="icon_sxn">
					<img src="http://www.stayclassy.org/images/mail/footIcon3.png" border="0"/>
					<div class="info">
                        <strong>Fundraising Campaigns.</strong>
                        <span>Get your supporters fundraising.</span>
					</div>
				</a>
				<a href="http://www.stayclassy.org/charity/organize-fundraising-events" class="icon_sxn">
					<img src="http://www.stayclassy.org/images/mail/footIcon4.png" border="0"/>
					<div class="info">
                        <strong>Reporting &amp; Analytics.</strong>
                        <span>Manage everything in one place.</span>
					</div>
				</a>
			</div>

<div id="classy_award">
					<span>Home of the Classy Awards</span>
					
					<span class="award-show">The Largest Philanthropic Awards Show in the United States</span>
					<ul class="award-info container">
						<li class="about-awards"><a href="http://classyawards.stayclassy.org/classy-awards" target="_blank"><span>&nbsp;</span>About the Awards</a></li>
						<li class="view-nominations"><a href="http://stayclassy.org/classy-awards/winners-all" target="_blank"><span>&nbsp;</span>2011 Winners				</a></li>
					</ul>
				</div>

		

			
        </div>        
        
        
        <div id="footer_right">
            <?php alli_footer();
		
		?>
		
		  <div id="footer_utilities">
            
                <span>&#169;2012 StayClassy, All Rights Reserved.</span>
                <ul>
                    <li><a href="http://www.stayclassy.org/terms" target="_blank">Terms &amp; Conditions</a></li>
                    <li><a href="/privacy" target="_blank">Privacy Policy</a></li>
                    <li><a href="http://www.stayclassy.org/copyright" target="_blank">Copyright Policy</a></li>
                </ul>
            </div>
        </div>
        
    </div>
</div>

<?php alli_after_footer();

?>

<?php wp_footer(); ?>
<?php alli_body_end(); ?>

<!-- Start of Async HubSpot Analytics Code -->
    <script type="text/javascript">
        (function(d,s,i,r) {
            if (d.getElementById(i)){return;}
            var n=d.createElement(s),e=d.getElementsByTagName(s)[0];
            n.id=i;n.src='//js.hubspot.com/analytics/'+(Math.ceil(new Date()/r)*r)+'/190333.js';
            e.parentNode.insertBefore(n, e);
        })(document,"script","hs-analytics",300000);
    </script>
<!-- End of Async HubSpot Analytics Code -->

</body>
</html>
