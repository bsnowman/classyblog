<?php
/**
 *
 */
function alli_head() {
	do_atomic( 'head' );
}

/**
 *
 */
function alli_before_header() {
	do_atomic( 'before_header' );
}

/**
 *
 */
function alli_header() {
	do_atomic( 'header' );
}

/**
 *
 */
function alli_after_header() {
	do_atomic( 'after_header' );
}

/**
 *
 */
function alli_before_main() {
	do_atomic( 'before_main' );
}

/**
 *
 */
function alli_main_menu_begin() {
	do_atomic( 'main_menu_begin' );
}

/**
 *
 */
function alli_main_menu_end() {
	do_atomic( 'main_menu_end' );
}

/**
 *
 */
function alli_intro_begin() {
	do_atomic( 'intro_begin' );
}

/**
 *
 */
function alli_intro_end() {
	do_atomic( 'intro_end' );
}

/**
 *
 */
function alli_before_page_content() {
	do_atomic( 'before_page_content' );
}

/**
 *
 */
function alli_post_image_begin() {
	do_atomic( 'post_image_begin' );
}

/**
 *
 */
function alli_post_image_end( $args = array()  ) {
	do_atomic( 'post_image_end', $args );
}

/**
 *
 */
function alli_portfolio_image_begin() {
	do_atomic( 'portfolio_image_begin' );
}

/**
 *
 */
function alli_portfolio_image_end( $args = array() ) {
	do_atomic( 'portfolio_image_end', $args );
}

/**
 *
 */
function alli_before_portfolio_image() {
	do_atomic( 'before_portfolio_image' );
}

/**
 *
 */
function alli_after_portfolio_image() {
	do_atomic( 'after_portfolio_image' );
}

/**
 *
 */
function alli_before_post( $args = array() ) {
	do_atomic( 'before_post', $args );
}

/**
 *
 */
function alli_after_post() {
	do_atomic( 'after_post' );
}

/**
 *
 */
function alli_after_comments() {
	do_atomic( 'after_comments' );
}

/**
 *
 */
function alli_before_entry() {
	do_atomic( 'before_entry' );
}

/**
 *
 */
function alli_after_entry() {
	do_atomic( 'after_entry' );
}

/**
 *
 */
function alli_after_page_content() {
	do_atomic( 'after_page_content' );
}

/**
 *
 */
function alli_sidebar_begin() {
	do_atomic( 'sidebar_begin' );
}

/**
 *
 */
function alli_sidebar_end() {
	do_atomic( 'sidebar_end' );
}

/**
 *
 */
function alli_after_main() {
	do_atomic( 'after_main' );
}

/**
 *
 */
function alli_before_footer() {
	do_atomic( 'before_footer' );
}

/**
 *
 */
function alli_footer() {
	do_atomic( 'footer' );
}

/**
 *
 */
function alli_after_footer() {
	do_atomic( 'after_footer' );
}

/**
 *
 */
function alli_body_end() {
	do_atomic( 'body_end' );
}

?>
