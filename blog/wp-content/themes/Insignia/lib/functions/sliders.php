<?php

if ( !function_exists( 'alli_slider_module' ) ) :
/**
 *
 */
function alli_slider_module() {
	
	$slider_page = apply_filters( 'alli_slider_page', alli_get_setting( 'slider_page' ) );
	if( ( !is_front_page() && !$slider_page ) || ( is_front_page() && alli_get_setting( 'home_slider_disable' ) != false ) )
		return;
	
	$out = '';
	$slider = '';
	
	$slider_type = apply_filters( 'alli_slider_type', alli_get_setting( 'homepage_slider' ) );
	
	$slider_custom = alli_get_setting( 'slider_custom' );
	
	if( $slider_custom == 'categories' )
		$slider_setting = alli_category_slider();
	else
		$slider_setting = alli_get_setting( 'slideshow' );
		
	$slider_setting = apply_filters('alli_slider_settings', $slider_setting );
	
	if( $slider_type == 'fading_slider' )
		$slider = alli_fading( $slider_type, $slider_setting );
		
	elseif( $slider_type == 'scrolling_slider' )
		$slider = alli_scrolling( $slider_type, $slider_setting );
		
	elseif( $slider_type == 'nivo_slider' )
		$slider = alli_nivo( $slider_type, $slider_setting );
		
	if( !empty( $slider ) ) {
		$out .= '<div id="slider_module">';
		$out .= '<div id="slider_module_inner">';
		
		$out .= $slider;
		
		$out .= '<div class="clearboth"></div>';
		$out .= '</div>';
		$out .= '</div>';
	}
	
	echo apply_atomic( 'slider', $out );
}
endif;

if ( !function_exists( 'alli_category_slider' ) ) :
/**
 *
 */
function alli_category_slider() {
	global $post, $wpdb;
	$slider_settings = array();
	$counter = 0;
	
	$slider_count = 4;
	$slider_cats = alli_get_setting( 'slider_cats' );
	$slider_cat_count = alli_get_setting( 'slider_cat_count' );
	if ($slider_cats) $slider_showcats = join( ',', $slider_cats );
	$slider_keys = array();
	
	$slider_count = ( is_numeric( $slider_cat_count ) ) ? $slider_cat_count : 5;
	$cat_slider_query = new WP_Query("cat={$slider_showcats}&showposts={$slider_count}");
	
	if( $cat_slider_query->have_posts() ) : while( $cat_slider_query->have_posts() ) : $cat_slider_query->the_post();
		
	$_homepage_image = get_post_meta( $post->ID, '_homepage_image', true );
	$_homepage_slider_stage = get_post_meta( $post->ID, '_homepage_slider_stage', true );
	$_homepage_disable_excerpt = get_post_meta( $post->ID, '_homepage_disable_excerpt', true );
	$_homepage_link = get_permalink($post->ID);

	
	if (!$_homepage_image) {
		$thumb = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
		$_homepage_image = $thumb[0];
		
	}

	//echo $_homepage_image. '<br/>' ;

	$_homepage_image = alli_wp_image($_homepage_image,720,360);

	if( $_homepage_image || $_homepage_slider_stage == 'raw_html' ) {
		$slider_settings[$counter]['slider_url'] = esc_url( $_homepage_image );
		$slider_settings[$counter]['alt_attr'] = esc_attr( get_the_title() );
		$slider_settings[$counter]['stage_effect'] =  ( !empty( $_homepage_slider_stage ) ) ? $_homepage_slider_stage : 'staged_slide';
		$slider_settings[$counter]['link_url'] = esc_url( get_permalink() );
		$slider_keys = array_merge( $slider_keys, array( (int)$counter ) );
		
		if( !$_homepage_disable_excerpt && $_homepage_slider_stage != 'raw_html' ) {
			$slider_settings[$counter]['description'] = alli_excerpt( get_the_excerpt(), apply_filters( 'alli_cat_slider_excerpt_length', 100 ), apply_filters( 'alli_cat_slider_excerpt_ellipsis', ' ... ' ) );
			$slider_settings[$counter]['title'] = get_the_title();
			$slider_settings[$counter]['read_more'] = false;
		} else {
			$slider_settings[$counter]['read_more'] = true;
		}
		
		if( $_homepage_slider_stage == 'raw_html' )
			$slider_settings[$counter]['description'] = get_the_excerpt();
		
		$counter++;
	}
	endwhile; endif;
	
	$slider_keys = array_merge( $slider_keys, array( '#' ) );
	$slider_settings['slider_keys'] = join( ',', $slider_keys );
	
	wp_reset_query();
	
	return $slider_settings;
}
endif;


if ( !function_exists( 'alli_nivo' ) ) :
/**
 *
 */
function alli_nivo( $slider_type, $slider ) {
	global $alli;
	
	$img_sizes = $alli->layout['images_slider'];
	$slider_keys = explode(',', $slider['slider_keys']);
	
	$img = array( 'w' => $img_sizes['nivo_slide'][0], 'h' => $img_sizes['nivo_slide'][1] );
		
	$out = '<div class="alli_preloader_large">';
	$out .= '<img src="' . THEME_IMAGES_ASSETS . '/transparent.gif" style="background-image: url(' . THEME_IMAGES_ASSETS . '/preloader.png);">';
	$out .= '</div>';
	
	$out .='<div id="alli_nivo_slider">';
	$out .='<div id="nivo_slider" class="noscript">';
	
	foreach ( $slider_keys as $key ) {

		if( $key != '#' ) {
			$link_n = ( !empty( $slider[$key]['link_url'] ) ) ? $slider[$key]['link_url'] : '';

			$slide_img = esc_url( $slider[$key]['slider_url'] );

			$img_alt = !empty( $slider[$key]['alt_attr'] ) ? esc_attr( $slider[$key]['alt_attr'] ) : '';
			
			
			if( preg_match_all( '!.+\.(?:jpe?g|png|gif)!Ui', $slide_img, $matches ) ) {
				
				$title = ( !empty( $slider[$key]['title'] ) ) ? $slider[$key]['title'] : '';
				$description = ( !empty( $slider[$key]['description'] ) ) ? $slider[$key]['description'] : '';
				
				if( !empty( $title ) || !empty( $description ) ) {
					$out .= '<a href="'.$link_n.'" class="nivo-imageLink">';
					$out .= alli_display_image( array( 'src' => $slide_img, 'title' => '<h2>'.$title.'</h2>'.$description, 'alt' => $img_alt, 'height' => $img['h'], 'width' => $img['w'] ) );
					$out .= '</a>';
				} 
				else {
					$out .= '<a href="'.$link_n.'" class="nivo-imageLink">';
					$out .= alli_display_image( array( 'src' => $slide_img, 'alt' => $img_alt, 'height' => $img['h'], 'width' => $img['w'] ) );
					$out .= '</a>';
				}
				
			}
		}
	}
	
	$out .= '</div><!-- #nivo_slider -->';
	
	$out .= '</div><!-- #alli_nivo_slider -->';
	
	foreach ( $slider_keys as $key ) {
		$title = ( !empty( $slider[$key]['title'] ) ) ? stripslashes( $slider[$key]['title'] ) : '';
		$description = ( !empty( $slider[$key]['description'] ) ) ? stripslashes( $slider[$key]['description'] ) : '';
		
		if( !empty( $title ) || !empty( $description ) ) {
			$out .= '<div id="htmlcaption_' . $key . '" class="nivo-html-caption">';
			
			if( !empty( $title ) )
				$out .= $title . ' ';
				
			if( !empty( $description ) )
				$out .= $description;
			
			$out .= '</div>';
		}
	}
	
	alli_slider_script( $slider_type );
	
	return $out;
}
endif;

if ( !function_exists( 'alli_slider_script' ) ) :
/**
 *
 */
function alli_slider_script( $slider_type ) {
	
	$defaults = array(
		'slider_disable_trans' => alli_get_setting( 'slider_disable_trans' ), // true | false
		'slider_hover_pause' => alli_get_setting( 'slider_hover_pause' ), // true | false
		'slider_speed' => alli_get_setting( 'slider_speed' ), // 2000 milli
		'hover_pause' => ( !empty( $slider_hover_pause ) ? 'false' : 'true' ) // true | fasle
	);
	
	$args = wp_parse_args( apply_filters( 'alli_slider_options', '' ), $defaults );

	extract( $args );
		
	if( $slider_type == 'nivo_slider' )
		$disable_trans = ( !empty( $slider_disable_trans ) ) ? 'true' : 'false';

	else
		$disable_trans = ( !empty( $slider_disable_trans ) ) ? 'false' : 'true';
		
	if( $slider_type != 'nivo_slider' && $disable_trans == 'false' )
		$hover_pause = 'false';
		
?><script type="text/javascript">
/* <![CDATA[ */


jQuery(document).ready(function(){
	
	// YouTube video present
	if( jQuery('.youtube_video').length>0 ) {
		jQuery('.youtube_video').each(function(index, youtube_video){
			onYouTubePlayerAPIReady(youtube_video.id);
		});
	}
	// Vimeo video present
	if( jQuery('.vimeo_video').length>0 ) {
	  	jQuery('.vimeo_video').each(function(index, vimeo_video){
			Froogaloop.init([vimeo_video]);
            vimeo_video.addEvent('onLoad', VimeoEmbed.vimeo_player_loaded);
        });
	}
	
<?php
switch ( $slider_type ) {
	
    case 'nivo_slider': ?>

	<?php
	$get_nivo_effect = alli_get_setting( 'nivo_effect' );
	$get_nivo_slices = alli_get_setting( 'nivo_slices' );
	$get_nivo_anim_speed = alli_get_setting( 'nivo_anim_speed' );
	$get_nivo_control_nav = alli_get_setting( 'nivo_control_nav' );
	$get_nivo_direction_nav = alli_get_setting( 'nivo_direction_nav' );
	//$get_disable_cufon = alli_get_setting( 'disable_cufon' );
	
	$defaults = array(
		'nivo_effect' => ( !empty( $get_nivo_effect ) ? $get_nivo_effect : 'sliceDown' ),
		'nivo_slices' => ( !empty( $get_nivo_slices ) ? $get_nivo_slices : '10' ),
		'nivo_anim_speed' => ( !empty( $get_nivo_anim_speed ) ? $get_nivo_anim_speed : '500' ),
		'nivo_control_nav' => ( !empty( $get_nivo_control_nav ) ? 'true' : 'false' ),
		'get_nivo_direction_nav' => $get_nivo_direction_nav
		
	);
	
	$args = wp_parse_args( apply_filters( 'alli_nivo_options', '' ), $defaults );

	extract( $args );
	
	if( $get_nivo_direction_nav == 'button' ) {
		$nivo_direction_nav = 'true';
		$nivo_direction_nav_hide = 'false';
		
	} elseif( $get_nivo_direction_nav == 'button_hover' ) {
		$nivo_direction_nav = 'true';
		$nivo_direction_nav_hide = 'true';
		
	} elseif( $get_nivo_direction_nav == 'disable') {
		$nivo_direction_nav = 'false';
		$nivo_direction_nav_hide = 'false';
	}
	?>

	jQuery('#nivo_slider').nivoSlider({
		effect:'<?php echo $nivo_effect; ?>',
		slices:<?php echo $nivo_slices; ?>,
		animSpeed:<?php echo $nivo_anim_speed; ?>, //Slide transition speed
		pauseTime:<?php echo $slider_speed; ?>,
		directionNav:<?php echo $nivo_direction_nav; ?>, //Next & Prev
		directionNavHide:<?php echo $nivo_direction_nav_hide; ?>, //Only show on hover
		controlNav:<?php echo $nivo_control_nav; ?>, //1,2,3...
		//controlNavThumbs: true,
		keyboardNav:false, //Use left & right arrows
		pauseOnHover:<?php echo $hover_pause; ?>, //Stop animation while hovering
		manualAdvance:<?php echo $disable_trans; ?>, //Force manual transitions
		customChange: function(){ <?php //echo $nivo_caption; ?> },
		afterLoad: function(){
			jQuery('#slider_module .alli_preloader_large').remove();
			jQuery('#nivo_slider').removeClass('noscript');
		}
	});
	
	<?php break; ?>
<?php } ?>

});
/* ]]> */
</script>
<?php
do_action( 'alli_after_slider_script' );
}
endif;

?>
