<?php
/**
 * The Alliase admin class.
 * The theme admin functions & classes are included & initialized from this file.
 *
 * @package Alliase
 * @subpackage Admin
 */

class alliAdmin {
	
	/**
	 * Initializes the theme admin framework by loading
	 * required files and functions for the theme options,
	 * meta boxes, skin generator, etc...
	 *
	 * @since 1.0
	 */
	function init() {
		self::functions();
		self::classes();
		self::actions();
		self::filters();
		self::metaboxes();
		self::activation();
		new alliSkinGenerator();
	}
	
	/**
	 * Loads the theme admin functions.
	 *
	 * @since 1.0
	 */
	function functions() {
		require_once( THEME_ADMIN_FUNCTIONS . '/core.php' );
		require_once( THEME_ADMIN_FUNCTIONS . '/scripts.php' );
		require_once( THEME_ADMIN_FUNCTIONS . '/media-upload.php');
	}
	
	/**
	 * Loads the theme admin classes.
	 *
	 * @since 1.0
	 */
	function classes() {
		require_once( THEME_ADMIN_CLASSES . '/option-generator.php' );
		require_once( THEME_ADMIN_CLASSES . '/metaboxes-generator.php' );
		require_once( THEME_ADMIN_CLASSES . '/shortcode-generator.php' );
		require_once( THEME_ADMIN_CLASSES . '/skin-generator.php' );
	}
	
	/**
	 * Adds the theme admin actions.
	 *
	 * @since 1.0
	 */
	function actions() {
		add_action( 'admin_init', 'alli_options_init', 1 );
		add_action( 'admin_init', 'alli_tinymce_init_size' );
		add_action( 'admin_notices', array( 'alliAdmin', 'warnings' ) );
		add_action( 'admin_menu', array( 'alliAdmin', 'menus' ) );
		add_action( 'admin_enqueue_scripts', 'alli_admin_enqueue_scripts' );
		add_action( 'admin_head-toplevel_page_alli-options', 'alli_admin_print_scripts' );
		add_action( 'admin_head-toplevel_page_alli-options', 'alli_admin_tinymce' );
		add_action( 'wp_ajax_alli_skin_upload', array( 'alliSkinGenerator', 'skin_upload' ) );
		add_action( 'save_post', 'alli_dependencies' );
		
		if ( isset( $_GET['alli_upload_button'] ) || isset( $_POST['alli_upload_button'] ) )
			add_action( 'admin_init', 'alli_image_upload_option' );
	}
	
	/**
	 * Adds the theme admin filters.
	 *
	 * @since 1.0
	 */
	function filters() {
		if( isset( $_GET['page'] ) && $_GET['page'] == 'alli-options' )
			add_filter( 'tiny_mce_before_init', 'alli_tiny_mce_before_init' );
	}
	
	/**
	 * Adds the theme options menu.
	 *
	 * @since 1.0
	 */
	function menus() {
		add_menu_page( THEME_NAME, THEME_NAME, 'edit_theme_options', 'alli-options', array( 'alliAdmin', 'options' ) );
	}
	
	/**
	 * Creates the theme options menu.
	 *
	 * @since 1.0
	 */
	function options() {
		$page = include( THEME_ADMIN_OPTIONS . '/' . $_GET['page'] . '.php' );
		
		if( $page['load'] ) {
			new alliOptionGenerator( $page['options'] );
		}
	}
	
	/**
	 * Adds the theme post/page metaboxes.
	 *
	 * @since 1.0
	 */
	function metaboxes() {
		$portfolio = include( THEME_ADMIN_OPTIONS . '/meta-portfolio.php' );
		$slideshow = include( THEME_ADMIN_OPTIONS . '/meta-post-slideshow.php' );
		$page = include( THEME_ADMIN_OPTIONS . '/meta-page.php' );
		$post = include( THEME_ADMIN_OPTIONS . '/meta-post.php' );
		
		new alliShortcodeMetaBox( $pages = array( 'page', 'post', 'portfolio' ) );
		
		if( $portfolio['load'] ) {
			new alliMetaBox( $portfolio['options'] );
		}
		
		if( $page['load'] ) {
			new alliMetaBox( $page['options'] );
		}
			
		if( $post['load'] ) {
			new alliMetaBox( $post['options'] );
		}
			
		if( $slideshow['load'] ) {
			new alliMetaBox( $slideshow['options'] );
		}
	}
	
	/**
	 * Checks & functions to run on theme activation.
	 *
	 * @since 1.0
	 */
	function activation() {
		global $pagenow, $wp_rewrite;
		if ( 'themes.php' == $pagenow && isset( $_GET['activated'] ) ) {

			# Check php version
			if( version_compare( PHP_VERSION, '5', '<' ) ) {
				switch_theme( 'twentyten', 'twentyten' );
				$error_msg = 'Your PHP version is too old, please upgrade to a newer version. Your version is %s, %s requires %s<br /><a href="%s">Return to theme activation &raquo</a>';
				wp_die(sprintf( __( $error_msg, ALLI_ADMIN_TEXTDOMAIN ), phpversion(), THEME_NAME, '5.0', admin_url( 'themes.php' ) ) );
			}
			
			# Add defualt widgets && show_on_front 'posts'
			if( get_option( ALLI_SETTINGS ) == false ) {
				//alli_default_options( 'widgets' );
				//update_option( 'show_on_front', 'posts' ); 
			}
				
			# Call alli_post_types() & flush rewrite rules
			alli_post_types();
			$wp_rewrite->flush_rules();

			# Redirect to admin panel
			wp_redirect(admin_url( "admin.php?page=alli-options&activated=true" ));
		}
	}
	
	/**
	 * Check current environment is supported for the theme.
	 * 
	 * @since 1.0
	 */
	function warnings(){
		global $wp_version;

		$errors = array();
		
		if( !alli_check_wp_version() )
			$errors[]='Wordpress version('.$wp_version.') is too low. Please upgrade to 3.1';
		
		if( !empty( $errors ) ) {
			$str = '<ul>';
			foreach($errors as $error){
				$str .= '<li>'.$error.'</li>';
			}
			$str .= '</ul>';
			echo '<div class="error fade"><p><strong>' . sprintf( __( '%1$s Error Messages', ALLI_ADMIN_TEXTDOMAIN ), THEME_NAME ) . '</strong><br />' . $str . '</p></div>';
		}
	}
	
}

?>
