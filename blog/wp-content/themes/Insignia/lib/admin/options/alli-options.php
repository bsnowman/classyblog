<?php

$option_tabs = array(
	'alli_generalsettings_tab' => __( 'General Settings', ALLI_ADMIN_TEXTDOMAIN ),
	'alli_homepage_tab' => __( 'Homepage', ALLI_ADMIN_TEXTDOMAIN ),
	'alli_skins_tab' => __( 'Skins', ALLI_ADMIN_TEXTDOMAIN ),
	'alli_imageresizing_tab' => __( 'Image Resizing', ALLI_ADMIN_TEXTDOMAIN ),
	'alli_slideshow_tab' => __( 'Slider', ALLI_ADMIN_TEXTDOMAIN ),
	'alli_blog_tab' => __( 'Magazine', ALLI_ADMIN_TEXTDOMAIN ),
	'alli_sidebar_tab' => __( 'Sidebar', ALLI_ADMIN_TEXTDOMAIN ),
	'alli_footer_tab' => __( 'Footer', ALLI_ADMIN_TEXTDOMAIN ),
	'alli_sociable_tab' => __( 'Socials', ALLI_ADMIN_TEXTDOMAIN ),
	'alli_advanced_tab' => __( 'Advanced', ALLI_ADMIN_TEXTDOMAIN )
);

$options = array(
	
	/**
	 * Navigation
	 */
	array(
		'name' => $option_tabs,
		'type' => 'navigation'
	),
	
	/**
	 * General Settings
	 */
	array(
		'name' => array( 'alli_generalsettings_tab' => $option_tabs ),
		'type' => 'tab_start'
	),
	
		array(
			'name' => __( 'Logo Settings', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'You can choose whether you wish to display a custom logo or your site title.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'display_logo',
			'options' => array(
				'true' => __( 'Custom Image Logo', ALLI_ADMIN_TEXTDOMAIN ),
				'' => sprintf( __( 'Display Site Title <small><a href="%1$s/wp-admin/options-general.php" target="_blank">(click here to edit site title)</a></small>', ALLI_ADMIN_TEXTDOMAIN ), esc_url( get_option('siteurl') ) )
			),
			'type' => 'radio'
		),
		array(
			'name' => __( 'Custom Image Logo', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'Upload an image to use as your logo.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'logo_url',
			'type' => 'upload'
		),
		array(
			'name' => __( 'Custom Favicon', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'Upload an image to use as your favicon.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'favicon_url',
			'type' => 'upload'
		),

		array( //add for insignia
			'name' => __( 'Extra Header Text', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'This will display in your header.<br /><br />It is usually used for a phone number or something similar.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'extra_header',
			'htmlspecialchars' => true,
			'type' => 'text'
		),	
		array( //add for insignia
			'name' => __( 'Header Right Side', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'Add a piece of HTML code to display in the right side of your header.<br /><br />It is usually used for an ad (60px height) or a phone number.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'header_right',
			'htmlspecialchars' => true,
			'type' => 'textarea'
		),	

		array(
			'name' => __('Main Layout Type', ALLI_ADMIN_TEXTDOMAIN),
			'desc' => __('Use this option to choose between a full-width or boxed layout.', ALLI_ADMIN_TEXTDOMAIN),
			'id' => 'main_layout_type',
			'options' => array(
					'boxed' => __('Boxed', ALLI_ADMIN_TEXTDOMAIN),
					'full_width' => __('Full width', ALLI_ADMIN_TEXTDOMAIN)
				),
			'type' => 'radio',
			'default' => 'boxed'
			),

		array(
			'name' => __('Site Width', ALLI_ADMIN_TEXTDOMAIN),
			'desc' => __('Choose whether to use a Fixed layout (which stays at 960px sitewide) or a Semi-Fluid layout which scales up to 1265px sitewide (resizing the sidebar depending on the users browser size)', ALLI_ADMIN_TEXTDOMAIN),
			'id' => 'site_width',
			'options' => array(
					'semi_fluid' => __('Semi-Fluid (up to 1265px)', ALLI_ADMIN_TEXTDOMAIN),
					'fixed' => __('Fixed (960px)', ALLI_ADMIN_TEXTDOMAIN)
				),
			'type' => 'radio',
			'default' => 'semi_fluid'
			),	


		array(
			'name' => __( 'Twitter Default Username', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'This will be used for all twitter related features.  You may still use a different twitter username to override the default.<br /><br />Example:  When using the twitter widget you may choose a username to use instead of the default.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'twitter_id',
			'type' => 'text'
		),

		array(
			'name' => __( 'Disable Breadcrumbs', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'Check if you do not want breadcrumbs to display anywhere in your site.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'disable_breadcrumbs',
			'options' => array( 'true' => __( 'Globally Disable Breadcrumbs', ALLI_ADMIN_TEXTDOMAIN ) ),
			'type' => 'checkbox'
		),
		array(
			'name' => __( 'Breadcrumb Delimiter', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'This is the symbol that will appear in between your breadcrumbs.<br /><br />Some common examples would be:<br /><br /><code>/ > - , :: >></code>', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'breadcrumb_delimiter',
			'htmlentities' => true,
			'type' => 'text'
		),
		array(
			'name' => __( 'Google Analytics Code', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' =>  __( 'After signing up with Google Analytics paste the code that it gives you here.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'analytics_code',
			'type' => 'textarea'
		),
		array(
			'name' => __( 'Custom CSS', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'This is a great place for doing quick custom styles.  For example if you wanted to change the site title color then you would paste this:<br /><br /><code>.logo a { color: blue; }</code><br /><br />If you are having problems styling something then ask on the support forum and we will be with you shortly.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'custom_css',
			'type' => 'textarea'
		),
		array(
			'name' => __( 'Custom JavaScript', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'In case you need to add some custom javascript you may insert it here.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'custom_js',
			'type' => 'textarea'
		),
		
	array(
		'type' => 'tab_end'
	),
	
	/**
	 * Homepage
	 */
	array(
		'name' => array( 'alli_homepage_tab' => $option_tabs ),
		'type' => 'tab_start'
	),
	
		array(
			'name' => __( 'Homepage Layout', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'You can choose between a left, right, or no sidebar layout for your homepage.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'homepage_layout',
			'options' => array(
				'full_width' => THEME_ADMIN_ASSETS_URI . '/images/footer_column/1.png',
				'left_sidebar' => THEME_ADMIN_ASSETS_URI . '/images/footer_column/fourth_threefourth.png',
				'right_sidebar' => THEME_ADMIN_ASSETS_URI . '/images/footer_column/threefourth_fourth.png',
			),
			'type' => 'layout'
		),

		array(
			'name' => __( 'Custom Homepage Text', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'You can add some custom content to your homepage.<br /><br />This will display under the slider and call to action button.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'content',
			'type' => 'editor'
		),
		array(
			'name' => __( 'Additional Homepage Text', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'You can choose additional page content to display on your homepage.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => "mainpage_content",
			'target' => 'page',
			'type' => 'select'
		),
		array(
			'name' => __( 'Disable Slider', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'Check to disable your slider on the homepage.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'home_slider_disable',
			'options' => array( 'true' => __( 'Disable Homepage Slider', ALLI_ADMIN_TEXTDOMAIN ) ),
			'type' => 'checkbox'
		),
		array(
			'name' => __( 'Display Blog Posts', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'Check to display your blog posts in the homepage content.<br /><br />You can control how many posts you wish to display in Dashboard -> Settings -> Reading.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'frontpage_blog',
			'options' => array( 'display_teaser_title' => __( 'Display Blog Posts on Homepage', ALLI_ADMIN_TEXTDOMAIN ) ),
			'type' => 'checkbox'
		),


	
	array(
		'type' => 'tab_end'
	),
	
	/**
	 * Looks and feeling
	 */
	array(
		'name' => array( 'alli_skins_tab' => $option_tabs ),
		'type' => 'tab_start'
	),

		
		
		array(
			'name' => __( 'Skin Generator Options', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'Use our advanced skin generator to design your website.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'skin_generator',
			'options' => array( 
				'choose' => __( 'Choose a Skin', ALLI_ADMIN_TEXTDOMAIN ),
				'create' => __( 'Create a New Skin', ALLI_ADMIN_TEXTDOMAIN ),
				'manage' => __( 'Manage Skins', ALLI_ADMIN_TEXTDOMAIN )
				),
			'default' => 'choose',
			'toggle' => 'toggle_true',
			'type' => 'skin_generator'
		),
		array(
			'name' => __( 'Available Skins', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'Select a skin to use with your theme.<br /><br />To upload new skins that you have downloaded click on the Manage Skins radio button.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'style_variations',
			'default' => '',
			'target' => 'style_variations',
			'toggle_class' => 'skin_generator_choose',
			'type' => 'skin_select'
		),

	array(
		'type' => 'tab_end'
	),
	
	/**
	 * Image Resizing
	 */
	
	array(
		'name' => array( 'alli_imageresizing_tab' => $option_tabs ),
		'type' => 'tab_start'
	),
	
		array(
			'name' => __( 'Disable Image Resizing', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'Check to disable all image resizing.<br /><br />Images will be displayed in the dimensions as they were uploaded.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'image_resize',
			'options' => array( 'true' => __( 'Disable Image Resizing', ALLI_ADMIN_TEXTDOMAIN ) ),
			'type' => 'checkbox'
		),
		array(
			'name' => __( 'Type of Image Resizing', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'Choose whether you wish to use the TimThumb resize script or Wordpress image resizing.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'image_resize_type',
			'options' => array( 
				'wordpress' => __( 'Built in WordPress', ALLI_ADMIN_TEXTDOMAIN ),
				'timthumb' => __( 'Timthumb', ALLI_ADMIN_TEXTDOMAIN )
			),
			'type' => 'radio'
		),
		array(
			'name' => __( 'Auto Featured Image', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'By default features such as the portfolio and blog will use the "featured image" in your posts.<br /><br />Check this if you wish to use the first image uploaded to your post instead of the featured image.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'auto_img',
			'options' => array( 'true' => __( 'Enable Auto Featured Image Selection', ALLI_ADMIN_TEXTDOMAIN ) ),
			'type' => "checkbox"
		),
		/*array(
			'name' => __( 'Images Sizes for Full Width Layouts', ALLI_ADMIN_TEXTDOMAIN ),
			'type' => 'toggle_start'
		),
		
		array(
			'name' => __( 'One Column Portfolio', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'Input the dimensions that you would like the one column portfolio images to use.<br /><br />These are the images displayed with the portfolio grid shortcode in a full width layout.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'one_column_portfolio_full',
			'type' => 'resize'
		),
		array(
			'name' => __( 'Two Column Portfolio', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'Input the dimensions that you would like the two column portfolio images to use.<br /><br />These are the images displayed with the portfolio grid shortcode in a full width layout.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'two_column_portfolio_full',
			'type' => 'resize'
		),
		array(
			'name' => __( 'Three Column Portfolio', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'Input the dimensions that you would like the three column portfolio images to use.<br /><br />These are the images displayed with the portfolio grid shortcode in a full width layout.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'three_column_portfolio_full',
			'type' => 'resize'
		),
		array(
			'name' => __( 'Four Column Portfolio', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'Input the dimensions that you would like the four column portfolio images to use.<br /><br />These are the images displayed with the portfolio grid shortcode in a full width layout.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'four_column_portfolio_full',
			'type' => 'resize'
		),
		array(
			'name' => __( 'Portfolio Single Post', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'Input the dimensions that you would like the portfolio single images to use.<br /><br />These are the images displayed on the portfolio single post.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'portfolio_single_full_full',
			'type' => 'resize'
		),
	
		array(
			'name' => __( 'One Column Blog', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'Input the dimensions that you would like the one column blog images to use.<br /><br />These are the images displayed with the blog grid shortcode in a full width layout.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'one_column_blog_full',
			'type' => 'resize'
		),
		array(
			'name' => __( 'Two Column Blog', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'Input the dimensions that you would like the two column blog images to use.<br /><br />These are the images displayed with the blog grid shortcode in a full width layout.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'two_column_blog_full',
			'type' => 'resize'
		),
		array(
			'name' => __( 'Three Column Blog', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'Input the dimensions that you would like the three column blog images to use.<br /><br />These are the images displayed with the blog grid shortcode in a full width layout.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'three_column_blog_full',
			'type' => 'resize'
		),
		array(
			'name' => __( 'Four Column Blog', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'Input the dimensions that you would like the four column blog images to use.<br /><br />These are the images displayed with the blog grid shortcode in a full width layout.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'four_column_blog_full',
			'type' => 'resize'
		),
	
		array(
			'name' => __( 'Small Post List', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'Input the dimensions that you would like the "small" list thumbnails to use.<br /><br />These are the images displayed with the blog and portfolio list shortcode in a full width layout.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'small_post_list_full',
			'type' => 'resize'
		),
		array(
			'name' => __( 'Medium Post List', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'Input the dimensions that you would like the "medium" list thumbnails to use.<br /><br />These are the images displayed with the blog and portfolio list shortcode in a full width layout.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'medium_post_list_full',
			'type' => 'resize'
		),
		array(
			'name' => __( 'Large Post List', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'Input the dimensions that you would like the "large" list thumbnails to use.<br /><br />These are the images displayed with the blog and portfolio list shortcode in a full width layout.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'large_post_list_full',
			'type' => 'resize'
		),
		array(
			'name' => __( 'Additional Posts Module', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'Input the dimensions that you would like the additional posts thumbnails to use.<br /><br />These are the images displayed in the additional posts module.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'additional_posts_grid_full',
			'type' => 'resize'
		),
				
		array(
			'type' => 'toggle_end'
		),*/
		
		array(
			'name' => __( 'Custom Images Sizes', ALLI_ADMIN_TEXTDOMAIN ),
			'type' => 'toggle_start'
		),
		
		array(
			'name' => __( 'One Column Portfolio', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'Input the dimensions that you would like the one column portfolio images to use.<br /><br />These are the images displayed with the portfolio grid shortcode in a right sidebar layout.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'one_column_portfolio_big',
			'type' => 'resize'
		),
		array(
			'name' => __( 'Two Column Portfolio', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'Input the dimensions that you would like the two column portfolio images to use.<br /><br />These are the images displayed with the portfolio grid shortcode in a right sidebar layout.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'two_column_portfolio_big',
			'type' => 'resize'
		),
		array(
			'name' => __( 'Three Column Portfolio', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'Input the dimensions that you would like the three column portfolio images to use.<br /><br />These are the images displayed with the portfolio grid shortcode in a right sidebar layout.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'three_column_portfolio_big',
			'type' => 'resize'
		),
		array(
			'name' => __( 'Four Column Portfolio', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'Input the dimensions that you would like the four column portfolio images to use.<br /><br />These are the images displayed with the portfolio grid shortcode in a right sidebar layout.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'four_column_portfolio_big',
			'type' => 'resize'
		),
		array(
			'name' => __( 'Portfolio Single Post', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'Input the dimensions that you would like the portfolio single images to use.<br /><br />These are the images displayed on the portfolio single post.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'portfolio_single_full_big',
			'type' => 'resize'
		),

		array(
			'name' => __( 'One Column Blog', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'Input the dimensions that you would like the one column blog images to use.<br /><br />These are the images displayed with the blog grid shortcode in a right sidebar layout.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'one_column_blog_big',
			'type' => 'resize'
		),
		array(
			'name' => __( 'Two Column Blog', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'Input the dimensions that you would like the two column blog images to use.<br /><br />These are the images displayed with the blog grid shortcode in a right sidebar layout.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'two_column_blog_big',
			'type' => 'resize'
		),
		array(
			'name' => __( 'Three Column Blog', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'Input the dimensions that you would like the three column blog images to use.<br /><br />These are the images displayed with the blog grid shortcode in a right sidebar layout.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'three_column_blog_big',
			'type' => 'resize'
		),
		array(
			'name' => __( 'Four Column Blog', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'Input the dimensions that you would like the four column blog images to use.<br /><br />These are the images displayed with the blog grid shortcode in a right sidebar layout.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'four_column_blog_big',
			'type' => 'resize'
		),

		array(
			'name' => __( 'Small Post List', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'Input the dimensions that you would like the "small" list thumbnails to use.<br /><br />These are the images displayed with the blog and portfolio list shortcode in a right sidebar layout.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'small_post_list_big',
			'type' => 'resize'
		),
		array(
			'name' => __( 'Medium Post List', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'Input the dimensions that you would like the "medium" list thumbnails to use.<br /><br />These are the images displayed with the blog and portfolio list shortcode in a right sidebar layout.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'medium_post_list_big',
			'type' => 'resize'
		),
		array(
			'name' => __( 'Large Post List', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'Input the dimensions that you would like the "large" list thumbnails to use.<br /><br />These are the images displayed with the blog and portfolio list shortcode in a right sidebar layout.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'large_post_list_big',
			'type' => 'resize'
		),
		array(
			'name' => __( 'Additional Posts Module', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'Input the dimensions that you would like the additional posts thumbnails to use.<br /><br />These are the images displayed in the additional posts module.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'additional_posts_grid_big',
			'type' => 'resize'
		),

		array(
			'type' => 'toggle_end'
		),
		
		/*array(
			'name' => __( 'Images Sizes for 960px width layout', ALLI_ADMIN_TEXTDOMAIN ),
			'type' => 'toggle_start'
		),

		array(
			'name' => __( 'One Column Portfolio', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'Input the dimensions that you would like the one column portfolio images to use.<br /><br />These are the images displayed with the portfolio grid shortcode in a 960px layout.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'one_column_portfolio_small',
			'type' => 'resize'
		),
		array(
			'name' => __( 'Two Column Portfolio', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'Input the dimensions that you would like the two column portfolio images to use.<br /><br />These are the images displayed with the portfolio grid shortcode in a 960px layout.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'two_column_portfolio_small',
			'type' => 'resize'
		),
		array(
			'name' => __( 'Three Column Portfolio', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'Input the dimensions that you would like the three column portfolio images to use.<br /><br />These are the images displayed with the portfolio grid shortcode in a 960px layout.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'three_column_portfolio_small',
			'type' => 'resize'
		),
		array(
			'name' => __( 'Four Column Portfolio', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'Input the dimensions that you would like the four column portfolio images to use.<br /><br />These are the images displayed with the portfolio grid shortcode in a 960px layout.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'four_column_portfolio_small',
			'type' => 'resize'
		),
		array(
			'name' => __( 'Portfolio Single Post', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'Input the dimensions that you would like the portfolio single images to use.<br /><br />These are the images displayed on the portfolio single post.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'portfolio_single_full_small',
			'type' => 'resize'
		),

		array(
			'name' => __( 'One Column Blog', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'Input the dimensions that you would like the one column blog images to use.<br /><br />These are the images displayed with the blog grid shortcode in a 960px layout.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'one_column_blog_small',
			'type' => 'resize'
		),
		array(
			'name' => __( 'Two Column Blog', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'Input the dimensions that you would like the two column blog images to use.<br /><br />These are the images displayed with the blog grid shortcode in a 960px layout.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'two_column_blog_small',
			'type' => 'resize'
		),
		array(
			'name' => __( 'Three Column Blog', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'Input the dimensions that you would like the three column blog images to use.<br /><br />These are the images displayed with the blog grid shortcode in a 960px layout.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'three_column_blog_small',
			'type' => 'resize'
		),
		array(
			'name' => __( 'Four Column Blog', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'Input the dimensions that you would like the four column blog images to use.<br /><br />These are the images displayed with the blog grid shortcode in a 960px layout.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'four_column_blog_small',
			'type' => 'resize'
		),

		array(
			'name' => __( 'Small Post List', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'Input the dimensions that you would like the "small" list thumbnails to use.<br /><br />These are the images displayed with the blog and portfolio list shortcode in a 960px layout.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'small_post_list_small',
			'type' => 'resize'
		),
		array(
			'name' => __( 'Medium Post List', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'Input the dimensions that you would like the "medium" list thumbnails to use.<br /><br />These are the images displayed with the blog and portfolio list shortcode in a 960px layout.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'medium_post_list_small',
			'type' => 'resize'
		),
		array(
			'name' => __( 'Large Post List', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'Input the dimensions that you would like the "large" list thumbnails to use.<br /><br />These are the images displayed with the blog and portfolio list shortcode in a 960px layout.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'large_post_list_small',
			'type' => 'resize'
		),
		array(
			'name' => __( 'Additional Posts Module', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'Input the dimensions that you would like the additional posts thumbnails to use.<br /><br />These are the images displayed in the additional posts module.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'additional_posts_grid_small',
			'type' => 'resize'
		),

		array(
			'type' => 'toggle_end'
		),*/
		
	array(
		'type' => 'tab_end'
	),
	
	/**
	 * Slideshow
	 */
	array(
		'name' => array( 'alli_slideshow_tab' => $option_tabs ),
		'type' => 'tab_start'
	),
	
		array(
			'name' => __( 'Choose Slider Type', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'To get started choose which slider you would like to use.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'homepage_slider',
			'options' => array( 
				/*'fading_slider' => __( 'Fading Slider', ALLI_ADMIN_TEXTDOMAIN ),
				'scrolling_slider' => __( 'Scrolling Slider', ALLI_ADMIN_TEXTDOMAIN ),*/
				'nivo_slider' => __( 'Nivo Slider', ALLI_ADMIN_TEXTDOMAIN )
			),
			'toggle' => 'toggle_true',
			'type' => 'select',
		),

		array(
			'name' => __( 'Advanced Slider Settings', ALLI_ADMIN_TEXTDOMAIN ),
			'toggle_class' => 'slider_option_toggle',
			'type' => 'toggle_start'
		),

		array(
			'name' => __( 'Transition Effects', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'With the Nivo slider there are a few transition effects that you can use.<br /><br />To use them all click on random.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'nivo_effect',
			'target' => 'nivo_effects',
			'toggle_class' => 'homepage_slider_nivo_slider',
			'type' => 'select',
		),
		array(
			'name' => __( 'Segments', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'Input a number for how many segments (slices) you want the transitions to use.<br /><br />It would be best to stick between 5 - 15 for best results.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'nivo_slices',
			'toggle_class' => 'homepage_slider_nivo_slider',
			'type' => 'text'
		),
		array(
			'name' => __( 'Animation Speed', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'Input a number to use for the animation speed.  This is the speed at which the transitions play.<br /><br />This number is in milliseconds so common values would be 1000 - 5000.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'nivo_anim_speed',
			'toggle_class' => 'homepage_slider_nivo_slider',
			'type' => 'text'
		),
		array(
			'name' => __( 'Slider Transition Speed', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'Input a number to use for the transition speed.  This is the speed which determines how long an image is displayed before transitioning to the next.<br /><br />This number is in milliseconds so common values would be 1000 - 5000.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'slider_speed',
			'type' => 'text'
		),
		array(
			'name' => __( 'Disable Slider Transitions', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'This will stop automatic sliding which will leave it to the user to navigate through the images.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'slider_disable_trans',
			'options' => array( 'true' => __( 'Disable Automatic Slider Transitions', ALLI_ADMIN_TEXTDOMAIN ) ),
			'type' => 'checkbox'
		),
		array(
			'name' => __( 'Disable Pause On Hover', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'By default the slider will stop sliding when you hover over it.<br /><br />Check to disable this.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'slider_hover_pause',
			'options' => array( 'true' => __( 'Disable Pause On Hover', ALLI_ADMIN_TEXTDOMAIN ) ),
			'default' => '',
			'type' => 'checkbox'
		),
		array(
			'name' => __( 'Slider Fade Speed', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'The speed of the fade animations.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'slider_fade_speed',
			'options' => array( 
				'slow' => __( 'Slow', ALLI_ADMIN_TEXTDOMAIN ),
				'fast' => __( 'Fast', ALLI_ADMIN_TEXTDOMAIN )
			),
			'toggle_class' => 'homepage_slider_fading_slider homepage_slider_scrolling_slider',
			'type' => 'radio'
		),
		array(
			'name' => __( 'Slider Navigation Style', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'You can choose between having dots or thumbnails for the slider navigation.<br /><br />If choosing thumbnails then they will be automatically generated and resized.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'slider_nav',
			'options' => array( 
				'thumb' => __( 'Image Thumbnails', ALLI_ADMIN_TEXTDOMAIN ),
				'dots' => __( 'Nav Dots', ALLI_ADMIN_TEXTDOMAIN )
			),
			'toggle_class' => 'homepage_slider_fading_slider homepage_slider_scrolling_slider',
			'type' => 'radio'
		),
		array(
			'name' => __( 'Next &amp; Prev Buttons', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'The Nivo slider comes with next and previous buttons which you can choose to disable, display on hover, or always show.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'nivo_direction_nav',
			'options' => array( 
				'button' => __( 'Always Display Next &amp; Previous Buttons', ALLI_ADMIN_TEXTDOMAIN ),
				'button_hover' => __( 'Display Next &amp; Previous Buttons on Hover', ALLI_ADMIN_TEXTDOMAIN ),
				'disable' => __( 'Disable Next &amp; Previous Buttons', ALLI_ADMIN_TEXTDOMAIN )
			),
			'toggle_class' => 'homepage_slider_nivo_slider',
			'type' => 'radio'
		),
		array(
			'name' => __( 'Display Nav Dots', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'Checking this will display dots along the bottom where the user can navigate between images.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'nivo_control_nav',
			'options' => array( 'true' => __( 'Display Navigation Dots', ALLI_ADMIN_TEXTDOMAIN ) ),
			'toggle_class' => 'homepage_slider_nivo_slider',
			'type' => 'checkbox'
		),
		array(
			'name' => __( 'Display Slider on Every Page', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'Checking this will enable the slider to display on every page.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'slider_page',
			'options' => array( 'true' => __( 'Display Homepage Slider on all Pages', ALLI_ADMIN_TEXTDOMAIN ) ),
			'type' => 'checkbox'
		),
		array(
			'name' => __( 'Slider Source', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'You can choose whether to populate the slider here or from your post categories.<br /><br />If you choose from post categories then you can set the images when editing your posts.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'slider_custom',
			'options' => array( 
				'custom' => __( 'Custom Define Slides Below', ALLI_ADMIN_TEXTDOMAIN ),
				'categories' => __( 'Automatically Create Slides from Blog Categories', ALLI_ADMIN_TEXTDOMAIN ),
			),
			'toggle' => 'toggle_true',
			'type' => 'radio'
		),
		array(
			'name' => __( 'Number of Slides', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'Enter the number of slider images to display (default is 5)', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'slider_cat_count',
			'toggle_class' => 'slider_custom_categories',
			'type' => 'text'
		),
		
		array(
			'type' => 'toggle_end'
		),

		array(
			'name' => __( 'Slideshow', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'slideshow',
			'toggle_class' => 'slider_custom_custom',
			'type' => 'slideshow'
		),
		array(
			'name' => __( 'Slider Categories', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'slider_cats',
			'target' => 'cat',
			'toggle_class' => 'slider_custom_categories',
			'type' => 'multidropdown'
		),
		
	array(
		'type' => 'tab_end'
	),
	
	/**
	 * Blog
	 */
	array(
		'name' => array( 'alli_blog_tab' => $option_tabs ),
		'type' => 'tab_start'
	),
	
		array(
			'name' => __( 'Blog Page', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'Choose one of your pages to use as a blog page.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'blog_page',
			'target' => 'page',
			'type' => 'select'
		),
		array(
			'name' => __( 'Magazine Layout', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'Your blog posts will use the layout you select here.<br /><br />If you want an image to display in the layout then do not forget to set your featured image when editing your posts.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'blog_layout',
			'options' => array(
				'blog_layout1' => THEME_ADMIN_ASSETS_URI . '/images/blog_layout/blog_layout1.png',
				'blog_layout2' => THEME_ADMIN_ASSETS_URI . '/images/blog_layout/blog_layout2.png',
				'blog_layout3' => THEME_ADMIN_ASSETS_URI . '/images/blog_layout/blog_layout3.png'
			),
			'type' => 'layout'
		),
		array(
			'name' => __( 'Exclude Categories', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'You can choose certain categories to exclude from your blog page.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'exclude_categories',
			'target' => 'cat',
			'type' => 'multidropdown'
		),
		array(
			'name' => __( 'Popular &amp; Related Posts', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'By default a popular / related posts module will display on your posts.  You can choose how to display it or disable it here.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'post_like_module',
			'options' => array( 
				'tab' => __( 'Display in a Tabbed Layout', ALLI_ADMIN_TEXTDOMAIN ),
				'column' => __( 'Display in a Column Layout', ALLI_ADMIN_TEXTDOMAIN ),
				'disable' => __( 'Disable Popular &amp; Related Posts Module', ALLI_ADMIN_TEXTDOMAIN )
			),
			'type' => 'radio'
		),
		array(
			'name' => __( 'Comments &amp; Trackbacks', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'You can choose whether you want your comments and trackbacks bundled together or separated in tabs.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'post_comment_styles',
			'options' => array( 
				'tab' => __( 'Display in Separate Tabs', ALLI_ADMIN_TEXTDOMAIN ),
				'list' => __( 'Display Together in a List', ALLI_ADMIN_TEXTDOMAIN )
			),
			'type' => 'radio'
		),
		array(
			'name' => __( 'Display Full Blog Posts', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'By default blog posts will be displayed as excerpts.<br /><br />Checking this will display the full content of your post.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'display_full',
			'options' => array( 'true' => __( 'Display Full Blog Posts on Blog Index Page', ALLI_ADMIN_TEXTDOMAIN ) ), 
			'type' => 'checkbox'
		),
		array(
			'name' => __( 'Disable About Author', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'By default an about the author module will display when viewing your posts.<br /><br />You can choose to disable it here.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'disable_post_author',
			'options' => array( 'true' => __( 'Disable the About Author Module', ALLI_ADMIN_TEXTDOMAIN ) ),
			'type' => 'checkbox'
		),
		array(
			'name' => __( 'Disable Post Nav', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'By default your posts will display links at the bottom to your other posts.<br /><br />Check this to disable those links.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'disable_post_nav',
			'options' => array( 'true' => __( 'Disable Post Navigation Module', ALLI_ADMIN_TEXTDOMAIN ) ),
			'type' => 'checkbox'
		),

		array(
			'name' => __( 'URL shortening', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'You can choose to have certain links automatically use the bit.ly URL shortening service.<br /><br />For example the social icons on each post will use bit.ly URLs when this is checked.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'url_shortening',
			'options' => array( 'true' => __( 'Enable bit.ly URL Shortening', ALLI_ADMIN_TEXTDOMAIN ) ),
			'toggle' => 'toggle_true',
			'type' => 'checkbox'
		),
		array(
			'name' => __( 'bit.ly Login', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'Input the Username for your bit.ly account here.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'bitly_login',
			'toggle_class' => 'url_shortening_true',
			'type' => 'text'
		),
		array(
			'name' => __( 'bit.ly API Key', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'Input the API key for your bit.ly account here.<br /><br />You can find this by logging in at bit.ly and navigating to your settings page.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'bitly_api',
			'toggle_class' => 'url_shortening_true',
			'type' => 'text'
		),
		array(
			'name' => __( 'Disable Meta Options', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'The post meta will display under the title on your blog page.<br /><br />You can choose sections to disable here.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'disable_meta_options',
			'options' => array(
				'author_meta' => 'Disable Author Meta',
				'date_meta' => 'Disable Date Meta',
				'comments_meta' => 'Disable Comments Meta',
				'categories_meta' => 'Disable Categories Meta',
				'tags_meta' => 'Disable Tags Meta'
			),
			'type' => 'checkbox'
		),
	
	array(
		'type' => 'tab_end'
	),
	
	/**
	 * Sidebar
	 */
	array(
		'name' => array( 'alli_sidebar_tab' => $option_tabs ),
		'type' => 'tab_start'
	),
	
		array(
			'name' => __( 'Create New Sidebar', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'You can create additional sidebars to use.<br /><br />To display your new sidebar then you will need to select it in the &quot;Custom Sidebar&quot; dropdown when editing a post or page.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'custom_sidebars',
			'type' => 'sidebar'
		),
	
	array(
		'type' => 'tab_end'
	),
	
	/**
	 * Footer
	 */
	array(
		'name' => array( 'alli_footer_tab' => $option_tabs ),
		'type' => 'tab_start'
	),
	
		array(
			'name' => __( 'Footer Column layout', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'Select which column layout you would like to display with your footer.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'footer_columns',
			'options' => array(
				'1' => THEME_ADMIN_ASSETS_URI . '/images/footer_column/1.png',
				'2' => THEME_ADMIN_ASSETS_URI . '/images/footer_column/2.png',
				'3' => THEME_ADMIN_ASSETS_URI . '/images/footer_column/3.png',
				'4' => THEME_ADMIN_ASSETS_URI . '/images/footer_column/4.png',
				'5' => THEME_ADMIN_ASSETS_URI . '/images/footer_column/5.png',
				'6' => THEME_ADMIN_ASSETS_URI . '/images/footer_column/6.png',
				
				'third_twothird' => THEME_ADMIN_ASSETS_URI . '/images/footer_column/third_twothird.png',
				'fourth_threefourth' => THEME_ADMIN_ASSETS_URI . '/images/footer_column/fourth_threefourth.png',
				'fourth_fourth_half' => THEME_ADMIN_ASSETS_URI . '/images/footer_column/fourth_fourth_half.png',
				'sixth_fivesixth' => THEME_ADMIN_ASSETS_URI . '/images/footer_column/sixth_fivesixth.png',
				'third_sixth_sixth_sixth_sixth' => THEME_ADMIN_ASSETS_URI . '/images/footer_column/third_sixth_sixth_sixth_sixth.png',
				'half_sixth_sixth_sixth' => THEME_ADMIN_ASSETS_URI . '/images/footer_column/half_sixth_sixth_sixth.png',
				
				'twothird_third' => THEME_ADMIN_ASSETS_URI . '/images/footer_column/twothird_third.png',
				'threefourth_fourth' => THEME_ADMIN_ASSETS_URI . '/images/footer_column/threefourth_fourth.png',
				'half_fourth_fourth' => THEME_ADMIN_ASSETS_URI . '/images/footer_column/half_fourth_fourth.png',
				'fivesixth_sixth' => THEME_ADMIN_ASSETS_URI . '/images/footer_column/fivesixth_sixth.png',
				'sixth_sixth_sixth_sixth_third' => THEME_ADMIN_ASSETS_URI . '/images/footer_column/sixth_sixth_sixth_sixth_third.png',
				'sixth_sixth_sixth_half' => THEME_ADMIN_ASSETS_URI . '/images/footer_column/sixth_sixth_sixth_half.png'
			),
			'type' => 'layout'
		),
		array(
			'name' => __( 'Disable Footer Widgets', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'Check this if you do not wish to display any widgets with your footer.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'footer_disable',
			'options' => array( 'true' => __( 'Disable All Footer Widgets', ALLI_ADMIN_TEXTDOMAIN ) ),
			'type' => 'checkbox'
		),

		array(
			'name' => __( 'Copyright Text', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'You can display copyright information here.  It will show below your footer on the left hand side.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'footer_text',
			'default' => '',
			'htmlspecialchars' => true,
			'type' => 'text'
		),
	
	array(
		'type' => 'tab_end'
	),
	
	/**
	 * Sociable
	 */
	array(
		'name' => array( 'alli_sociable_tab' => $option_tabs ),
		'type' => 'tab_start'
	),
	
		array(
			'name' => __( 'Sociable', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'Sociable Generator', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'sociable',
			'type' => 'sociable'
		),
		
	array(
		'type' => 'tab_end'
	),
	
	/**
	 * Advanced
	 */
	array(
		'name' => array( 'alli_advanced_tab' => $option_tabs ),
		'type' => 'tab_start'
	),
	
		array(
			'name' => __( 'Custom Admin Logo', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'Upload an image to replace the default Alliase logo.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'admin_logo_url',
			'type' => 'upload'
		),
		array(
			'name' => __( 'Import Options', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'Copy your export code here to import your theme settings.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'import_options',
			'type' => 'textarea'
		),
		array(
			'name' => __( 'Export Options', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'When moving your site to a new Wordpress installation you can export your theme settings here.', ALLI_ADMIN_TEXTDOMAIN ),
			'id' => 'export_options',
			'type' => 'export_options'
		),
		
	array(
		'type' => 'tab_end'
	),
	
);

return array(
	'load' => true,
	'name' => 'options',
	'options' => $options
);
	
?>
