<?php
/**
 *
 */
function alli_options_init() {
	register_setting( ALLI_SETTINGS, ALLI_SETTINGS );
	
	# Add default options if they don't exist
	add_option( ALLI_SETTINGS, alli_default_options( 'settings' ) );
	add_option( ALLI_INTERNAL_SETTINGS, alli_default_options( 'internal' ) );
	# delete_option(ALLI_SETTINGS);
	# delete_option(ALLI_INTERNAL_SETTINGS);
	
	if( alli_ajax_request() ) {
		# Ajax option save
		if( isset( $_POST['alli_option_save'] ) ) {
			alli_ajax_option_save();
			
		# Sidebar option save
		} elseif( isset( $_POST['alli_sidebar_save'] ) ) {
			alli_sidebar_option_save();
			
		} elseif( isset( $_POST['alli_sidebar_delete'] ) ) {
			alli_sidebar_option_delete();
			
		} elseif( isset( $_POST['action'] ) && $_POST['action'] == 'add-menu-item' ) {
			add_filter( 'nav_menu_description', create_function('','return "";') );
		}
	}
	
	# Option import
	if( ( !alli_ajax_request() ) && ( isset( $_POST['alli_import_options'] ) ) ) {
		alli_import_options( $_POST[ALLI_SETTINGS]['import_options'] );

	# Reset options
	} elseif( ( !alli_ajax_request() ) && ( isset( $_POST[ALLI_SETTINGS]['reset'] ) ) ) {
		update_option( ALLI_SETTINGS, alli_default_options( 'settings' ) );
		delete_option( ALLI_SIDEBARS );
		wp_redirect( admin_url( 'admin.php?page=alli-options&reset=true' ) );
		exit;
		
	# $_POST option save
	} elseif( ( !alli_ajax_request() ) && ( isset( $_POST['alli_admin_wpnonce'] ) ) ) {
		unset(  $_POST[ALLI_SETTINGS]['export_options'] );
	}
	
}

/**
 *
 */
function alli_sidebar_option_delete() {
	check_ajax_referer( ALLI_SETTINGS . '_wpnonce', 'alli_admin_wpnonce' );
	
	$data = $_POST;
	
	$saved_sidebars = get_option( ALLI_SIDEBARS );
	
	$msg = array( 'success' => false, 'sidebar_id' => $data['sidebar_id'], 'message' => sprintf( __( 'Error: Sidebar &quot;%1$s&quot; not deleted, please try again.', ALLI_ADMIN_TEXTDOMAIN ), $data['alli_sidebar_delete'] ) );
	
	unset( $saved_sidebars[$data['sidebar_id']] );
	
	if( update_option( ALLI_SIDEBARS, $saved_sidebars ) ) {
		$msg = array( 'success' => 'deleted_sidebar', 'sidebar_id' => $data['sidebar_id'], 'message' => sprintf( __( 'Sidebar &quot;%1$s&quot; Deleted.', ALLI_ADMIN_TEXTDOMAIN ), $data['alli_sidebar_delete'] ) );
	}
	
	$echo = json_encode( $msg );

	@header( 'Content-Type: application/json; charset=' . get_option( 'blog_charset' ) );
	echo $echo;
	exit;
}

/**
 *
 */
function alli_sidebar_option_save() {
	check_ajax_referer( ALLI_SETTINGS . '_wpnonce', 'alli_admin_wpnonce' );
	
	$data = $_POST;
	
	$saved_sidebars = get_option( ALLI_SIDEBARS );
	
	$msg = array( 'success' => false, 'sidebar' => $data['custom_sidebars'], 'message' => sprintf( __( 'Error: Sidebar &quot;%1$s&quot; not saved, please try again.', ALLI_ADMIN_TEXTDOMAIN ), $data['custom_sidebars'] ) );
	
	if( empty( $saved_sidebars ) ) {
		$update_sidebar[$data['alli_sidebar_id']] = $data['custom_sidebars'];
		
		if( update_option( ALLI_SIDEBARS, $update_sidebar ) )
			$msg = array( 'success' => 'saved_sidebar', 'sidebar' => $data['custom_sidebars'], 'sidebar_id' => $data['alli_sidebar_id'], 'message' => sprintf( __( 'Sidebar &quot;%1$s&quot; Added.', ALLI_ADMIN_TEXTDOMAIN ), $data['custom_sidebars'] ) );
		
	} elseif( is_array( $saved_sidebars ) ) {
		
		if( in_array( $data['custom_sidebars'], $saved_sidebars ) ) {
			$msg = array( 'success' => false, 'sidebar' => $data['custom_sidebars'], 'message' => sprintf( __( 'Sidebar &quot;%1$s&quot; Already Exists.', ALLI_ADMIN_TEXTDOMAIN ), $data['custom_sidebars'] ) );
			
		} elseif( !in_array( $data['custom_sidebars'], $saved_sidebars ) ) {
			$sidebar[$data['alli_sidebar_id']] = $data['custom_sidebars'];
			$update_sidebar = $saved_sidebars + $sidebar;
			
			if( update_option( ALLI_SIDEBARS, $update_sidebar ) )
				$msg = array( 'success' => 'saved_sidebar', 'sidebar' => $data['custom_sidebars'], 'sidebar_id' => $data['alli_sidebar_id'], 'message' => sprintf( __( 'Sidebar &quot;%1$s&quot; Added.', ALLI_ADMIN_TEXTDOMAIN ), $data['custom_sidebars'] ) );
			
		}
	}
		
	$echo = json_encode( $msg );

	@header( 'Content-Type: application/json; charset=' . get_option( 'blog_charset' ) );
	echo $echo;
	exit;
}

/**
 *
 */
function alli_ajax_option_save() {
	check_ajax_referer( ALLI_SETTINGS . '_wpnonce', 'alli_admin_wpnonce' );
	
	$data = $_POST;
	
	unset( $data['_wp_http_referer'], $data['_wpnonce'], $data['action'], $data['alli_full_submit'], $data[ALLI_SETTINGS]['export_options'] );
	unset( $data['alli_admin_wpnonce'], $data['alli_option_save'], $data['option_page'] );
	
	$msg = array( 'success' => false, 'message' => __( 'Error: Options not saved, please try again.', ALLI_ADMIN_TEXTDOMAIN ) );
	
	if( get_option( ALLI_SETTINGS ) != $data[ALLI_SETTINGS] ) {
		
		if( update_option( ALLI_SETTINGS, $data[ALLI_SETTINGS] ) )
			$msg = array( 'success' => 'options_saved', 'message' => __( 'Options Saved.', ALLI_ADMIN_TEXTDOMAIN ) );
			
	} else {
		$msg = array( 'success' => true, 'message' => __( 'Options Saved.', ALLI_ADMIN_TEXTDOMAIN ) );
	}
	
	$echo = json_encode( $msg );

	@header( 'Content-Type: application/json; charset=' . get_option( 'blog_charset' ) );
	echo $echo;
	exit;
}

/**
 * 
 */
function alli_shortcode_generator() {
	global $alli;
	
	$shortcodes = alli_shortcodes();
	
	$options = array();
	
	foreach( $shortcodes as $shortcode ) {
		$shortcode = str_replace( '.php', '',$shortcode );
		$shortcode = preg_replace( '/[0-9-]/', '', $shortcode );
		
		if( $shortcode[0] != '_' ) {
			$class = 'alli' . ucwords( $shortcode );
			$options[] = call_user_func( array( &$class, '_options' ), $class );
		}
	}
	
	return $options;
}

/**
 *
 */
function alli_check_wp_version(){
	global $wp_version;
	
	$check_WP = '3.0';
	$is_ok = version_compare($wp_version, $check_WP, '>=');
	
	if ( ($is_ok == FALSE) ) {
		return false;
	}
	
	return true;
}

/**
 * 
 */
function alli_wpmu_style_option() {
	$styles = array();
	if( is_multisite() ) {
		global $blog_id;
		$wpmu_styles_path = $_SERVER['DOCUMENT_ROOT'] . '/' . get_blog_option( $blog_id, 'upload_path' ) . '/styles/';
		if(is_dir( $wpmu_styles_path ) ) {
			if($open_dirs = opendir( $wpmu_styles_path ) ) {
				while(($style = readdir($open_dirs)) !== false) {
					if(stristr($style, '.css') !== false) {
						$theme_name = md5( THEME_NAME ) . 'muskin_';
						$style_mu = str_replace( $theme_name, '', $style );
						$styles[$style_mu] = @filemtime( $wpmu_styles_path . $style);
						
						if( stristr($style, 'muskin_') !== false && stristr($style, $theme_name) === false )
							unset($styles[$style_mu]);
						
					}
				}
			}
		}
	}
	
	return $styles;
}

/**
 * 
 */
function alli_style_option() {
	$styles = array();
	$sort_styles = array();
	
	if(is_dir(TEMPLATEPATH . '/styles/')) {
		if($open_dirs = opendir(TEMPLATEPATH . '/styles/')) {
			while(($style = readdir($open_dirs)) !== false) {
				if(stristr($style, '.css') !== false) {
					$styles[$style] = @filemtime(TEMPLATEPATH . '/styles/' . $style);
				}
			}
		}
	}
	
	$styles = array_merge( $styles, alli_wpmu_style_option() );
	
	arsort($styles);
	
	$nt_writable = get_option( ALLI_SKIN_NT_WRITABLE );
	if( !empty( $nt_writable ) ) {
		foreach ( $nt_writable as $key => $val ) {
			$val = $val . '.css';
			$sort_styles[$val] = $val;
		}
	}
	
	foreach ($styles as $key => $val) {
	    $sort_styles[$key] = $key;
	}
	
	unset( $sort_styles['_create_new.css'] );
	
	return $sort_styles;
}

/**
 * 
 */
function alli_sociable_option() {
	$sociables = array();
	$styles = array();
	
	$pic_types = array('jpg', 'jpeg', 'gif', 'png');

	if( is_dir( THEME_DIR . '/images/sociables/default/' ) ) {
		if( $open_dirs = opendir( THEME_DIR . '/images/sociables/default/' ) ) {
			while( ( $sociable = readdir( $open_dirs ) ) !== false ) {
				$parts = explode( '.', $sociable );
				$ext = strtolower( $parts[count($parts) - 1] );
				
				if( in_array( $ext, $pic_types ) ) {
					$option = str_replace( '_',' ', $parts[count($parts) - 2] );
					$option = ucwords( $option );
					$sociables[$sociable] = str_replace( ' ','', $option );
				}
			}
		}
	}
	
	if( is_dir( THEME_DIR . '/images/sociables/' ) ) {
		if( $open_dirs = opendir( THEME_DIR . '/images/sociables/' ) ) {
			while( ( $style = readdir( $open_dirs ) ) !== false ) {
				
				$styles[$style] = ucwords( str_replace( '_',' ', $style ) );
				
				while(($ix = array_search('.',$styles)) > -1)
					unset($styles[$ix]);
			        while(($ix = array_search('..',$styles)) > -1)
			         	unset($styles[$ix]);
			}
			
		}
	}
	
	return array( 'styles' => $styles, 'sociables' => $sociables );
}

/**
 * 
 */
function alli_pattern_presets() {
	$patterns = array();
	$pic_types = array( 'jpg', 'jpeg', 'gif', 'png' );

	if( is_dir( THEME_PATTERNS_DIR ) ) {
		if( $open_dirs = opendir( THEME_PATTERNS_DIR ) ) {
			while( ( $pattern = readdir( $open_dirs ) ) !== false ) {
				$parts = explode( '.', $pattern );
				$ext = strtolower( $parts[count($parts) - 1] );
				
				if( in_array( $ext, $pic_types ) ) {
					$patterns[$pattern] = $parts[count($parts) - 2];
				}
			}
		}
	}
	
	asort( $patterns );
	
	return $patterns;
}

/**
 * 
 */
function alli_cufon_fonts() {
	$cufon = array();
	if(is_dir(THEME_FONTS)) {
		if($open_dirs = opendir(THEME_FONTS)) {
			while(($font = readdir($open_dirs)) !== false) {
				if(stristr($font, '.js') !== false) {
					$font =  str_replace( '.js', '', $font );
					$cufon[$font] = ucfirst( $font );
				}
			}
		}
	}
	
	asort( $cufon );
	
	return $cufon;
}

/**
 * 
 */
function alli_typography_options() {
	$font = array(
		'Web' => 'Web',
		'Arial, Helvetica, sans-serif' => 'Arial',
		'Verdana, Geneva, Tahoma, sans-serif' => 'Verdana',
		'"Lucida Sans", "Lucida Grande", "Lucida Sans Unicode", sans-serif' => 'Lucida',
		'Georgia, Times, "Times New Roman", serif' => 'Georgia',
		'"Times New Roman", Times, Georgia, serif' => 'Times New Roman',
		'"Trebuchet MS", Tahoma, Arial, sans-serif' => 'Trebuchet',
		'"Courier New", Courier, monospace' => 'Courier New',
		'Impact, Haettenschweiler, "Arial Narrow Bold", sans-serif' => 'Impact',
		'Tahoma, Geneva, Verdana, sans-serif' => 'Tahoma',
		

		'"Acme", sans-serif' => 'Acme',
		'"Aladin", sans-serif' => 'Aladin',
		'"Amethysta", sans-serif' => 'Amethysta',
		'"Boogaloo", sans-serif' => 'Boogaloo',
		'"Chelsea Market", sans-serif' => 'Chelsea Market',
		'"Coming Soon", sans-serif' => 'Coming Soon',
		'"Droid Sans", sans-serif' => 'Droid Sans',
		'"Flamenco", sans-serif' => 'Flamenco',
		'"Galdeano", sans-serif' => 'Galdeano',
		'"Germania One", sans-serif' => 'Germania One',
		'"Gudea", sans-serif' => 'Gudea',
		'"Homenaje", sans-serif' => 'Homenaje',
		'"Inika", sans-serif' => 'Inika',
		'"Istok Web", sans-serif' => 'Istok Web',
		'"Jim Nightshade", sans-serif' => 'Jim Nightshade',
		'"Junge", sans-serif' => 'Junge',
		'"Lora", sans-serif' => 'Lora',
		'"Lusitana", sans-serif' => 'Lusitana',
		'"Lustria", sans-serif' => 'Lustria',
		'"Macondo", sans-serif' => 'Macondo',
		'"Maven Pro", sans-serif' => 'Maven Pro',
		'"Merienda One", sans-serif' => 'Merienda One',
		'"Montez", sans-serif' => 'Montez',
		'"Nunito", sans-serif' => 'Nunito',
		'"Oldenburg", sans-serif' => 'Oldenburg',
		'"Overlock SC", sans-serif' => 'Overlock SC',
		'"Questrial", sans-serif' => 'Questrial',
		'"Qwigley", sans-serif' => 'Qwigley',
		'"Rogue Script", sans-serif' => 'Rogue Script',
		'"Ruda", sans-serif' => 'Ruda',
		'"Ruluko", sans-serif' => 'Ruluko',
		'"Shadows Intro Light", sans-serif' => 'Shadows Intro Light',
		'"Sofia", sans-serif' => 'Sofia',
		'"Telex", sans-serif' => 'Telex',
		'"Viga", sans-serif' => 'Viga',
		'"Yesteryear", sans-serif' => 'Yesteryear',
		
		
		'inherit' =>'Inherit',
		'optgroup' => 'optgroup');
		
	$cufon = alli_cufon_fonts();
		
	if( !empty( $cufon ) ) {
		array_unshift( $cufon, 'Cufon' );
		array_push( $cufon, 'optgroup' );

		$font = array_merge( $font, $cufon );
	}
	
	$size = range( 1,100 );
	$weight = array( 'normal', 'bold' );
	$style = array( 'normal', 'italic', 'oblique' );
		
	$options = array( 'font-size' => $size, 'font-weight' => $weight,  'font-style' => $style, 'font-family' => $font );
	
	return $options;
}

/**
 *
 */
function alli_dependencies( $post_id ) {
	global $alli;
	
	if( !is_admin() ) return;
	
	  if ( empty( $alli->dependencies ) && !empty( $_POST[ALLI_SETTINGS] ) ) {
	    $post = $_POST;
		
		$dependencies = array();
		
		if( strpos( $post['post_content'], '[nivo' ) !== false  ) { $dependencies[] = 'nivo'; }
		
		if( strpos( $post['post_content'], '[galleria' ) !== false ) { $dependencies[] = 'galleria'; }
		
		if( strpos( $post['post_content'], '[tab' ) !== false ) { $dependencies[] = 'tabs'; }
		
		if( strpos( $post['post_content'], '[tooltip' ) !== false ) { $dependencies[] = 'tooltip'; }
		
		if( strpos( $post['post_content'], '[contactform' ) !== false ) { $dependencies[] = 'contactform'; }
		
		if( strpos( $post['post_content'], 'post_content=\"full' ) ) { $dependencies[] = 'all_scripts'; }
		
		$dependencies = serialize( $dependencies );
		update_post_meta( $post_id, '_' . THEME_SLUG .'_dependencies', $dependencies );
	  }
	
	$alli->dependencies = true;
}

/**
 * 
 */
function alli_tinymce_init_size() {
	if( isset( $_GET['page'] ) ) {
		if( $_GET['page'] == 'alli-options' ) {
			$tinymce = 'TinyMCE_' . ALLI_SETTINGS . '_content_size';
			if( !isset( $_COOKIE[$tinymce] ) )
				setcookie($tinymce, 'cw=577&ch=251');
		}
	}
}

/**
 *
 */
function alli_import_options( $import ) {
	
	$imported_options = alli_decode( $import, $serialize = true );
	
	if( is_array( $imported_options ) ) {
		
		if( array_key_exists( 'allimyway_options_export', $imported_options ) ) {
			if( get_option( ALLI_SETTINGS ) != $imported_options ) {

				if( update_option( ALLI_SETTINGS, $imported_options ) )
					wp_redirect( admin_url( 'admin.php?page=alli-options&import=true' ) );
				else
					wp_redirect( admin_url( 'admin.php?page=alli-options&import=false' ) );

			} else {
				wp_redirect( admin_url( 'admin.php?page=alli-options&import=true' ) );
			}
			
		} else {
			wp_redirect( admin_url( 'admin.php?page=alli-options&import=false' ) );
		}
		
	} else {
		wp_redirect( admin_url( 'admin.php?page=alli-options&import=false' ) );
	}
	
	exit;
}

/**
 *
 */
function alli_default_options( $type ) {
	global $alli;
	
	$options = '';
	
	$default_options = 'zVlbb9s2FP4rHAr0KbUl-e5gD8WwoQPWIcACDH0SKImWWVOkyotdr8h_36EkUrLlOMqCrC2C1CbP_XyHPIfB69lk_U2tw2j99osR-jajqmT4GDORi3rlVq2nzaaWhvjFZbNoKWMjmd8Img2_EIbNygbvaSr4E9TOFPJVSxxvCc6IbLXOmt37LVUIfvSWoHrlV0uPPlT06B6462W0oYRlI_RJGJRijhjBe4KoRgnDfIcwz5BRBGFUEG4Q5UqDhJ_65tSGxJLmW-13o3mze8dwStBRGIk-3H_8A6UiI2hLJOlIcq7qA9UaRNHs8ShEbiWRoDeVpkjijDBaUN2JhgvsuF1xucIcs6OmqYqtKVfC7VZSo7Qo4lSpx4lXp7Sfr5CGLlFbUZAS5yQGWAnThi6cNARVRGNFM5Lg1rWFUyW4Jrxlm0y8KgbCo7e5xQJkUqGNkHUGSiPTLVYQeyAZ1zT2YyLR-NLnshbSAupDYzPSAKMRuhcI5PHcwuYGcUBxjjVsCvS3kFkpiVLofVZQju4wJwy9s_LQ71zRnFPcfHUyayQW-FjBTm2F1DZDyopLIdkgOKObDUCHa1THDOljCQQWqqVhDFWVAfgVOSqF0mqE7gDVIE0S4LOCrBOZSA1gWmNNBa-CUwgJLnD4WFSLdXzKp8KT0T1KGVbqZ3dG7KmvSUc0htXqs8-vK40CU14B4DyVfcw48G4kkFY81slmHwPcv9F10K0Od1xp676MNdXMYf0B5DkQ0cLKgjzRf0hsg9lD9MElsq1AZ4zg1nRmCh6XkKyNYFTECW3NiqoT1AlqkdqsLKKgV7DbHtVkGbSGe936IF6kezKNBugOF_OObpc3vZXkZZ5H4XyI9qjruTs2NlDIL1IeziZXlLtTfTG7oLvVpyjPGYk3UHWvnPJw1YebBf__pbaDtOeqHQiykzSHl0D2XMXX8eVT3AV3cAFez1UbRtEAtdP5hSirAgOS7KEdMwq_Bmh18pbBAKWz4IKvBcmoKZ6n1Yc4GAKplqrrLMMSTt3_pHaymg1Ru7xUvTjLqL3dcB1oFeeSZq97cvSbHcVot219vCfmdC9iAvd9qodSg-yUDGm9KmrMaRGrkpBsQM9dm31G7i6jadAFw-KUZQPd8SN8iolDv99s-KCb6jFkQrfuhcuuNxmVECnI7gmjtz8xWsPeVuy7XfK8K8F2IVKwDn-nsbgw6Tx0WtXG5rr9vdBCQ_OWC0k7yfG6HSvWYIHpdEEutfNeT1KxQIN48HZOrZ0hfFhUCD4LZXesWvixRetyPR6Hq2gUzpejYBQGk3EqisJwqo_jQ_muacvGpmQCZ2ocBWE4DlbjRTBfruLJYhGNPpd5b-7DTMdY6ysY9xMi5bvrM58DbLd_60DsXoqSppihDYERiOfqAnC1rbqzOvIBqu7vKkq9INvxKraNcS8hYb_8IB2ppKXFX6fanKg7O3MwZhSSVMFvBrYYdQNtN4PzD3HowjXlKc0g_WhP2A0yTEtby_ZbQz5Cfxqe2j5_TzmWyKZKZAJlhiJFMkQMVbAwQr8ZBRMn8CtNvxiCGPhQVIKIFAr4eA6r8N8eW_Uj5PBM19EQBC0XL0MQ_NC_tEh3cWD_rYIwms_Cj9Vd9L0Adf54gNEZwfeHUxQ4I--kgIlyDz0KjEIasC_tNGcKO94CuiwY9ljSGjKM4QJGO01kBhQFDGsYvGM0ATDAV63BW6IRTmEmVJjDiIkLD7IRek84sasAMqJgxt0Qk1NQykmKFM5pxW9gg4kEmmOH5grtoN7qBmhK4AClOLXmOS5NCuinO4dp494bf6rNn4Lio0B4fchcuSovQuRkNzsDydXEn2l66JA3IdmRo-qZG95EN29aHofDqqe1nciApqKiPX-difqbUf9Wa5q7HQHMZ6YTN3djapy0XKsulz1BAM-x0kfWuTAvMs7bcR8nrOkpn3OBe1chudXjJuVPv3LVxLikQ14shLBPevVEoXrFP73wGFpzaP9QaZPqVP8iymP1Lobe4qK8TeHrbXOAf7IvXAeSKKobDxGctxHUMGOo4oH7hygi9yTzh35bCEqk1MbQh27heoqpLUPnj32n7YNhg1OSCLEblTzvIRF8F_33u4xsMFxzfn1-8oT4eGSnncLtI6i5k7w9qRfVXG9XnJl4y6CPpgIOsO_kzXnF0_XkuuW-PyZ7irnGUv84pk-vmr70BZznP47Ns-vh9tc94GQnfxSzL92gAzCTVEfDDg6NAY7M_OGP091rueHITq41fzzae21yM72ZdW-39q8b9rU9HvCnJx8U24ToWFR37pXZ2b-82qa5OB7w0fHE5KsV0VAmcGQ-_As';
	
	
	$widgets = '';
		
	
	# Default theme settings
	if( $type == 'settings' ) {
		# Set to "false" to create initial export
		$include_images = true;
		
		# Decode options and unserialize
		$default_options = alli_decode( $default_options, $serialize = true );
		foreach( $default_options as $key => $value )
			if( is_array( $value ) )
				foreach( $value as $key2 => $value2 )
					$default_options[$key][$key2] = str_replace( '%site_url%', THEME_IMAGES . '/activation', $value2 );
		
		if( $include_images ) {
			# Add default image sizes to options array 
			foreach( $alli->layout['images'] as $img_key_full => $image_full ) {
				$image_sizes_full['w'] = $image_full[0];
				$image_sizes_full['h'] = $image_full[1];
				$images_full["${img_key_full}_full"] = $image_sizes_full;
			}

			foreach( $alli->layout['big_sidebar_images'] as $img_key_big => $image_big ) {
				$image_sizes_big['w'] = $image_big[0];
				$image_sizes_big['h'] = $image_big[1];
				$images_big["${img_key_big}_big"] = $image_sizes_big;
			}

			foreach( $alli->layout['small_sidebar_images'] as $img_key_small => $image_small ) {
				$image_sizes_small['w'] = $image_small[0];
				$image_sizes_small['h'] = $image_small[1];
				$images_small["${img_key_small}_small"] = $image_sizes_small;
			}

			# Merge default options & images sizes 
			$image_merge1 = array_merge( $default_options, $images_full );
			$image_merge2 = array_merge( $image_merge1, $images_big );
			$options = array_merge( $image_merge2, $images_small );
			
		} else {
			$options = $default_options;
		}
	}
	
	# Interanl framework settings
	if( $type == 'internal' ) {
		$options = array();
		
		if( defined( 'FRAMEWORK_VERSION' ) )
			$options['framework_version'] = FRAMEWORK_VERSION;
			
		if( defined( 'DOCUMENTATION_URL' ) )
			$options['documentation_url'] = DOCUMENTATION_URL;
			
		if( defined( 'SUPPORT_URL' ) )
			$options['support_url'] = SUPPORT_URL;
	}
	
	# Default activation widgets
	if( $type == 'widgets' ) {
		
		
		return;
	}
		
	
	return $options;
}

?>
