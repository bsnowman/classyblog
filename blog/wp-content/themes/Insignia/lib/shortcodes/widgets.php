<?php
/**
 *
 */
class alliWidgets {

	/**
	 *
	 */
	function twitter( $atts = null ) {
		if( $atts == 'generator' ) {
			$numbers = range(1,20);
			foreach( $numbers as $val )
				$number[$val] = $val;

			$option = array( 
				'name' => __( 'Twitter', ALLI_ADMIN_TEXTDOMAIN ),
				'value' => 'twitter',
				'options' => array(
					array(
						'name' => __( 'Username', ALLI_ADMIN_TEXTDOMAIN ),
						'desc' => __( 'Paste your twitter username here.  You can find your username by going to your settings page within twitter.', ALLI_ADMIN_TEXTDOMAIN ),
						'id' => 'id',
						'default' => '',
						'type' => 'text'
					),
					array(
						'name' => __( 'Count', ALLI_ADMIN_TEXTDOMAIN ),
						'desc' => __( 'Select how many tweets you want to be displayed.', ALLI_ADMIN_TEXTDOMAIN ),
						'id' => 'number',
						'default' => '',
						'options' => $number,
						'type' => 'select'
					),
				'shortcode_has_atts' => true,
				)
			);

			return $option;
		}
		
		$defaults = array(
			'id' 		=> '',
			'number'	=> '1',
			'title' 	=> ' '
		);
		
		if( isset( $atts['count'] ) )
			$atts['number'] = $atts['count'];
		
		if( isset( $atts['username'] ) )
			$atts['id'] = $atts['username'];
			
		if( empty( $atts['id'] ) )
			$atts['id'] = alli_get_setting( 'twitter_id' );
			
		$atts = wp_parse_args( $atts, $defaults );
		
		$instance = http_build_query( $atts );

		$args = array( 'widget_name' => 'AlliaseW_Twitter_Widget', 'instance' => $instance );
		
		$widget = new alliWidgets();
		return $widget->_widget_generator( $args );
	}
	
	/**
	 *
	 */
	function flickr( $atts = null ) {
		if( $atts == 'generator' ) {
			$numbers = range(1,20);
			foreach( $numbers as $val )
				$number[$val] = $val;

			$option = array( 
				'name' => __( 'Flickr', ALLI_ADMIN_TEXTDOMAIN ),
				'value' => 'flickr',
				'options' => array(
					array(
						'name' => __( 'Flickr id (<a target="_blank" href="http://idgettr.com/">idGettr</a>)', ALLI_ADMIN_TEXTDOMAIN ),
						'desc' => __( 'Set your Flickr ID here.  You can use the idGettr service to easily find your ID.', ALLI_ADMIN_TEXTDOMAIN ),
						'id' => 'id',
						'default' => '',
						'type' => 'text'
					),
					array(
						'name' => __( 'Count', ALLI_ADMIN_TEXTDOMAIN ),
						'desc' => __( 'Select how many flickr images you wish to display.', ALLI_ADMIN_TEXTDOMAIN ),
						'id' => 'count',
						'default' => '',
						'options' => $number,
						'type' => 'select'
					),
					array(
						'name' => __( 'Size', ALLI_ADMIN_TEXTDOMAIN ),
						'desc' => __( 'Set the size of your flickr images.<br /><br />Each setting will display differently so try and experiment with them to find which one suits you best.', ALLI_ADMIN_TEXTDOMAIN ),
						'id' => 'size',
						'default' => '',
						'options' => array(
							's' => __('Square', ALLI_ADMIN_TEXTDOMAIN ),
							't' => __('Thumbnail', ALLI_ADMIN_TEXTDOMAIN ),
							'm' => __('Medium', ALLI_ADMIN_TEXTDOMAIN )
						),
						'type' => 'select'
					),
					array(
						'name' => __('Display', ALLI_ADMIN_TEXTDOMAIN ),
						'desc' => __( 'Select whether you want your latest images to display or a random selection.', ALLI_ADMIN_TEXTDOMAIN ),
						'id' => 'display',
						'default' => '',
						'options' => array(
							'latest' => __('Latest', ALLI_ADMIN_TEXTDOMAIN ),
							'random' => __('Random', ALLI_ADMIN_TEXTDOMAIN )
						),
						'type' => 'select'
					),
				'shortcode_has_atts' => true,
				)
			);

			return $option;
		}
		
		$defaults = array(
			'id' 		=> '44071822@N08',
			'number'	=> '9',
			'display'	=> 'latest',
			'size'		=> 's',
			'title' 	=> ' '
		);
		
		$atts = wp_parse_args( $atts, $defaults );
		
		$instance = http_build_query( $atts );

		$args = array( 'widget_name' => 'AlliaseW_Flickr_Widget', 'instance' => $instance );
		
		$widget = new alliWidgets();
		return $widget->_widget_generator( $args );
	}
	
	/**
	 *
	 */
	function recent_posts( $atts = null ) {
		if( $atts == 'generator' ) {
			$numbers = range(1,20);
			foreach( $numbers as $val )
				$number[$val] = $val;

			$option = array( 
				'name' => __( 'Recent Posts', ALLI_ADMIN_TEXTDOMAIN ),
				'value' => 'recent_posts',
				'options' => array(
					array(
						'name' => __( 'Number', ALLI_ADMIN_TEXTDOMAIN ),
						'desc' => __( 'Select the number of posts you wish to display.', ALLI_ADMIN_TEXTDOMAIN ),
						'id' => 'number',
						'default' => '',
						'options' => $number,
						'type' => 'select'
					),
					array(
						'name' => __( 'Thumbnail', ALLI_ADMIN_TEXTDOMAIN ),
						'desc' => __( 'Choose whether you want thumbnails to display alongside your posts.  The thumbnail uses the featured image of your post.', ALLI_ADMIN_TEXTDOMAIN ),
						'id' => 'disable_thumb',
						'default' => '',
						'options' => array(
							'0' => __( 'Yes', ALLI_ADMIN_TEXTDOMAIN ),
							'1' => __( 'No', ALLI_ADMIN_TEXTDOMAIN )
						),
						'type' => 'select'
					),
				'shortcode_has_atts' => true,
				)
			);

		return $option;
		
		}
		
		$defaults = array(
			'number'	=> '',
			'disable_thumb'	=> '',
			'title' 	=> ' '
		);
		
		$atts = wp_parse_args( $atts, $defaults );
		
		$instance = http_build_query( $atts );

		$args = array( 'widget_name' => 'AlliaseW_RecentPost_Widget', 'instance' => $instance );
		
		$widget = new alliWidgets();
		return $widget->_widget_generator( $args );
	}
	
	/**
	 *
	 */
	function popular_posts( $atts = null ) {
		if( $atts == 'generator' ) {
			$numbers = range(1,20);
			foreach( $numbers as $val )
				$number[$val] = $val;

			$option = array( 
				'name' => __( 'Popular Posts', ALLI_ADMIN_TEXTDOMAIN ),
				'value' => 'popular_posts',
				'options' => array(
					array(
						'name' => __( 'Number', ALLI_ADMIN_TEXTDOMAIN ),
						'desc' => __( 'Select the number of posts you wish to display.', ALLI_ADMIN_TEXTDOMAIN ),
						'id' => 'number',
						'default' => '',
						'options' => $number,
						'type' => 'select'
					),
					array(
						'name' => __( 'Thumbnail', ALLI_ADMIN_TEXTDOMAIN ),
						'desc' => __( 'Choose whether you want thumbnails to display alongside your posts.  The thumbnail uses the featured image of your post.', ALLI_ADMIN_TEXTDOMAIN ),
						'id' => 'disable_thumb',
						'default' => '',
						'options' => array(
							'0' => __( 'Yes', ALLI_ADMIN_TEXTDOMAIN ),
							'1' => __( 'No', ALLI_ADMIN_TEXTDOMAIN )
						),
						'type' => 'select'
					),
				'shortcode_has_atts' => true,
				)
			);

		return $option;
		
		}
		
		$defaults = array(
			'number'	=> '',
			'disable_thumb'	=> '',
			'title' 	=> ' '
		);
		
		$atts = wp_parse_args( $atts, $defaults );
		
		$instance = http_build_query( $atts );

		$args = array( 'widget_name' => 'AlliaseW_PopularPost_Widget', 'instance' => $instance );
		
		$widget = new alliWidgets();
		return $widget->_widget_generator( $args );
	}
	
	/**
	 *
	 */
	function contact_info( $atts = null ) {
		if( $atts == 'generator' ) {
			$option = array( 
				'name' => __( 'Contact Info', ALLI_ADMIN_TEXTDOMAIN ),
				'value' => 'contact_info',
				'options' => array(
					array(
						'name' => __( 'Name', ALLI_ADMIN_TEXTDOMAIN ),
						'desc' => __( 'Type in your name.', ALLI_ADMIN_TEXTDOMAIN ),
						'id' => 'name',
						'default' => '',
						'type' => 'text'
					),
					array(
						'name' => __( 'Phone', ALLI_ADMIN_TEXTDOMAIN ),
						'desc' => __( 'Type in your phone number.', ALLI_ADMIN_TEXTDOMAIN ),
						'id' => 'phone',
						'default' => '',
						'type' => 'text'
					),
					array(
						'name' => __( 'Email', ALLI_ADMIN_TEXTDOMAIN ),
						'desc' => __( 'Type in your email address.', ALLI_ADMIN_TEXTDOMAIN ),
						'id' => 'email',
						'default' => '',
						'type' => 'text'
					),
					array(
						'name' => __( 'Address', ALLI_ADMIN_TEXTDOMAIN ),
						'desc' => __( 'Type in your address.', ALLI_ADMIN_TEXTDOMAIN ),
						'id' => 'address',
						'default' => '',
						'type' => 'text'
					),
					array(
						'name' => __( 'City', ALLI_ADMIN_TEXTDOMAIN ),
						'desc' => __( 'Type in your city.', ALLI_ADMIN_TEXTDOMAIN ),
						'id' => 'city',
						'default' => '',
						'type' => 'text'
					),
					array(
						'name' => __( 'State', ALLI_ADMIN_TEXTDOMAIN ),
						'desc' => __( 'Type in your state.', ALLI_ADMIN_TEXTDOMAIN ),
						'id' => 'state',
						'default' => '',
						'type' => 'text'
					),
					array(
						'name' => __( 'Zip', ALLI_ADMIN_TEXTDOMAIN ),
						'desc' => __( 'Type in your zip.', ALLI_ADMIN_TEXTDOMAIN ),
						'id' => 'zip',
						'default' => '',
						'type' => 'text'
					),
				'shortcode_has_atts' => true,
				)
			);

			return $option;
		}
		
		$defaults = array(
			'name'          => '',
			'address' 	=> '',
			'city'          => '',
			'state'         => '',
			'zip'           => '',
			'phone'         => '',
			'email'         => '',
			'title'         => ' '
		);
		
		$atts = wp_parse_args( $atts, $defaults );
		
		$instance = http_build_query( $atts );

		$args = array( 'widget_name' => 'AlliaseW_Contact_Widget', 'instance' => $instance );
		
		$widget = new alliWidgets();
		return $widget->_widget_generator( $args );
	}
	
	/**
	 *
	 */
	function comments( $atts = null ) {
		if( $atts == 'generator' ) {
			$numbers = range(1,20);
			foreach( $numbers as $val )
				$number[$val] = $val;

			$option = array( 
				'name' => __( 'Comments', ALLI_ADMIN_TEXTDOMAIN ),
				'value' => 'comments',
				'options' => array(
					array(
						'name' => __( 'Number', ALLI_ADMIN_TEXTDOMAIN ),
						'desc' => __( 'Select the number of comments you wish to display.', ALLI_ADMIN_TEXTDOMAIN ),
						'id' => 'number',
						'default' => '',
						'options' => $number,
						'type' => 'select'
					),
				'shortcode_has_atts' => true,
				)
			);

			return $option;
		}
			
		$defaults = array(
			'title' => ' ',
			'number' => '5'
		);
		
		$atts = wp_parse_args( $atts, $defaults );
		
		$instance = http_build_query( $atts );

		$args = array( 'widget_name' => 'WP_Widget_Recent_Comments', 'instance' => $instance );
		
		$widget = new alliWidgets();
		return $widget->_widget_generator( $args );
	}
	
	/**
	 *
	 */
	function tags( $atts = null ) {
		if( $atts == 'generator' ) {
			$option = array( 
				'name' => __( 'Tags', ALLI_ADMIN_TEXTDOMAIN ),
				'value' => 'tags',
				'options' => array(
					array(
						'name' => __( 'Taxonomy', ALLI_ADMIN_TEXTDOMAIN ),
						'desc' => __( 'Select whether you wish to display categories or tags.', ALLI_ADMIN_TEXTDOMAIN ),
						'id' => 'taxonomy',
						'default' => '',
						'options' => array(
							'post_tag' => __( 'Post Tags', ALLI_ADMIN_TEXTDOMAIN ),
							'category' => __( 'Category', ALLI_ADMIN_TEXTDOMAIN )
						),
						'type' => 'select'
					),
				'shortcode_has_atts' => true,
				)
			);

			return $option;
		}
		
		$defaults = array(
			'title' => ' ',
			'taxonomy' => 'post_tag'
		);
			
		$atts = wp_parse_args( $atts, $defaults );
		
		$instance = http_build_query( $atts );

		$args = array( 'widget_name' => 'WP_Widget_Tag_Cloud', 'instance' => $instance );
		
		$widget = new alliWidgets();
		return $widget->_widget_generator( $args );
	}

	/**
	 *
	 */
	function rss( $atts = null ) {
		if( $atts == 'generator' ) {
			$numbers = range(1,20);
			foreach( $numbers as $val ) {
				$number[$val] = $val;
			}

			$option = array( 
				'name' => __( 'Rss', ALLI_ADMIN_TEXTDOMAIN ),
				'value' => 'rss',
				'options' => array(
					array(
						'name' => __( 'RSS feed URL ', ALLI_ADMIN_TEXTDOMAIN ),
						'desc' => __( 'Paste the URL to your feed.  For example if you are using feedburner then you would paste something like this,<br /><br />http://feeds.feedburner.com/username', ALLI_ADMIN_TEXTDOMAIN ),
						'id' => 'url',
						'default' => '',
						'type' => 'text'
					),
					array(
						'name' => __( 'How many items would you like to display?', ALLI_ADMIN_TEXTDOMAIN ),
						'desc' => __( 'Select the number of RSS items you wish to display.', ALLI_ADMIN_TEXTDOMAIN ),
						'id' => 'items',
						'default' => '',
						'options' => $number,
						'type' => 'select'
					),
					array(
						'name' => __( 'Show Summary', ALLI_ADMIN_TEXTDOMAIN ),
						'desc' => __( 'Check this if you wish to display a summary of the item.', ALLI_ADMIN_TEXTDOMAIN ),
						'id' => 'show_summary',
						'options' => array( '1' => __( 'Show Summary', ALLI_ADMIN_TEXTDOMAIN )),
						'default' => '',
						'type' => 'checkbox'
					),
					array(
						'name' => __( 'Show Author', ALLI_ADMIN_TEXTDOMAIN ),
						'desc' => __( 'Check if you wish to display the author of the item.', ALLI_ADMIN_TEXTDOMAIN ),
						'id' => 'show_author',
						'options' => array( '1' => __( 'Show Author', ALLI_ADMIN_TEXTDOMAIN )),
						'default' => '',
						'type' => 'checkbox'
					),
					array(
						'name' => __( 'Show Date', ALLI_ADMIN_TEXTDOMAIN ),
						'desc' => __( 'Check if you wish to display the date of the item.', ALLI_ADMIN_TEXTDOMAIN ),
						'id' => 'show_date',
						'options' => array( '1' => __( 'Show Date', ALLI_ADMIN_TEXTDOMAIN )),
						'default' => '',
						'type' => 'checkbox'
					),
				'shortcode_has_atts' => true,
				)
			);
			
			return $option;
		}
		
		$defaults = array(
			'title' => ' ',
			'url' => '',
			'items' => 3,
			'error' => false,
			'show_summary' => 0,
			'show_author' => 0,
			'show_date' => 0
		);
		
		$atts = wp_parse_args( $atts, $defaults );
		
		$instance = http_build_query( $atts );

		$args = array( 'widget_name' => 'WP_Widget_RSS', 'instance' => $instance );
		
		$widget = new alliWidgets();
		return $widget->_widget_generator( $args );
	}
	
	/**
	 *
	 */
	function search( $atts = null ) {
		
		if( $atts == 'generator' ) {
			$option = array( 
				'name' => __( 'Search', ALLI_ADMIN_TEXTDOMAIN ),
				'value' => 'search'
			);

			return $option;
		}
		
		$defaults = array( 'title' => ' ' );
		
		$atts = wp_parse_args( $atts, $defaults );
		
		$instance = http_build_query( $atts );

		$args = array( 'widget_name' => 'WP_Widget_Search', 'instance' => $instance );
		
		$widget = new alliWidgets();
		return $widget->_widget_generator( $args );
	}

	/**
	 *
	 */
	function _widget_generator( $args = array() ) {
		global $wp_widget_factory;
		
		$widget_name = esc_html( $args['widget_name'] );

		ob_start();
		the_widget( $widget_name, $args['instance'], array( 'before_title' => '', 'after_title' => '', 'widget_id' => '-1' ) );
		$out = ob_get_contents();
		ob_end_clean();
		return $out;
	}

	/**
	 *
	 */
	function _options( $class ) {
		$shortcode = array();
		
		$class_methods = get_class_methods( $class );
		
		foreach( $class_methods as $method ) {
			if( $method[0] != '_' ) {
				$shortcode[] = call_user_func(array( &$class, $method ), $atts = 'generator' );
			}
		}
		
		$options = array(
			'name' => __( 'Widget', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'Select which widget shortcode you would like to use.', ALLI_ADMIN_TEXTDOMAIN ),
			'value' => 'widget',
			'options' => $shortcode,
			'shortcode_has_types' => true
		);
		
		return $options;
	}
	
}

?>
