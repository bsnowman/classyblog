<?php
/**
 *
 */
class alliLists {
	
	/**
	 *
	 */
	function styled_list( $atts = null, $content = null ) {
		if( $atts == 'generator' ) {
			$option = array( 
				'name' => __( 'Styled List', ALLI_ADMIN_TEXTDOMAIN ),
				'value' => 'styled_list',
				'options' => array(
					array(
						'name' => __( 'Type', ALLI_ADMIN_TEXTDOMAIN ),
						'desc' => __( 'Choose the style of list that you wish to use.  Each one has a different icon.', ALLI_ADMIN_TEXTDOMAIN ),
						'id' => 'style',
						'default' => '',
						'options' => array(
							'arrow_list' => __( 'Arrow List', ALLI_ADMIN_TEXTDOMAIN ),
							'bullet_list' => __( 'Bullet List', ALLI_ADMIN_TEXTDOMAIN ),
							'check_list' => __( 'Check List', ALLI_ADMIN_TEXTDOMAIN ),
							'circle_arrow' => __( 'Circle Arrow', ALLI_ADMIN_TEXTDOMAIN ),
							'triangle_arrow' => __( 'Triangle Arrow', ALLI_ADMIN_TEXTDOMAIN ),
							'comment_list' => __('Comment List', ALLI_ADMIN_TEXTDOMAIN ),
							'minus_list' => __( 'Minus List', ALLI_ADMIN_TEXTDOMAIN ),
							'plus_list' => __( 'Plus List', ALLI_ADMIN_TEXTDOMAIN ),
							'star_list' => __( 'Star List', ALLI_ADMIN_TEXTDOMAIN )
						),
						'type' => 'select'
					),
					array(
						'name' => __( 'List Html', ALLI_ADMIN_TEXTDOMAIN ),
						'desc' => __( 'Type out the content of your list.  You need to use the &#60;ul&#62; and &#60;li&#62; elements when typing out your list content.', ALLI_ADMIN_TEXTDOMAIN ),
						'id' => 'content',
						'default' => '',
						'type' => 'textarea',
						'return' => true
					),
					array(
						'name' => __( 'Color Variation <small>(optional)</small>', ALLI_ADMIN_TEXTDOMAIN ),
						'desc' => __( 'Choose one of our predefined color skins to use with your list.', ALLI_ADMIN_TEXTDOMAIN ),
						'id' => 'variation',
						'default' => '',
						'target' => 'color_variations',
						'type' => 'select'
					),
				'shortcode_has_atts' => true,
				'shortcode_carriage_return' => true
				)
			);
			
			return $option;
		}
		
		extract(shortcode_atts(array(
			'style'     => '',
			'variation'	=> '',
	    ), $atts));
	
		$style = ( $style ) ? trim( $style ) : 'arrow_list';
	
		$variation = ( $variation ) ? ' ' . trim( $variation ) . '_sprite' : '';

		$content = str_replace( '<ul>', '<ul class="styled_list">', $content );
		$content = str_replace( '<li>', '<li class="' . $style . $variation . '">', $content );
	
		return alli_remove_wpautop( $content );
	}

	
	/**
	 *
	 */
	function _options( $class ) {
		$shortcode = array();
		
		$class_methods = get_class_methods( $class );
		
		foreach( $class_methods as $method ) {
			if( $method[0] != '_' )
				$shortcode[] = call_user_func(array( &$class, $method ), $atts = 'generator' );
		}
		
		$options = array(
			'name' => __( 'Styled List', ALLI_ADMIN_TEXTDOMAIN ),
			'value' => 'styled_list',
			'options' => $shortcode
		);
		
		return $options;
	}
	
}

?>