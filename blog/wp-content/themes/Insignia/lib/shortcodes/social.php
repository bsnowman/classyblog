<?php
/**
 *
 */
class alliSocial {

	/**
	 *  ReTweet button
	 */
	function tweet( $atts = null, $content = null ) {
	
		if( $atts == 'generator' ) {
			$option = array( 
				"name" => __( "Twitter", ALLI_ADMIN_TEXTDOMAIN ),
				"value" => "tweet",
				"options" => array(
					array(
						"name" => __( "Twitter Username", ALLI_ADMIN_TEXTDOMAIN ),
						"id" => "username",
						"desc" => __( 'Type out your twitter username here.  You can find your twitter username by logging into your twitter account.', ALLI_ADMIN_TEXTDOMAIN ),
						"default" => "",
						"type" => "text"
					),
					array(
						"name" => __( "Tweet Position", ALLI_ADMIN_TEXTDOMAIN ),
						"id" => "layout",
						"desc" => __( 'Choose whether you want your tweets to display vertically, horizontally, or none at all.', ALLI_ADMIN_TEXTDOMAIN ),
						"default" => "",
						"options" => array(
							"vertical" => __( "Vertical", ALLI_ADMIN_TEXTDOMAIN ),
							"horizontal" => __( "Horizontal", ALLI_ADMIN_TEXTDOMAIN ),
							"none" => __( "None", ALLI_ADMIN_TEXTDOMAIN ),
						),
						"type" => "select"
					),
					array(
						"name" => __( "Custom Text", ALLI_ADMIN_TEXTDOMAIN ),
						"id" => "text",
						"desc" => __( 'This is the text that people will include in their Tweet when they share from your website.', ALLI_ADMIN_TEXTDOMAIN ),
						"default" => "",
						"type" => "text"
					),
					array(
						"name" => __( "Custom URL", ALLI_ADMIN_TEXTDOMAIN ),
						"id" => "url",
						"desc" => __( 'By default the URL from your page will be used but you can input a custom URL here.', ALLI_ADMIN_TEXTDOMAIN ),
						"default" => "",
						"type" => "text"
					),
					array(
						"name" => __( "Related Users", ALLI_ADMIN_TEXTDOMAIN ),
						"id" => "related",
						"desc" => __( 'You can input another twitter username for recommendation.', ALLI_ADMIN_TEXTDOMAIN ),
						"default" => "",
						"type" => "text"
					),
					array(
						"name" => __( "Language", ALLI_ADMIN_TEXTDOMAIN ),
						"id" => "lang",
						"desc" => __( 'Select which language you would like to display the button in.', ALLI_ADMIN_TEXTDOMAIN ),
						"default" => "",
						"options" => array(
							"fr" => __( "French", ALLI_ADMIN_TEXTDOMAIN ),
							"de" => __( "German", ALLI_ADMIN_TEXTDOMAIN ),
							"it" => __( "Italian", ALLI_ADMIN_TEXTDOMAIN ),
							"ja" => __( "Japanese", ALLI_ADMIN_TEXTDOMAIN ),
							"ko" => __( "Korean", ALLI_ADMIN_TEXTDOMAIN ),
							"ru" => __( "Russian", ALLI_ADMIN_TEXTDOMAIN ),
							"es" => __( "Spanish", ALLI_ADMIN_TEXTDOMAIN ),
							"tr" => __( "Turkish", ALLI_ADMIN_TEXTDOMAIN ),
						),
						"type" => "select"
					),
					"shortcode_has_atts" => true,
				)
			);

			return $option;
		}
		
		extract(shortcode_atts(array(
			'layout'        => 'vertical',
			'username'		  => '',
			'text' 			  => '',
			'url'			  => '',
			'related'		  => '',
			'lang'			  => '',
	    	), $atts));
	    	
	    if ($text != '') { $text = "data-text='".$text."'"; }
	    if ($url != '') { $url = "data-url='".$url."'"; }
	    if ($related != '') { $related = "data-related='".$related."'"; }
	    if ($lang != '') { $lang = "data-lang='".$lang."'"; }
		
		$out = '<div class = "alli_sociable"><a href="http://twitter.com/share" class="twitter-share-button" '.$url.' '.$lang.' '.$text.' '.$related.' data-count="'.$layout.'" data-via="'.$username.'">Tweet</a>';
		$out .= '<script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script></div>';
		
		return $out;
	}
	
	/**
	 *  Facebook Like button
	 */
	function fblike( $atts = null, $content = null ) {
	
		if( $atts == 'generator' ) {
			$option = array( 
				"name" => __( "Facebook Like", ALLI_ADMIN_TEXTDOMAIN ),
				"value" => "fblike",
				"options" => array(
					array(
						"name" => __( "Layout", ALLI_ADMIN_TEXTDOMAIN ),
						"id" => "layout",
						"desc" => __( 'Choose the layout you would like to use with your facebook button.', ALLI_ADMIN_TEXTDOMAIN ),
						"default" => "",
						"options" => array(
							"standard" => __( "Standard", ALLI_ADMIN_TEXTDOMAIN ),
							"box_count" => __( "Box Count", ALLI_ADMIN_TEXTDOMAIN ),
							"button_count" => __( "Button Count", ALLI_ADMIN_TEXTDOMAIN ),
						),
						"type" => "select"
					),
					array(
						"name" => __( "Show Faces", ALLI_ADMIN_TEXTDOMAIN ),
						"id" => "show_faces",
						"desc" => __( 'Choose whether to display faces or not.', ALLI_ADMIN_TEXTDOMAIN ),
						"default" => "",
						"options" => array(
							"true" => __( "Yes", ALLI_ADMIN_TEXTDOMAIN ),
							"false" => __( "No", ALLI_ADMIN_TEXTDOMAIN ),
						),
						"type" => "select"
					),
					array(
						"name" => __( "Action", ALLI_ADMIN_TEXTDOMAIN ),
						"id" => "action",
						"desc" => __( 'This is the text that gets displayed on the button.', ALLI_ADMIN_TEXTDOMAIN ),
						"default" => "",
						"options" => array(
							"like" => __( "Like", ALLI_ADMIN_TEXTDOMAIN ),
							"recommend" => __( "Recommend", ALLI_ADMIN_TEXTDOMAIN ),
						),
						"type" => "select"
					),
					array(
						"name" => __( "Font", ALLI_ADMIN_TEXTDOMAIN ),
						"id" => "font",
						"desc" => __( 'Select which font you would like to use.', ALLI_ADMIN_TEXTDOMAIN ),
						"default" => "",
						"options" => array(
							"lucida+grande" => __( "Lucida Grande", ALLI_ADMIN_TEXTDOMAIN ),
							"arial" => __( "Arial", ALLI_ADMIN_TEXTDOMAIN ),
							"segoe+ui" => __( "Segoe Ui", ALLI_ADMIN_TEXTDOMAIN ),
							"tahoma" => __( "Tahoma", ALLI_ADMIN_TEXTDOMAIN ),
							"trebuchet+ms" => __( "Trebuchet MS", ALLI_ADMIN_TEXTDOMAIN ),
							"verdana" => __( "Verdana", ALLI_ADMIN_TEXTDOMAIN ),
						),
						"type" => "select"
					),
					array(
						"name" => __( "Color Scheme", ALLI_ADMIN_TEXTDOMAIN ),
						"id" => "colorscheme",
						"desc" => __( 'Select the color scheme you would like to use.', ALLI_ADMIN_TEXTDOMAIN ),
						"default" => "",
						"options" => array(
							"light" => __( "Light", ALLI_ADMIN_TEXTDOMAIN ),
							"dark" => __( "Dark", ALLI_ADMIN_TEXTDOMAIN ),
						),
						"type" => "select"
					),
					"shortcode_has_atts" => true,
				)
			);

			return $option;
		}
		
		extract(shortcode_atts(array(
				'layout'		=> 'box_count',
				'width'			=> '',
				'height'		=> '',
				'show_faces'	=> 'false',
				'action'		=> 'like',
				'font'			=> 'lucida+grande',
				'colorscheme'	=> 'light',
	    	), $atts));
		
	    global $wpdb;
	    
	    if ($layout == 'standard') { $width = '450'; $height = '35';  if ($show_faces == 'true') { $height = '80'; } }
	    if ($layout == 'box_count') { $width = '55'; $height = '65'; }
	    if ($layout == 'button_count') { $width = '90'; $height = '20'; }
	    	
		$out = '<div class = "alli_sociable"><iframe src="http://www.facebook.com/plugins/like.php?href='.get_permalink();
		$out .= '&layout='.$layout.'&show_faces=false&width='.$width.'&action='.$action.'&font='.$font.'&colorscheme='.$colorscheme.'"';
		$out .= 'allowtransparency="true" style="border: medium none; overflow: hidden; width: '.$width.'px; height: '.$height.'px;"';
		$out .= 'frameborder="0" scrolling="no"></iframe></div>';
		
		return $out;
	}
	
	/**
	 *  Google Buzz button
	 */
	function googlebuzz( $atts = null, $content = null ) {
	
		if( $atts == 'generator' ) {
			$option = array( 
				"name" => __( "Google Buzz", ALLI_ADMIN_TEXTDOMAIN ),
				"value" => "googlebuzz",
				"options" => array(
					array(
						"name" => __( "Layout", ALLI_ADMIN_TEXTDOMAIN ),
						"id" => "layout",
						"desc" => __( 'Choose how you would like to display the google buzz button.', ALLI_ADMIN_TEXTDOMAIN ),
						"default" => "",
						"options" => array(
							"normal-count" => __( "Count", ALLI_ADMIN_TEXTDOMAIN ),
							"normal-button" => __( "Button", ALLI_ADMIN_TEXTDOMAIN ),
							"link" => __( "Link", ALLI_ADMIN_TEXTDOMAIN ),
						),
						"type" => "select"
					),
					array(
						"name" => __( "Custom URL", ALLI_ADMIN_TEXTDOMAIN ),
						"id" => "url",
						"desc" => __( 'In case you wish to use a different URL you can input it here.', ALLI_ADMIN_TEXTDOMAIN ),
						"default" => "",
						"type" => "text"
					),
					"shortcode_has_atts" => true,
					array(
						"name" => __( "Custom Image", ALLI_ADMIN_TEXTDOMAIN ),
						"id" => "imageurl",
						"desc" => __( 'In case you wish to use a different image you can paste the URL here.', ALLI_ADMIN_TEXTDOMAIN ),
						"default" => "",
						"type" => "text"
					),
					array(
						"name" => __( "Language", ALLI_ADMIN_TEXTDOMAIN ),
						"id" => "lang",
						"desc" => __( 'Select which language you would like to display the button in.', ALLI_ADMIN_TEXTDOMAIN ),
						"default" => "",
						"options" => array(
							"ar" => __( "Arabic", ALLI_ADMIN_TEXTDOMAIN ),
							"bn" => __( "Bengali", ALLI_ADMIN_TEXTDOMAIN ),
							"bg" => __( "Bulgarian", ALLI_ADMIN_TEXTDOMAIN ),
							"ca" => __( "Catalan", ALLI_ADMIN_TEXTDOMAIN ),
							"zh" => __( "Chinese", ALLI_ADMIN_TEXTDOMAIN ),
							"zh_CN" => __( "Chinese (China)", ALLI_ADMIN_TEXTDOMAIN ),
							"zh_HK" => __( "Chinese (Hong Kong)", ALLI_ADMIN_TEXTDOMAIN ),
							"zh_TW" => __( "Chinese (Taiwan)", ALLI_ADMIN_TEXTDOMAIN ),
							"hr" => __( "Croation", ALLI_ADMIN_TEXTDOMAIN ),
							"cs" => __( "Czech", ALLI_ADMIN_TEXTDOMAIN ),
							"da" => __( "Danish", ALLI_ADMIN_TEXTDOMAIN ),
							"nl" => __( "Dutch", ALLI_ADMIN_TEXTDOMAIN ),
							"en_IN" => __( "English (India)", ALLI_ADMIN_TEXTDOMAIN ),
							"en_IE" => __( "English (Ireland)", ALLI_ADMIN_TEXTDOMAIN ),
							"en_SG" => __( "English (Singapore)", ALLI_ADMIN_TEXTDOMAIN ),
							"en_ZA" => __( "English (South Africa)", ALLI_ADMIN_TEXTDOMAIN ),
							"en_GB" => __( "English (United Kingdom)", ALLI_ADMIN_TEXTDOMAIN ),
							"fil" => __( "Filipino", ALLI_ADMIN_TEXTDOMAIN ),
							"fi" => __( "Finnish", ALLI_ADMIN_TEXTDOMAIN ),
							"fr" => __( "French", ALLI_ADMIN_TEXTDOMAIN ),
							"de" => __( "German", ALLI_ADMIN_TEXTDOMAIN ),
							"de_CH" => __( "German (Switzerland)", ALLI_ADMIN_TEXTDOMAIN ),
							"el" => __( "Greek", ALLI_ADMIN_TEXTDOMAIN ),
							"gu" => __( "Gujarati", ALLI_ADMIN_TEXTDOMAIN ),
							"iw" => __( "Hebrew", ALLI_ADMIN_TEXTDOMAIN ),
							"hi" => __( "Hindi", ALLI_ADMIN_TEXTDOMAIN ),
							"hu" => __( "Hungarian", ALLI_ADMIN_TEXTDOMAIN ),
							"in" => __( "Indonesian", ALLI_ADMIN_TEXTDOMAIN ),
							"it" => __( "Italian", ALLI_ADMIN_TEXTDOMAIN ),
							"ja" => __( "Japanese", ALLI_ADMIN_TEXTDOMAIN ),
							"kn" => __( "Kannada", ALLI_ADMIN_TEXTDOMAIN ),
							"ko" => __( "Korean", ALLI_ADMIN_TEXTDOMAIN ),
							"lv" => __( "Latvian", ALLI_ADMIN_TEXTDOMAIN ),
							"ln" => __( "Lingala", ALLI_ADMIN_TEXTDOMAIN ),
							"lt" => __( "Lithuanian", ALLI_ADMIN_TEXTDOMAIN ),
							"ms" => __( "Malay", ALLI_ADMIN_TEXTDOMAIN ),
							"ml" => __( "Malayalam", ALLI_ADMIN_TEXTDOMAIN ),
							"mr" => __( "Marathi", ALLI_ADMIN_TEXTDOMAIN ),
							"no" => __( "Norwegian", ALLI_ADMIN_TEXTDOMAIN ),
							"or" => __( "Oriya", ALLI_ADMIN_TEXTDOMAIN ),
							"fa" => __( "Persian", ALLI_ADMIN_TEXTDOMAIN ),
							"pl" => __( "Polish", ALLI_ADMIN_TEXTDOMAIN ),
							"pt_BR" => __( "Portugese (Brazil)", ALLI_ADMIN_TEXTDOMAIN ),
							"pt_PT" => __( "Portugese (Portugal)", ALLI_ADMIN_TEXTDOMAIN ),
							"ro" => __( "Romanian", ALLI_ADMIN_TEXTDOMAIN ),
							"ru" => __( "Russian", ALLI_ADMIN_TEXTDOMAIN ),
							"sr" => __( "Serbian", ALLI_ADMIN_TEXTDOMAIN ),
							"sk" => __( "Slovak", ALLI_ADMIN_TEXTDOMAIN ),
							"sl" => __( "Slovenian", ALLI_ADMIN_TEXTDOMAIN ),
							"es" => __( "Spanish", ALLI_ADMIN_TEXTDOMAIN ),
							"sv" => __( "Swedish", ALLI_ADMIN_TEXTDOMAIN ),
							"gsw" => __( "Swiss German", ALLI_ADMIN_TEXTDOMAIN ),
							"ta" => __( "Tamil", ALLI_ADMIN_TEXTDOMAIN ),
							"te" => __( "Telugu", ALLI_ADMIN_TEXTDOMAIN ),
							"th" => __( "Thai", ALLI_ADMIN_TEXTDOMAIN ),
							"tr" => __( "Turkish", ALLI_ADMIN_TEXTDOMAIN ),
							"uk" => __( "Ukranian", ALLI_ADMIN_TEXTDOMAIN ),
							"vi" => __( "Vietnamese", ALLI_ADMIN_TEXTDOMAIN ),
						),
						"type" => "select"
					),
					"shortcode_has_atts" => true,
				)
			);

			return $option;
		}
		
		extract(shortcode_atts(array(
				'layout'		=> 'normal-count',
				'url'			=> '',
				'lang'			=> '',
				'imageurl'		=> '',
	    ), $atts));
		
	    if ($url != '') { $url = "data-url='".$url."'"; }
	    if ($imageurl != '') { $url = "data-imageurl='".$imageurl."'"; }
	    if ($lang != '') { $lang = "data-locale='".$lang."'"; }
	    
	    $out = '<div class = "alli_sociable"><a title="Post to Google Buzz" class="google-buzz-button" href="http://www.google.com/buzz/post" '.$lang.' '.$imageurl.' '.$url.' data-button-style="'.$layout.'"></a>';
	    $out .= '<script type="text/javascript" src="http://www.google.com/buzz/api/button.js"></script></div>';
	    		
		return $out;
	}
	
	/**
	 *  Google +1
	 */
	function googleplusone( $atts = null, $content = null ) {
	
		if( $atts == 'generator' ) {
			$option = array( 
				"name" => __( "Google +1", ALLI_ADMIN_TEXTDOMAIN ),
				"value" => "googleplusone",
				"options" => array(
					array(
						"name" => __( "Size", ALLI_ADMIN_TEXTDOMAIN ),
						"id" => "size",
						"desc" => __( 'Choose how you would like to display the google plus button.', ALLI_ADMIN_TEXTDOMAIN ),
						"default" => "",
						"options" => array(
							"small" => __( "Small", ALLI_ADMIN_TEXTDOMAIN ),
							"standard" => __( "Standard", ALLI_ADMIN_TEXTDOMAIN ),
							"medium" => __( "Medium", ALLI_ADMIN_TEXTDOMAIN ),
							"tall" => __( "Tall", ALLI_ADMIN_TEXTDOMAIN ),
						),
						"type" => "select"
					),
					array(
						"name" => __( "Language", ALLI_ADMIN_TEXTDOMAIN ),
						"id" => "lang",
						"desc" => __( 'Select which language you would like to display the button in.', ALLI_ADMIN_TEXTDOMAIN ),
						"default" => "",
						"options" => array(
							"ar" => __( "Arabic", ALLI_ADMIN_TEXTDOMAIN ),
							"bn" => __( "Bengali", ALLI_ADMIN_TEXTDOMAIN ),
							"bg" => __( "Bulgarian", ALLI_ADMIN_TEXTDOMAIN ),
							"ca" => __( "Catalan", ALLI_ADMIN_TEXTDOMAIN ),
							"zh" => __( "Chinese", ALLI_ADMIN_TEXTDOMAIN ),
							"zh_CN" => __( "Chinese (China)", ALLI_ADMIN_TEXTDOMAIN ),
							"zh_HK" => __( "Chinese (Hong Kong)", ALLI_ADMIN_TEXTDOMAIN ),
							"zh_TW" => __( "Chinese (Taiwan)", ALLI_ADMIN_TEXTDOMAIN ),
							"hr" => __( "Croation", ALLI_ADMIN_TEXTDOMAIN ),
							"cs" => __( "Czech", ALLI_ADMIN_TEXTDOMAIN ),
							"da" => __( "Danish", ALLI_ADMIN_TEXTDOMAIN ),
							"nl" => __( "Dutch", ALLI_ADMIN_TEXTDOMAIN ),
							"en_IN" => __( "English (India)", ALLI_ADMIN_TEXTDOMAIN ),
							"en_IE" => __( "English (Ireland)", ALLI_ADMIN_TEXTDOMAIN ),
							"en_SG" => __( "English (Singapore)", ALLI_ADMIN_TEXTDOMAIN ),
							"en_ZA" => __( "English (South Africa)", ALLI_ADMIN_TEXTDOMAIN ),
							"en_GB" => __( "English (United Kingdom)", ALLI_ADMIN_TEXTDOMAIN ),
							"fil" => __( "Filipino", ALLI_ADMIN_TEXTDOMAIN ),
							"fi" => __( "Finnish", ALLI_ADMIN_TEXTDOMAIN ),
							"fr" => __( "French", ALLI_ADMIN_TEXTDOMAIN ),
							"de" => __( "German", ALLI_ADMIN_TEXTDOMAIN ),
							"de_CH" => __( "German (Switzerland)", ALLI_ADMIN_TEXTDOMAIN ),
							"el" => __( "Greek", ALLI_ADMIN_TEXTDOMAIN ),
							"gu" => __( "Gujarati", ALLI_ADMIN_TEXTDOMAIN ),
							"iw" => __( "Hebrew", ALLI_ADMIN_TEXTDOMAIN ),
							"hi" => __( "Hindi", ALLI_ADMIN_TEXTDOMAIN ),
							"hu" => __( "Hungarian", ALLI_ADMIN_TEXTDOMAIN ),
							"in" => __( "Indonesian", ALLI_ADMIN_TEXTDOMAIN ),
							"it" => __( "Italian", ALLI_ADMIN_TEXTDOMAIN ),
							"ja" => __( "Japanese", ALLI_ADMIN_TEXTDOMAIN ),
							"kn" => __( "Kannada", ALLI_ADMIN_TEXTDOMAIN ),
							"ko" => __( "Korean", ALLI_ADMIN_TEXTDOMAIN ),
							"lv" => __( "Latvian", ALLI_ADMIN_TEXTDOMAIN ),
							"ln" => __( "Lingala", ALLI_ADMIN_TEXTDOMAIN ),
							"lt" => __( "Lithuanian", ALLI_ADMIN_TEXTDOMAIN ),
							"ms" => __( "Malay", ALLI_ADMIN_TEXTDOMAIN ),
							"ml" => __( "Malayalam", ALLI_ADMIN_TEXTDOMAIN ),
							"mr" => __( "Marathi", ALLI_ADMIN_TEXTDOMAIN ),
							"no" => __( "Norwegian", ALLI_ADMIN_TEXTDOMAIN ),
							"or" => __( "Oriya", ALLI_ADMIN_TEXTDOMAIN ),
							"fa" => __( "Persian", ALLI_ADMIN_TEXTDOMAIN ),
							"pl" => __( "Polish", ALLI_ADMIN_TEXTDOMAIN ),
							"pt_BR" => __( "Portugese (Brazil)", ALLI_ADMIN_TEXTDOMAIN ),
							"pt_PT" => __( "Portugese (Portugal)", ALLI_ADMIN_TEXTDOMAIN ),
							"ro" => __( "Romanian", ALLI_ADMIN_TEXTDOMAIN ),
							"ru" => __( "Russian", ALLI_ADMIN_TEXTDOMAIN ),
							"sr" => __( "Serbian", ALLI_ADMIN_TEXTDOMAIN ),
							"sk" => __( "Slovak", ALLI_ADMIN_TEXTDOMAIN ),
							"sl" => __( "Slovenian", ALLI_ADMIN_TEXTDOMAIN ),
							"es" => __( "Spanish", ALLI_ADMIN_TEXTDOMAIN ),
							"sv" => __( "Swedish", ALLI_ADMIN_TEXTDOMAIN ),
							"gsw" => __( "Swiss German", ALLI_ADMIN_TEXTDOMAIN ),
							"ta" => __( "Tamil", ALLI_ADMIN_TEXTDOMAIN ),
							"te" => __( "Telugu", ALLI_ADMIN_TEXTDOMAIN ),
							"th" => __( "Thai", ALLI_ADMIN_TEXTDOMAIN ),
							"tr" => __( "Turkish", ALLI_ADMIN_TEXTDOMAIN ),
							"uk" => __( "Ukranian", ALLI_ADMIN_TEXTDOMAIN ),
							"vi" => __( "Vietnamese", ALLI_ADMIN_TEXTDOMAIN ),
						),
						"type" => "select"
					),
					"shortcode_has_atts" => true,
				)
			);

			return $option;
		}
		
		extract(shortcode_atts(array(
				'size'			=> '',
				'lang'			=> '',
	    ), $atts));
		
	    if ($size != '') { $size = "size='".$size."'"; }
	    if ($lang != '') { $lang = "{lang: '".$lang."'}"; }
	    
		$out = '<div class = "alli_sociable"><script type="text/javascript" src="https://apis.google.com/js/plusone.js">'.$lang.'</script>';
		$out .= '<g:plusone '.$size.'></g:plusone></div>';
	    		
		return $out;
	}
	
	/**
	 *  Digg button
	 */
	function digg( $atts = null, $content = null ) {
	
		if( $atts == 'generator' ) {
			$option = array( 
				"name" => __( "Digg", ALLI_ADMIN_TEXTDOMAIN ),
				"value" => "digg",
				"options" => array(
					array(
						"name" => __( "Layout", ALLI_ADMIN_TEXTDOMAIN ),
						"id" => "layout",
						"desc" => __( 'Choose how you would like to display the digg button.', ALLI_ADMIN_TEXTDOMAIN ),
						"default" => "",
						"options" => array(
							"DiggWide" => __( "Wide", ALLI_ADMIN_TEXTDOMAIN ),
							"DiggMedium" => __( "Medium", ALLI_ADMIN_TEXTDOMAIN ),
							"DiggCompact" => __( "Compact", ALLI_ADMIN_TEXTDOMAIN ),
							"DiggIcon" => __( "Icon", ALLI_ADMIN_TEXTDOMAIN ),
						),
						"type" => "select"
					),
					array(
						"name" => __( "Custom URL", ALLI_ADMIN_TEXTDOMAIN ),
						"id" => "url",
						"desc" => __( 'In case you wish to use a different URL you can input it here.', ALLI_ADMIN_TEXTDOMAIN ),
						"default" => "",
						"type" => "text"
					),
					array(
						"name" => __( "Custom Title", ALLI_ADMIN_TEXTDOMAIN ),
						"id" => "title",
						"desc" => __( 'In case you wish to use a different title you can input it here.', ALLI_ADMIN_TEXTDOMAIN ),
						"default" => "",
						"type" => "text"
					),
					array(
						"name" => __( "Article Type", ALLI_ADMIN_TEXTDOMAIN ),
						"id" => "type",
						"desc" => __( 'You can set the article type here for digg.<br /><br />For example if you wanted to set it in the gaming or entertainment topics then you would type this, "gaming, entertainment".', ALLI_ADMIN_TEXTDOMAIN ),
						"default" => "",
						"type" => "text"
					),
					array(
						"name" => __( "Custom Description", ALLI_ADMIN_TEXTDOMAIN ),
						"id" => "description",
						"desc" => __( 'You can set a custom description to be displayed within digg here.', ALLI_ADMIN_TEXTDOMAIN ),
						"default" => "",
						"type" => "text"
					),
					array(
						"name" => __( "Related Stories", ALLI_ADMIN_TEXTDOMAIN ),
						"id" => "related",
						"desc" => __( 'This option allows you to specify whether links to related stories should be present in the pop up window that may appear when users click the button.', ALLI_ADMIN_TEXTDOMAIN ),
						"default" => "",
						"options" => array(
							"true" => __( "Disable related stories?", ALLI_ADMIN_TEXTDOMAIN ),
						),
						"type" => "checkbox"
					),
					"shortcode_has_atts" => true,
				)
			);			
			
			return $option;
		}
		
		global $wpdb;
		
		extract(shortcode_atts(array(
			'layout'        => 'DiggMedium',
			'url'			=> get_permalink(),
			'title'			=> '',
			'type'			=> '',
			'description'	=> '',
			'related'		=> '',
	    	), $atts));
	    
	    if ($title != '') { $title = "&title='".$title."'"; }
	    if ($type != '') { $type = "rev='".$type."'"; }
	    if ($description != '') { $description = "<span style = 'display: none;'>".$description."</span>"; }
	    if ($related != '') { $related = "&related=no"; }
	    	
		$out = '<div class = "alli_sociable"><a class="DiggThisButton '.$layout.'" href="http://digg.com/submit?url='.$url.$title.$related.'"'.$type.'>'.$description.'</a>';
		$out .= '<script type = "text/javascript" src = "http://widgets.digg.com/buttons.js"></script></div>';
		
		return $out;
	}
	
	/**
	 *  Stumbleupon button
	 */
	function stumbleupon( $atts = null, $content = null ) {
	
		if( $atts == 'generator' ) {
			$option = array( 
				"name" => __( "Stumbleupon", ALLI_ADMIN_TEXTDOMAIN ),
				"value" => "stumbleupon",
				"options" => array(
					array(
						"name" => __( "Layout", ALLI_ADMIN_TEXTDOMAIN ),
						"id" => "layout",
						"desc" => __( 'Choose how you would like to display the stumbleupon button.', ALLI_ADMIN_TEXTDOMAIN ),
						"default" => "",
						"options" => array(
							"1" => __( "Style 1", ALLI_ADMIN_TEXTDOMAIN ),
							"2" => __( "Style 2", ALLI_ADMIN_TEXTDOMAIN ),
							"3" => __( "Style 3", ALLI_ADMIN_TEXTDOMAIN ),
							"4" => __( "Style 4", ALLI_ADMIN_TEXTDOMAIN ),
							"5" => __( "Style 5", ALLI_ADMIN_TEXTDOMAIN ),
							"6" => __( "Style 6", ALLI_ADMIN_TEXTDOMAIN ),
						),
						"type" => "select"
					),
					array(
						"name" => __( "Custom URL", ALLI_ADMIN_TEXTDOMAIN ),
						"id" => "url",
						"desc" => __( 'You can set a custom URL to be displayed within stumbleupon here.', ALLI_ADMIN_TEXTDOMAIN ),
						"default" => "",
						"type" => "text"
					),
					"shortcode_has_atts" => true,
				)
			);			
			
			return $option;
		}
		
		global $wpdb;
		
		extract(shortcode_atts(array(
			'layout'        => '5',
			'url'			=> '',
	    	), $atts));
	    	
	    if ($url != '') { $url = "&r=".$url; }
	    	
		return '<div class = "alli_sociable"><script src="http://www.stumbleupon.com/hostedbadge.php?s='.$layout.$url.'"></script></div>';
	}
	
	/**
	 *  Reddit button
	 */
	function reddit( $atts = null, $content = null ) {
	
		if( $atts == 'generator' ) {
			$option = array( 
				"name" => __( "Reddit", ALLI_ADMIN_TEXTDOMAIN ),
				"value" => "reddit",
				"options" => array(
					array(
						"name" => __( "Layout", ALLI_ADMIN_TEXTDOMAIN ),
						"id" => "layout",
						"desc" => __( 'Choose how you would like to display the reddit button.', ALLI_ADMIN_TEXTDOMAIN ),
						"default" => "",
						"options" => array(
							"1" => __( "Style 1", ALLI_ADMIN_TEXTDOMAIN ),
							"2" => __( "Style 2", ALLI_ADMIN_TEXTDOMAIN ),
							"3" => __( "Style 3", ALLI_ADMIN_TEXTDOMAIN ),
							"4" => __( "Style 4", ALLI_ADMIN_TEXTDOMAIN ),
							"5" => __( "Style 5", ALLI_ADMIN_TEXTDOMAIN ),
							"6" => __( "Style 6", ALLI_ADMIN_TEXTDOMAIN ),
							"7" => __( "Interactive 1", ALLI_ADMIN_TEXTDOMAIN ),
							"8" => __( "Interactive 2", ALLI_ADMIN_TEXTDOMAIN ),
							"9" => __( "Interactive 3", ALLI_ADMIN_TEXTDOMAIN ),
						),
						"type" => "select"
					),
					array(
						"name" => __( "Custom URL", ALLI_ADMIN_TEXTDOMAIN ),
						"id" => "url",
						"desc" => __( 'You can set a custom URL to be displayed with your button instead of the current page.', ALLI_ADMIN_TEXTDOMAIN ),
						"default" => "",
						"type" => "text"
					),
					array(
						"name" => __( "Custom Title", ALLI_ADMIN_TEXTDOMAIN ),
						"id" => "title",
						"desc" => __( 'If using the interactive buttons you can specify a custom title to use here.', ALLI_ADMIN_TEXTDOMAIN ),
						"default" => "",
						"type" => "text"
					),
					array(
						"name" => __( "Styling", ALLI_ADMIN_TEXTDOMAIN ),
						"id" => "disablestyle",
						"desc" => __( 'Checking this will disable the reddit styling used for the button.', ALLI_ADMIN_TEXTDOMAIN ),
						"default" => "",
						"options" => array(
							"true" => __( "Disable reddit styling?", ALLI_ADMIN_TEXTDOMAIN ),
						),
						"type" => "checkbox"
					),
					array(
						"name" => __( "Target", ALLI_ADMIN_TEXTDOMAIN ),
						"id" => "target",
						"desc" => __( 'Select the target for this button.', ALLI_ADMIN_TEXTDOMAIN ),
						"default" => "",
						"options" => array(
							"true" => __( "Display in new window?", ALLI_ADMIN_TEXTDOMAIN ),
						),
						"type" => "checkbox"
					),
					array(
						"name" => __( "Community", ALLI_ADMIN_TEXTDOMAIN ),
						"id" => "community",
						"desc" => __( 'If using the interactive buttons you can specify a community to target here.', ALLI_ADMIN_TEXTDOMAIN ),
						"default" => "",
						"type" => "text"
					),
					"shortcode_has_atts" => true,
				)
			);			
			
			return $option;
		}
		
		global $wpdb;
		
		extract(shortcode_atts(array(
			'layout'        => '8',
			'url'			=> '',
			'disablestyle'	=> '',
			'target'		=> '',
			'community'		=> '',
			'title'			=> '',
	    	), $atts));
	    	
	    if ($disablestyle != '') { $disablestyle = "&styled=off"; }
	    if ($target != '') { $target = "&newwindow=1"; }
	    if ($layout == '7' || $layout == '8' || $layout == '9') { $url = "reddit_url='".$url."';"; } else { if ($url != '') { $url = "&url='".$url."'"; } }
	    if ($title != '') { $title = "reddit_title='".$title."';"; }
	    if ($community != '') { $community = "reddit_target='".$community."';"; }
	    if ($layout == '7' || $layout == '8' || $layout == '9') { $target = "reddit_newwindow='1';"; }
	    	
		switch ($layout)
		{
			case 1: return '<div class = "alli_sociable"><script type="text/javascript" src="http://www.reddit.com/buttonlite.js?i=0'.$disablestyle.$url.$target.'"></script></div>'; break;
			case 2: return '<div class = "alli_sociable"><script type="text/javascript" src="http://www.reddit.com/buttonlite.js?i=1'.$disablestyle.$url.$target.'"></script></div>'; break;		
			case 3: return '<div class = "alli_sociable"><script type="text/javascript" src="http://www.reddit.com/buttonlite.js?i=2'.$disablestyle.$url.$target.'"></script></div>'; break;		
			case 4: return '<div class = "alli_sociable"><script type="text/javascript" src="http://www.reddit.com/buttonlite.js?i=3'.$disablestyle.$url.$target.'"></script></div>'; break;		
			case 5: return '<div class = "alli_sociable"><script type="text/javascript" src="http://www.reddit.com/buttonlite.js?i=4'.$disablestyle.$url.$target.'"></script></div>'; break;		
			case 6: return '<div class = "alli_sociable"><script type="text/javascript" src="http://www.reddit.com/buttonlite.js?i=5'.$disablestyle.$url.$target.'"></script></div>'; break;	
			case 7: $out = '<div class = "alli_sociable"><script type="text/javascript" src="http://www.reddit.com/static/button/button1.js"></script>'; 
					$out .= '<script type = "text/javascript">'.$url.$title.$community.$target.'</script></div>';
					return $out; break;
			case 8: $out = '<div class = "alli_sociable"><script type="text/javascript" src="http://www.reddit.com/static/button/button2.js"></script>';
					$out .= '<script type = "text/javascript">'.$url.$title.$community.$target.'</script></div>';
					return $out; break;
			case 9: $out = '<div class = "alli_sociable"><script type="text/javascript" src="http://www.reddit.com/static/button/button3.js"></script>'; 
					$out .= '<script type = "text/javascript">'.$url.$title.$community.$target.'</script></div>';
					return $out; break;	
		}
	}
	
	/**
	 *  LinkedIn button
	 */
	function linkedin( $atts = null, $content = null ) {
	
		if( $atts == 'generator' ) {
			$option = array( 
				"name" => __( "LinkedIn", ALLI_ADMIN_TEXTDOMAIN ),
				"value" => "linkedin",
				"options" => array(
					array(
						"name" => __( "Layout", ALLI_ADMIN_TEXTDOMAIN ),
						"id" => "layout",
						"desc" => __( 'Choose how you would like to display the linkedin button.', ALLI_ADMIN_TEXTDOMAIN ),
						"default" => "",
						"options" => array(
							"1" => __( "Style 1", ALLI_ADMIN_TEXTDOMAIN ),
							"2" => __( "Style 2", ALLI_ADMIN_TEXTDOMAIN ),
							"3" => __( "Style 3", ALLI_ADMIN_TEXTDOMAIN ),
						),
						"type" => "select"
					),
					array(
						"name" => __( "Custom URL", ALLI_ADMIN_TEXTDOMAIN ),
						"id" => "url",
						"desc" => __( 'You can set a custom URL to be displayed within linkedin here.', ALLI_ADMIN_TEXTDOMAIN ),
						"default" => "",
						"type" => "text"
					),
					"shortcode_has_atts" => true,
				)
			);			
			
			return $option;
		}
		
		global $wpdb;
		
		extract(shortcode_atts(array(
			'layout'        => '3',
			'url'			=> '',
	    	), $atts));
	    	
	    if ($url != '') { $url = "data-url='".$url."'"; }
	    if ($layout == '2') { $layout = 'right'; }
		if ($layout == '3') { $layout = 'top'; }
	    	
		return '<div class = "alli_sociable"><script type="text/javascript" src="http://platform.linkedin.com/in.js"></script><script type="in/share" data-counter = "'.$layout.'" '.$url.'></script></div>';
	}
	
	/**
	 *  Delicious button
	 */
	function delicious( $atts = null, $content = null ) {
	
		if( $atts == 'generator' ) {
			$option = array( 
				"name" => __( "Delicious", ALLI_ADMIN_TEXTDOMAIN ),
				"value" => "delicious",
				"options" => array(
					array(
						"name" => __( "Custom Text", ALLI_ADMIN_TEXTDOMAIN ),
						"id" => "text",
						"desc" => __( 'You can set some text to display alongside your delicious button.', ALLI_ADMIN_TEXTDOMAIN ),
						"default" => "",
						"type" => "text"
					),
					"shortcode_has_atts" => true,
				)
			);			
			
			return $option;
		}
		
		global $wpdb;
		
		extract(shortcode_atts(array(
			'text'			=> '',
	    	), $atts));
	    	
		return '<div class = "alli_sociable"><img src="http://l.yimg.com/hr/img/delicious.small.gif" height="10" width="10" alt="Delicious" />&nbsp;<a href="http://www.delicious.com/save" onclick="window.open(&#39;http://www.delicious.com/save?v=5&noui&jump=close&url=&#39;+encodeURIComponent(location.href)+&#39;&title=&#39;+encodeURIComponent(document.title), &#39;delicious&#39;,&#39;toolbar=no,width=550,height=550&#39;); return false;">'.$text.'</a></div>';
	}
	
	/**
	 *
	 */
	function _options( $class ) {
		$shortcode = array();
		
		$class_methods = get_class_methods( $class );
		
		foreach( $class_methods as $method ) {
			if( $method[0] != '_' )
				$shortcode[] = call_user_func(array( &$class, $method ), $atts = 'generator' );
		}
		
		$options = array(
			"name" => __( 'Social', ALLI_ADMIN_TEXTDOMAIN ),
			'desc' => __( 'Choose which type of social button you wish to use.', ALLI_ADMIN_TEXTDOMAIN ),
			"value" => "social",
			"options" => $shortcode,
			"shortcode_has_types" => true
		);
		
		return $options;
	}
}

?>
