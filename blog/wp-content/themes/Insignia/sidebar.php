<?php
/**
 * Sidebar Template
 *
 * @package Alliase
 * @subpackage Template
 */
?>

		<style>

form.story {margin-top:20px; display:table;}
input[type="text"].story_field { font-size: 13px; color: #666; width: 150px; height: 20px; margin: 8px 0; border: 1px solid #ccc; background: #fafafa; padding: 4px 8px; -moz-border-radius: 4px; -webkit-border-radius: 4px; border-radius: 4px; position: relative; behavior: url(/css/PIE.php); }
label.lead { font-weight: bold; width:85px; text-align: right; padding: 19px 8px 2px 0; float: left; clear: both; }
.green_btn { float:left; margin:8px 0 40px 90px;font-size:12px; text-align: center; font-weight: bold; color: #fff; text-decoration: none; padding: 8px 23px; border: none; background: #A6D20E; -moz-border-radius: 4px; -webkit-border-radius: 4px; border-radius: 4px; position: relative; behavior: url(/css/PIE.php); }
.green_btn:hover {background: #b5e21b;cursor: pointer;}
 </style>
<div id="sidebar">


		<div id="connect_subscribe">
		
		<!--<h2 class="right_column"><span>r</span>Connect with StayClassy</h2>
		
		<a href="http://www.linkedin.com/company/stayclassy" target="_blank"><img src="/blog/wp-content/themes/Insignia/images/linkedIn_joinBtn.jpg" border="0"/></a>
		
		<p>Subscribe to our Newsletter and receive weekly charitable achievements.</p>
            
            <form action="http://stayclassy.us2.list-manage.com/subscribe/post?u=a786802c7dfb982c9ff4a6be6&amp;id=c882d99c35" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" style="font: normal 100% Arial, sans-serif;font-size: 10px;" _lpchecked="1">
                <div class="mc-field-group" style="margin-top:12px;">
                    <input type="text" value="Your email address..." onfocus="if(this.value=='Your email address...')this.value=''" name="EMAIL" class="required email" id="mce-EMAIL" ></div>
                   <div id="mce-responses" style="float: left;top: -1.4em;padding: 0em .5em 0em .5em;overflow: hidden;width: 90%;margin: 0 5%;clear: both;">
                       <div class="response" id="mce-error-response" style="display: none;margin: 1em 0;padding: 1em .5em .5em 0;font-weight: bold;float: left;top: -1.5em;z-index: 1;width: 80%;background: FBE3E4;color: #D12F19;"></div>
                       <div class="response" id="mce-success-response" style="display: none;margin: 1em 0;padding: 1em .5em .5em 0;font-weight: bold;float: left;top: -1.5em;z-index: 1;width: 80%;background: #E3FBE4;color: #529214;"></div>
                   </div>
               
               <input type="submit" value="Subscribe &#8594;" name="subscribe" id="mc-embedded-subscribe" class="btn">
               <a href="#" id="mc_embed_close" class="mc_embed_close" style="display: none;">Close</a>
            </form>
			<br><br>-->
		
            <h2 class="right_column"><span>r</span>Connect with StayClassy</h2>
            <p>Nonprofits using StayClassy grew their online fundraising volume an average of <strong>180%</strong> from 2010 to 2011.  Enter your contact info below to learn more about how StayClassy can help your organization:</p>
          	<form action="https://www.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8" method="POST" class="story" >

			<input type=hidden name="oid" value="00DE0000000K08T">
			<input type=hidden name="retURL" value="http://www.stayclassy.org/blog/thank-you">
			
			
			<label for="first_name" class="lead">First Name</label><input  id="first_name" maxlength="40" name="first_name" size="20" type="text" class="story_field" /><br>
			
			<label for="last_name" class="lead">Last Name</label><input  id="last_name" maxlength="80" name="last_name" size="20" type="text" class="story_field"/><br>
			
			<label for="email" class="lead">Email</label><input  id="email" maxlength="80" name="email" size="20" type="text" class="story_field"/><br>
			
			<label for="company" class="lead">Organization</label><input  id="company" maxlength="40" name="company" size="20" type="text" class="story_field"/><br>
          
            <label for="lead_source" style="display:none;">Lead Source</label><select  id="lead_source" name="lead_source" style="display:none;"><option value="SC Blog">SC Blog</option></select>
            
            <input type="submit" name="submit" class="green_btn" value="SUBMIT">
            
            </form>
            
            <!--  
          
             <div id="connect_icons">
                <a href="http://www.facebook.com/StayClassy" id="facebook" target="_blank">Facebook</a>
                <a href="https://twitter.com/#!/stayclassysd" id="twitter" target="_blank">Twitter</a>
                <a href="http://www.linkedin.com/company/stayclassy" id="linkedin" target="_blank">LinkedIn</a>
                <a href="https://plus.google.com/108467924754044426206/posts" id="google" target="_blank">Google Plus</a>
                <a href="/blog/?feed=rss" id="rss" target="_blank">RSS</a>
            </div><br>
<iframe src="//www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2Fstayclassy&amp;width=270&amp;height=258&amp;colorscheme=light&amp;show_faces=true&amp;border_color&amp;stream=false&amp;header=false" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:270px; height:258px;" allowTransparency="true"></iframe>           

 
 <h2 class="right_column"><span>N</span>Newsletter</h2>
<p>Subscribe to our Newsletter and receive weekly charitable achievements.</p>
            
            <form action="http://stayclassy.us2.list-manage.com/subscribe/post?u=a786802c7dfb982c9ff4a6be6&amp;id=53bdbf6b1b" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" style="font: normal 100% Arial, sans-serif;font-size: 10px;" _lpchecked="1">
                <div class="mc-field-group" style="margin-top:12px;">
                    <input type="text" value="Your email address..." onfocus="if(this.value=='Your email address...')this.value=''" name="EMAIL" class="required email" id="mce-EMAIL" ></div>
                   <div id="mce-responses" style="float: left;top: -1.4em;padding: 0em .5em 0em .5em;overflow: hidden;width: 90%;margin: 0 5%;clear: both;">
                       <div class="response" id="mce-error-response" style="display: none;margin: 1em 0;padding: 1em .5em .5em 0;font-weight: bold;float: left;top: -1.5em;z-index: 1;width: 80%;background: FBE3E4;color: #D12F19;"></div>
                       <div class="response" id="mce-success-response" style="display: none;margin: 1em 0;padding: 1em .5em .5em 0;font-weight: bold;float: left;top: -1.5em;z-index: 1;width: 80%;background: #E3FBE4;color: #529214;"></div>
                   </div>
               <div>
               <input type="submit" value="Subscribe &#8594;" name="subscribe" id="mc-embedded-subscribe" class="btn">
               <a href="#" id="mc_embed_close" class="mc_embed_close" style="display: none;">Close</a>
            </form>
        </div> -->
    
        <div id="must_reads" style="margin-top:35px;">
            <h1>Must Reads</h1>
            <div id="weekOf">&#8901; Week of October 8, 2012 &#8901;</div>
            <div id="content">
            	
                <div class="story">
                  
                    <div class="link">
                        <a href="http://www.nonprofitmarketingblog.com/comments/the_top_two_reasons_people_donate_to_your_cause" target="_blank">The Two Reasons People Donate to Your Cause</a>
                        <em>Katya's Nonprofit Marketing Blog</em>
                    </div>
                </div>
                <div class="story">
                  
                    <div class="link">
                        <a href="http://www.frogloop.com/care2blog/2012/10/1/tips-to-engage-baby-boomers-and-seniors-through-social-media.html" target="_blank">Engaging Multiple Generations Through Social Media</a>
                        <em>FrogLoop</em>
                    </div>
                </div>
                <div class="story">
		
                    <div class="link">
                        <a href="http://www.huffingtonpost.com/paul-clolery/presidential-debates-charity-candidates_b_1935446.html?utm_hp_ref=impact" target="_blank">What Nonprofits Want from the Next President</a>
                        <em>HuffPost Impact</em>
                    </div>
                </div>
                <div class="story">
			
                    <div class="link">
                        <a href="http://philanthropy.com/blogs/prospecting/watchdog-plans-review-of-charity-fundraising-appeals/35464" target="_blank">Charity Watchdog Reviews Fundraising Appeals </a>
                        <em>Chronicle of Philanthropy</em>
                    </div>
                </div>
                <div class="story">
		
                    <div class="link">
                        <a href="http://www.fastcoexist.com/1680486/to-raise-a-generation-of-creative-kids-let-them-make-their-own-stories" target="_blank">The Rising Generation of Creativity</a>
                        <em>Fast Company</em>
                    </div>
                </div>
                <div class="story" style="border:none;">
	
                    <div class="link">
                        <a href="http://nptalk.co/nonprofit-mobile-giving-campaign/" target="_blank">Before You Launch a Mobile Giving Campaign... </a>
                        <em>TNPTalk</em>
                    </div>
                </div>
             
            </div>
            
           <div id="bottom_cap"></div>
     
        </div>
        
        <div id="archives">
            <h2 class="right_column"><span>I</span>Blog Archives</h2>
           <form id="searchform" method="get" action="http://stayclassy.org/blog/">
      
     
         <input type="text" name="s" id="search_field" size="15" value="Search keywords..." onfocus="if(this.value=='Search keywords...')this.value=''">
   

      <input type="submit" value="Search &#8594;" id="search_btn">
      
    </form>
            <p>or, sort by most popular tags:</p>
           <?php //alli_sidebar_begin(); ?>

			<?php alli_dynamic_sidebar(); ?>

			<?php ///alli_sidebar_end(); ?>
        </div>
        
        
        <div id="twitter_feed">
            <h2 class="right_column"><span>B</span>Twitter Feed</h2>
            <script charset="utf-8" src="http://widgets.twimg.com/j/2/widget.js"></script>
			<script>
            new TWTR.Widget({
              version: 2,
              type: 'profile',
              rpp: 5,
              interval: 30000,
              width: 270,
              height: 300,
              theme: {
                shell: {
                  background: '#ffffff',
                  color: '#ffffff'
                },
                tweets: {
                  background: '#f8f8f8',
                  color: '#666666',
                  links: '#4292cb'
                }
              },
              features: {
                scrollbar: false,
                loop: false,
                live: false,
                behavior: 'all'
              }
            }).render().setUser('stayclassysd').start();
            </script>
        </div>
        
        <div id="blogs_we_love">
            <h2 class="right_column"><span>k</span>Sites We Love</h2>
            <ul>
                <li><a href="http://www.fastcoexist.com/" target="_blank">Co.Exist</a></li>
                <li><a href="http://www.frogloop.com/" target="_blank">FrogLoop</a></li>
                <li><a href="http://www.huffingtonpost.com/impact" target="_blank">HuffPost Impact</a></li>
                <li><a href="http://www.nonprofitmarketingblog.com/" target="_blank">Katya's Nonprofit Marketing Blog</a></li>
                <li><a href="http://mashable.com/social-good/" target="_blank">Mashable Social Good</a></li>
                <li><a href="http://act.mtv.com/" target="_blank">MTV Act</a></li>
                <li><a href="http://nptalk.co/" target="_blank">NPTalk</a></li>
                <li><a href="http://socialmedia4nonprofits.org/" target="_blank">Social Media for Nonprofits</a></li>
                <li><a href="http://techcrunch.com/" target="_blank">TechCrunch</a></li>
                <li><a href="http://www.philanthropy.com/" target="_blank">The Chronicle of Philanthropy</a></li>
                <li><a href="http://www.thefundraisingauthority.com/" target="_blank">The Fundraising Authority</a></li>
			</ul>
                
        </div>
    
    
    </div>
		
			
		
	

</div><!-- #sidebar -->