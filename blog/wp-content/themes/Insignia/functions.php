<?php

/**
 * Sets up the theme by loading the Alliase class & initializing the framework
 * which activates all classes and functions needed for theme's operation.
 *
 * @package Alliase
 * @subpackage Functions
 */

# Load the Alliase class.
require_once( TEMPLATEPATH . '/framework.php' );

# Get theme data.
$theme_data = get_theme_data( TEMPLATEPATH . '/style.css' );

# Initialize the Alliase framework.
Alliase::init(array(
	'theme_name' => $theme_data['Name'],
	'theme_version' => $theme_data['Version']
));

?>
