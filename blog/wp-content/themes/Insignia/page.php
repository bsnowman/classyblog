<?php
/**
 * Page Template
 *
 * @package Alliase
 * @subpackage Template
 */
global $alli;
get_header(); ?>

<?php if( isset( $alli->is_blog ) ) : ?>
	
	<?php get_template_part( 'loop', 'index' ); ?>
	
<?php else : ?>
	
	<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

		<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	
			<?php alli_before_entry(); ?>
			
			<div class="entry">
				<?php the_content(); ?>
				
				<div class="clearboth"></div>
				
				<?php wp_link_pages( array( 'before' => '<div class="page_link">' . __( 'Pages:', ALLI_TEXTDOMAIN ), 'after' => '</div>' ) ); ?>
				<?php edit_post_link( __( 'Edit', ALLI_TEXTDOMAIN ), '<div class="edit_link">', '</div>' ); ?>
				
				</div><!-- .entry -->
							
			<?php alli_after_entry(); ?>

		</div><!-- #post-## -->
		
	<?php endwhile; ?>
	
<?php endif; ?>

	<?php alli_after_page_content(); ?>
	
		<div class="clearboth"></div>
	</div><!-- #main_inner -->
</div><!-- #main -->

<?php get_footer(); ?>
