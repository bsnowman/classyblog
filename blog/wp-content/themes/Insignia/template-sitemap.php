<?php
/**
 * Template Name: Sitemap
 *
 * @package Alliase
 * @subpackage Template
 */

get_header(); ?>

	<?php alli_sitemap(); ?>
	
	<?php alli_after_page_content(); ?>

			<div class="clearboth"></div>
		</div><!-- #main_inner -->
	</div><!-- #main -->

<?php get_footer(); ?>
