<?php
/**
 * The Alliase class. Defines the necessary constants 
 * and includes the necessary files for theme's operation.
 *
 * @package Alliase
 * @subpackage Insignia
 */

class Alliase {
	
	/**
	 * Initializes the theme framework by loading
	 * required files and functions for the theme.
	 *
	 * @since 1.0
	 */
	public static function init( $options ) {
		self::constants( $options );
		self::functions();
		self::extensions();
		self::classes();
		self::variables();
		self::actions();
		self::filters();
		self::supports();
		self::locale();
		self::admin();
	}
	
	/**
	 * Define theme constants.
	 *
	 * @since 1.0
	 */
	function constants( $options ) {
		define( 'THEME_NAME', $options['theme_name'] );
		define( 'THEME_SLUG', get_template() );
		define( 'THEME_VERSION', $options['theme_version'] );
		define( 'FRAMEWORK_VERSION', '1.3' );
		define( 'DOCUMENTATION_URL', 'http://alliase.com/themes/insignia/support/online-documentation/' );
		define( 'SUPPORT_URL', 'http://alliase.com/support-forums/' );
		define( 'ALLI_PREFIX', 'alli' );
		define( 'ALLI_TEXTDOMAIN', THEME_SLUG );
		define( 'ALLI_ADMIN_TEXTDOMAIN', THEME_SLUG . '_admin' );
		
		define( 'ALLI_SETTINGS', 'alli_' . THEME_SLUG . '_options' );
		define( 'ALLI_INTERNAL_SETTINGS', 'alli_' . THEME_SLUG . '_internal_options' );
		define( 'ALLI_SIDEBARS', 'alli_' . THEME_SLUG . '_sidebars' );
		define( 'ALLI_SKINS', 'alli_' . THEME_SLUG . '_skins' );
		define( 'ALLI_ACTIVE_SKIN', 'alli_' . THEME_SLUG . '_active_skin' );
		define( 'ALLI_SKIN_NT_WRITABLE', 'alli_' . THEME_SLUG . '_skins_nt_writable' );

		define( 'THEME_URI', get_template_directory_uri() );
		define( 'THEME_DIR', get_template_directory() );

		define( 'THEME_LIBRARY', THEME_DIR . '/lib' );
		define( 'THEME_ADMIN', THEME_LIBRARY . '/admin' );
		define( 'THEME_FUNCTIONS', THEME_LIBRARY . '/functions' );
		define( 'THEME_CLASSES', THEME_LIBRARY . '/classes' );
		define( 'THEME_EXTENSIONS', THEME_LIBRARY . '/extensions' );
		define( 'THEME_SHORTCODES', THEME_LIBRARY . '/shortcodes' );
		define( 'THEME_CACHE', THEME_DIR . '/cache' );
		define( 'THEME_FONTS', THEME_LIBRARY . '/scripts/fonts' );
		define( 'THEME_STYLES_DIR', THEME_DIR . '/styles' );
		define( 'THEME_PATTERNS_DIR', THEME_STYLES_DIR . '/_patterns' );
		define( 'THEME_SPRITES_DIR', THEME_STYLES_DIR . '/_icons' );
		define( 'THEME_IMAGES_DIR', THEME_DIR . '/images' );

		define( 'THEME_PATTERNS', '_patterns' );
		define( 'THEME_IMAGES', THEME_URI . '/images' );
		define( 'THEME_IMAGES_ASSETS', THEME_IMAGES . '/assets' );
		define( 'THEME_JS', THEME_URI . '/lib/scripts' );
		define( 'THEME_STYLES', THEME_URI . '/styles' );
		define( 'THEME_SPRITES', THEME_STYLES . '/_icons' );

		define( 'THEME_ADMIN_FUNCTIONS', THEME_ADMIN . '/functions' );
		define( 'THEME_ADMIN_CLASSES', THEME_ADMIN . '/classes');
		define( 'THEME_ADMIN_OPTIONS', THEME_ADMIN . '/options');
		define( 'THEME_ADMIN_ASSETS_URI', THEME_URI . '/lib/admin/assets' );
	}
		
	/**
	 * Loads theme functions.
	 *
	 * @since 1.0
	 */
	function functions() {
		require_once( THEME_FUNCTIONS . '/hooks-actions.php' );
		require_once( THEME_FUNCTIONS . '/context.php' );
		require_once( THEME_FUNCTIONS . '/core.php' );
		require_once( THEME_FUNCTIONS . '/theme.php' );
		require_once( THEME_FUNCTIONS . '/sliders.php' );
		require_once( THEME_FUNCTIONS . '/scripts.php' );
		require_once( THEME_FUNCTIONS . '/image.php' );
		require_once( THEME_FUNCTIONS . '/twitter.php' );
		require_once( THEME_FUNCTIONS . '/bookmarks.php' );
		require_once( THEME_FUNCTIONS . '/hooks-actions.php' );
	}
	
	/**
	 * Loads theme extensions.
	 *
	 * @since 1.0
	 */
	function extensions() {
		require_once( THEME_EXTENSIONS . '/breadcrumbs-plus/breadcrumbs-plus.php' );
	}
	
	/**
	 * Loads theme classes.
	 *
	 * @since 1.0
	 */
	function classes() {
		require_once( THEME_CLASSES . '/contact.php' );
		require_once( THEME_CLASSES . '/menu-walker.php' );
	}
	
	/**
	 * Loads theme actions.
	 *
	 * @since 1.0
	 */
	function actions() {
		global $alli;
		// 12-10-12, added by EBC, the link rel next/previous in the head is contributing to memory issues
		// by loading the next and previous posts in memory to set these. It also adversely affects SEO
		// for outdated posts.
		remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
		remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
		# WordPress actions
		add_action( 'init', 'alli_shortcodes_init' );
		add_action( 'init', 'alli_menus' );
		add_action( 'init', 'alli_addestyle' );
		add_action( 'init', 'alli_post_types' );
		add_action( 'init', 'alli_register_script' );
		add_action( 'init', 'alli_wp_image_resize', 11 );
		add_action( 'init', array( 'alliForm', 'init'), 11 );
		add_action( 'widgets_init', 'alli_sidebars' );
		add_action( 'widgets_init', 'alli_widgets' );
		add_action( 'template_redirect', 'alli_enqueue_script' );
		add_action( 'wp_head', 'alli_analytics' );
		add_action( 'get_header', 'alli_custom_post_layout' );
		add_action( 'comment_form_defaults', 'alli_comment_form_args' );
		
		# Alliase actions
		add_action( 'alli_head', 'alli_header_scripts' );
		add_action( 'alli_header', 'alli_header_extras' );
		add_action( 'alli_header', 'alli_logo' );
		add_action( 'alli_header' , 'alli_header_ad' );
		add_action( 'alli_after_header', 'alli_main_menu' );
		add_action('alli_before_page_content', 'alli_breadcrumbs');
		add_action( 'alli_before_page_content', 'alli_slider_module' );
		add_action( 'alli_before_page_content', 'alli_home_content' );
		add_action( 'alli_before_page_content', 'alli_page_content' );
		add_action( 'alli_before_page_content', 'alli_page_title' );
		add_action( 'alli_before_page_content', 'alli_query_posts' );
		add_action( 'alli_after_post', 'alli_page_navi' );
		add_action( 'alli_post_image_begin', 'alli_post_img_shadow_before' );
		add_action( 'alli_post_image_end', 'alli_post_img_shadow_after' );
		add_action( 'alli_portfolio_image_begin', 'alli_post_img_shadow_before');
		add_action( 'alli_portfolio_image_end', 'alli_post_img_shadow_after' );
		add_action( 'alli_before_portfolio_image', 'alli_post_img_shadow_before' );
		add_action( 'alli_after_portfolio_image', 'alli_post_img_shadow_after' );
		add_action( 'alli_singular-post_after_entry', 'alli_post_nav' );
		add_action( 'alli_singular-post_after_entry', 'alli_post_meta_bottom' );
		add_action( 'alli_singular-post_after_post', 'alli_about_author' );
		add_action( 'alli_singular-post_after_post', 'alli_like_module' );
		add_action( 'alli_after_main', 'alli_get_sidebar' );
		add_action( 'alli_footer', 'alli_main_footer' );
		add_action( 'alli_after_footer', 'alli_sub_footer' );
		add_action( 'alli_body_end', 'alli_image_preloading' );
		add_action( 'alli_body_end', 'alli_custom_javascript' );
		add_action( 'alli_body_end', 'alli_insignia_menu_script' );
	}
	
	/**
	 * Loads theme filters.
	 *
	 * @since 1.0
	 */
	function filters() { 
		
		# Alliase filters
		add_filter( 'alli_avatar_size', create_function('','return "60";' ) );
		add_filter( 'alli_additional_posts_title', create_function('','return;' ) );
		add_filter( 'alli_read_more', 'alli_read_more' );
		add_filter( 'alli_blog_sc_meta', array( &$this, 'post_meta' ) );
		add_filter( 'alli_widget_meta', 'alli_widget_meta' );
		
		# WordPress filters
		remove_filter( 'the_content', 'wpautop' );
		remove_filter( 'the_content', 'wptexturize' );
		add_filter( 'the_content', 'alli_texturize_shortcode_before' );
		add_filter( 'the_content', 'alli_formatter', 99 );
		add_filter( 'the_content_more_link', 'alli_full_read_more', 10, 2 );
		add_filter( 'widget_text', 'alli_formatter', 99 );
		add_filter( 'excerpt_length', 'alli_excerpt_length_long', 999 );
		add_filter( 'excerpt_more', 'alli_excerpt_more' );
		add_filter( 'posts_where', 'alli_multi_tax_terms' );
		add_filter( 'pre_get_posts', 'alli_exclude_category_feed' );
		add_filter( 'widget_categories_args', 'alli_exclude_category_widget' );
		add_filter( 'query_vars', 'alli_queryvars' );
		add_filter( 'rewrite_rules_array', 'alli_rewrite_rules',10,2 );
		add_filter( 'widget_text', 'do_shortcode' );
		add_filter( 'wp_page_menu_args', 'alli_page_menu_args' );
		add_filter( 'the_password_form', 'alli_password_form' );
		add_filter( 'wp_feed_cache_transient_lifetime', 'alli_twitter_feed_cahce', 10, 2 );
	}
	
	/**
	 * Loads theme supports.
	 *
	 * @since 1.0
	 */
	function supports() {
		add_theme_support( 'menus' );
		add_theme_support( 'widgets' );
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'automatic-feed-links' );

		if ( ! isset( $content_width ) ) $content_width = 720;
	}

	/**
	 * Handles the locale functions file and translations.
	 *
	 * @since 1.0
	 */
	function locale() {
		# Get the user's locale.
		$locale = get_locale();
		
		if( is_admin() ) {
			# Load admin theme textdomain.
			load_theme_textdomain( ALLI_ADMIN_TEXTDOMAIN, THEME_ADMIN . '/languages' );
			$locale_file = THEME_ADMIN . "/languages/$locale.php";
			
		} else {
			# Load theme textdomain.
			load_theme_textdomain( ALLI_TEXTDOMAIN, THEME_DIR . '/languages' );
			$locale_file = THEME_DIR . "/languages/$locale.php";
		}
		
		if ( is_readable( $locale_file ) )
			require_once( $locale_file );
	}
	
	/**
	 * Loads admin files.
	 *
	 * @since 1.0
	 */
	function admin() {
		if( !is_admin() ) return;
			
		require_once( THEME_ADMIN . '/admin.php' );
		alliAdmin::init();
	}
	
	/**
	 * Define theme variables.
	 *
	 * @since 1.0
	 */
	function variables() {
		global $alli;
		
		$img_set = get_option( ALLI_SETTINGS );
		$img_set = ( !empty( $img_set ) && !isset( $_POST[ALLI_SETTINGS]['reset'] ) ) ? $img_set : array();
		$blog_layout = apply_filters( 'alli_blog_layout', alli_get_setting( 'blog_layout' ) );
		
		# Images
		$images = array(
		    'one_column_portfolio' => array( 
		        ( !empty( $img_set['one_column_portfolio_full']['w'] ) ? $img_set['one_column_portfolio_full']['w'] : 950 ),
		        ( !empty( $img_set['one_column_portfolio_full']['h'] ) ? $img_set['one_column_portfolio_full']['h'] : 590 )),
		    'two_column_portfolio' => array( 
		        ( !empty( $img_set['two_column_portfolio_full']['w'] ) ? $img_set['two_column_portfolio_full']['w'] : 446 ),
		        ( !empty( $img_set['two_column_portfolio_full']['h'] ) ? $img_set['two_column_portfolio_full']['h'] : 277 )),
		    'three_column_portfolio' => array( 
		        ( !empty( $img_set['three_column_portfolio_full']['w'] ) ? $img_set['three_column_portfolio_full']['w'] : 278 ),
		        ( !empty( $img_set['three_column_portfolio_full']['h'] ) ? $img_set['three_column_portfolio_full']['h'] : 172 )),
		    'four_column_portfolio' => array( 
		        ( !empty( $img_set['four_column_portfolio_full']['w'] ) ? $img_set['four_column_portfolio_full']['w'] : 194 ),
		        ( !empty( $img_set['four_column_portfolio_full']['h'] ) ? $img_set['four_column_portfolio_full']['h'] : 120 )),

		    'one_column_blog' => array( 
		        ( !empty( $img_set['one_column_blog_full']['w'] ) ? $img_set['one_column_blog_full']['w'] : 950 ),
		        ( !empty( $img_set['one_column_blog_full']['h'] ) ? $img_set['one_column_blog_full']['h'] : 365 )),
		    'two_column_blog' => array( 
		        ( !empty( $img_set['two_column_blog_full']['w'] ) ? $img_set['two_column_blog_full']['w'] : 446 ),
		        ( !empty( $img_set['two_column_blog_full']['h'] ) ? $img_set['two_column_blog_full']['h'] : 171 )),
		    'three_column_blog' => array( 
		        ( !empty( $img_set['three_column_blog_full']['w'] ) ? $img_set['three_column_blog_full']['w'] : 278 ),
		        ( !empty( $img_set['three_column_blog_full']['h'] ) ? $img_set['three_column_blog_full']['h'] : 106 )),
		    'four_column_blog' => array( 
		        ( !empty( $img_set['four_column_blog_full']['w'] ) ? $img_set['four_column_blog_full']['w'] : 194 ),
		        ( !empty( $img_set['four_column_blog_full']['h'] ) ? $img_set['four_column_blog_full']['h'] : 74 )),

		    'small_post_list' => array( 
		        ( !empty( $img_set['small_post_list_full']['w'] ) ? $img_set['small_post_list_full']['w'] : 60 ),
		        ( !empty( $img_set['small_post_list_full']['h'] ) ? $img_set['small_post_list_full']['h'] : 60 )),
		    'medium_post_list' => array( 
		        ( !empty( $img_set['medium_post_list_full']['w'] ) ? $img_set['medium_post_list_full']['w'] : 200 ),
		        ( !empty( $img_set['medium_post_list_full']['h'] ) ? $img_set['medium_post_list_full']['h'] : 200 )),
		    'large_post_list' => array( 
		        ( !empty( $img_set['large_post_list_full']['w'] ) ? $img_set['large_post_list_full']['w'] : 724 ),
		        ( !empty( $img_set['large_post_list_full']['h'] ) ? $img_set['large_post_list_full']['h'] : 381 )),

		    'portfolio_single_full' => array( 
		        ( !empty( $img_set['portfolio_single_full_full']['w'] ) ? $img_set['portfolio_single_full_full']['w'] : 950 ),
		        ( !empty( $img_set['portfolio_single_full_full']['h'] ) ? $img_set['portfolio_single_full_full']['h'] : 590 )),
		    'additional_posts_grid' => array( 
		        ( !empty( $img_set['additional_posts_grid_full']['w'] ) ? $img_set['additional_posts_grid_full']['w'] : 194 ),
		        ( !empty( $img_set['additional_posts_grid_full']['h'] ) ? $img_set['additional_posts_grid_full']['h'] : 120 )),

		);

		$big_sidebar_images = array(
		    'one_column_portfolio' => array( 
		        ( !empty( $img_set['one_column_portfolio_big']['w'] ) ? $img_set['one_column_portfolio_big']['w'] : 720 ),
		        ( !empty( $img_set['one_column_portfolio_big']['h'] ) ? $img_set['one_column_portfolio_big']['h'] : 380 )),
		    'two_column_portfolio' => array( 
		        ( !empty( $img_set['two_column_portfolio_big']['w'] ) ? $img_set['two_column_portfolio_big']['w'] : 342 ),
		        ( !empty( $img_set['two_column_portfolio_big']['h'] ) ? $img_set['two_column_portfolio_big']['h'] : 176 )),
		    'three_column_portfolio' => array( 
		        ( !empty( $img_set['three_column_portfolio_big']['w'] ) ? $img_set['three_column_portfolio_big']['w'] : 216 ),
		        ( !empty( $img_set['three_column_portfolio_big']['h'] ) ? $img_set['three_column_portfolio_big']['h'] : 120 )),
		    'four_column_portfolio' => array( 
		        ( !empty( $img_set['four_column_portfolio_big']['w'] ) ? $img_set['four_column_portfolio_big']['w'] : 153 ),
		        ( !empty( $img_set['four_column_portfolio_big']['h'] ) ? $img_set['four_column_portfolio_big']['h'] : 75 )),

		    'one_column_blog' => array( 
		        ( !empty( $img_set['one_column_blog_big']['w'] ) ? $img_set['one_column_blog_big']['w'] : 720 ),
		        ( !empty( $img_set['one_column_blog_big']['h'] ) ? $img_set['one_column_blog_big']['h'] : 380 )),
		    'two_column_blog' => array( 
		        ( !empty( $img_set['two_column_blog_big']['w'] ) ? $img_set['two_column_blog_big']['w'] : 342 ),
		        ( !empty( $img_set['two_column_blog_big']['h'] ) ? $img_set['two_column_blog_big']['h'] : 120 )),
		    'three_column_blog' => array( 
		        ( !empty( $img_set['three_column_blog_big']['w'] ) ? $img_set['three_column_blog_big']['w'] : 216 ),
		        ( !empty( $img_set['three_column_blog_big']['h'] ) ? $img_set['three_column_blog_big']['h'] : 76 )),
		    'four_column_blog' => array( 
		        ( !empty( $img_set['four_column_blog_big']['w'] ) ? $img_set['four_column_blog_big']['w'] : 122 ),
		        ( !empty( $img_set['four_column_blog_big']['h'] ) ? $img_set['four_column_blog_big']['h'] : 46 )),

		    'small_post_list' => array( 
		        ( !empty( $img_set['small_post_list_big']['w'] ) ? $img_set['small_post_list_big']['w'] : 80 ),
		        ( !empty( $img_set['small_post_list_big']['h'] ) ? $img_set['small_post_list_big']['h'] : 50 )),
		    'medium_post_list' => array( 
		        ( !empty( $img_set['medium_post_list_big']['w'] ) ? $img_set['medium_post_list_big']['w'] : 200 ),
		        ( !empty( $img_set['medium_post_list_big']['h'] ) ? $img_set['medium_post_list_big']['h'] : 200 )),
		    'large_post_list' => array( 
		        ( !empty( $img_set['large_post_list_big']['w'] ) ? $img_set['large_post_list_big']['w'] : 395 ),
		        ( !empty( $img_set['large_post_list_big']['h'] ) ? $img_set['large_post_list_big']['h'] : 285 )),

		    'portfolio_single_full' => array( 
		        ( !empty( $img_set['portfolio_single_full_big']['w'] ) ? $img_set['portfolio_single_full_big']['w'] : 720 ),
		        ( !empty( $img_set['portfolio_single_full_big']['h'] ) ? $img_set['portfolio_single_full_big']['h'] : 380 )),
		    'additional_posts_grid' => array( 
		        ( !empty( $img_set['additional_posts_grid_big']['w'] ) ? $img_set['additional_posts_grid_big']['w'] : 153 ),
		        ( !empty( $img_set['additional_posts_grid_big']['h'] ) ? $img_set['additional_posts_grid_big']['h'] : 75 )),

		);

		$small_sidebar_images = array(
		    'one_column_portfolio' => array( 
		        ( !empty( $img_set['one_column_portfolio_small']['w'] ) ? $img_set['one_column_portfolio_small']['w'] : 720 ),
		        ( !empty( $img_set['one_column_portfolio_small']['h'] ) ? $img_set['one_column_portfolio_small']['h'] : 380 )),
		    'two_column_portfolio' => array( 
		        ( !empty( $img_set['two_column_portfolio_small']['w'] ) ? $img_set['two_column_portfolio_small']['w'] : 342 ),
		        ( !empty( $img_set['two_column_portfolio_small']['h'] ) ? $img_set['two_column_portfolio_small']['h'] : 176 )),
		    'three_column_portfolio' => array( 
		        ( !empty( $img_set['three_column_portfolio_small']['w'] ) ? $img_set['three_column_portfolio_small']['w'] : 216 ),
		        ( !empty( $img_set['three_column_portfolio_small']['h'] ) ? $img_set['three_column_portfolio_small']['h'] : 120 )),
		    'four_column_portfolio' => array( 
		        ( !empty( $img_set['four_column_portfolio_small']['w'] ) ? $img_set['four_column_portfolio_small']['w'] : 153 ),
		        ( !empty( $img_set['four_column_portfolio_small']['h'] ) ? $img_set['four_column_portfolio_small']['h'] : 75 )),

		    'one_column_blog' => array( 
		        ( !empty( $img_set['one_column_blog_small']['w'] ) ? $img_set['one_column_blog_small']['w'] : 720 ),
		        ( !empty( $img_set['one_column_blog_small']['h'] ) ? $img_set['one_column_blog_small']['h'] : 380 )),
		    'two_column_blog' => array( 
		        ( !empty( $img_set['two_column_blog_small']['w'] ) ? $img_set['two_column_blog_small']['w'] : 342 ),
		        ( !empty( $img_set['two_column_blog_small']['h'] ) ? $img_set['two_column_blog_small']['h'] : 176 )),
		    'three_column_blog' => array( 
		        ( !empty( $img_set['three_column_blog_small']['w'] ) ? $img_set['three_column_blog_small']['w'] : 216 ),
		        ( !empty( $img_set['three_column_blog_small']['h'] ) ? $img_set['three_column_blog_small']['h'] : 120 )),
		    'four_column_blog' => array( 
		        ( !empty( $img_set['four_column_blog_small']['w'] ) ? $img_set['four_column_blog_small']['w'] : 153 ),
		        ( !empty( $img_set['four_column_blog_small']['h'] ) ? $img_set['four_column_blog_small']['h'] : 75 )),

		    'small_post_list' => array( 
		        ( !empty( $img_set['small_post_list_small']['w'] ) ? $img_set['small_post_list_small']['w'] : 80 ),
		        ( !empty( $img_set['small_post_list_small']['h'] ) ? $img_set['small_post_list_small']['h'] : 50 )),
		    'medium_post_list' => array( 
		        ( !empty( $img_set['medium_post_list_small']['w'] ) ? $img_set['medium_post_list_small']['w'] : 200 ),
		        ( !empty( $img_set['medium_post_list_small']['h'] ) ? $img_set['medium_post_list_small']['h'] : 200 )),
		    'large_post_list' => array( 
		        ( !empty( $img_set['large_post_list_small']['w'] ) ? $img_set['large_post_list_small']['w'] : 395 ),
		        ( !empty( $img_set['large_post_list_small']['h'] ) ? $img_set['large_post_list_small']['h'] : 285 )),

		    'portfolio_single_full' => array( 
		        ( !empty( $img_set['portfolio_single_full_small']['w'] ) ? $img_set['portfolio_single_full_small']['w'] : 720 ),
		        ( !empty( $img_set['portfolio_single_full_small']['h'] ) ? $img_set['portfolio_single_full_small']['h'] : 380 )),
		    'additional_posts_grid' => array( 
		        ( !empty( $img_set['additional_posts_grid_small']['w'] ) ? $img_set['additional_posts_grid_small']['w'] : 153 ),
		        ( !empty( $img_set['additional_posts_grid_small']['h'] ) ? $img_set['additional_posts_grid_small']['h'] : 75 )),

		);




		# Slider
		$images_slider = array(
			'nivo_slide' => array( 720, 360 ),
			'floating_slide' => array( 960, 360 ),
			'staged_slide' => array( 960, 360 ),
			'partial_staged_slide' => array( 600, 360 ),
			'partial_gradient_slide' => array( 600, 360 ),
			'overlay_slide' => array( 960, 360 ),
			'full_slide' => array( 720, 360 ),
			'nav_thumbs' => array( 34, 34 )
		);
		
		foreach( $images as $key => $value ) {
			foreach( $value as $img => $size ) {
				$size = str_replace( ' ', '', $size );
				$new_size[$img] = str_replace( 'px', '', $size );
			}
			$images[$key] = $new_size;
		}
	
		foreach( $big_sidebar_images as $key => $value ) {
			foreach( $value as $img => $size ) {
				$size = str_replace( ' ', '', $size );
				$new_size[$img] = str_replace( 'px', '', $size );
			}
			$big_sidebar_images[$key] = $new_size;
		}
	
		foreach( $small_sidebar_images as $key => $value ) {
			foreach( $value as $img => $size ) {
				$size = str_replace( ' ', '', $size );
				$new_size[$img] = str_replace( 'px', '', $size );
			}
			$small_sidebar_images[$key] = $new_size;
		}
	
		# Blog layouts
		switch( $blog_layout ) {
			case "blog_layout1":
				$layout = array(
					'blog_layout' => $blog_layout,
					'main_class' => 'post_grid blog_layout1',
					'post_class' => 'post_grid_module',
					'content_class' => 'post_grid_content',
					'img_class' => 'post_grid_image'
				);
				break;
			case "blog_layout2":
				$layout = array(
					'blog_layout' => $blog_layout,
					'main_class' => 'post_list blog_layout2',
					'post_class' => 'post_list_module',
					'content_class' => 'post_list_content',
					'img_class' => 'post_list_image'
				);
				break;
			case "blog_layout3":
				$columns_num = 2;
				$featured = ( is_archive() || is_search() ) ? false : 1;
				$columns = ( $columns_num == 2 ? 'one_half'
				: ( $columns_num == 3 ? 'one_third'
				: ( $columns_num == 4 ? 'one_fourth'
				: ( $columns_num == 5 ? 'one_fifth'
				: ( $columns_num == 6 ? 'one_sixth'
				: ''
				)))));
			
				$layout = array(
					'blog_layout' => $blog_layout,
					'main_class' => 'post_grid blog_layout3',
					'post_class' => 'post_grid_module',
					'content_class' => 'post_grid_content',
					'img_class' => 'post_grid_image',
					'columns_num' => ( !empty( $columns_num ) ? $columns_num : '' ),
					'featured' => ( !empty( $featured ) ? $featured : '' ),
					'columns' => ( !empty( $columns ) ? $columns : '' )
				);
				break;
		}
	
		$alli->layout['blog'] = $layout;
		$alli->layout['images'] = array_merge( $images, array( 'image_padding' => 10 ) );
		$alli->layout['big_sidebar_images'] = $big_sidebar_images;
		$alli->layout['small_sidebar_images'] = $small_sidebar_images;
		$alli->layout['images_slider'] = $images_slider;
	
	}
	
}

/**
 * Functions & Pluggable functions specific to theme.
 *
 * @package Alliase
 * @subpackage Insignia
 */

if ( !function_exists( 'alli_read_more' ) ) :
/**
 *
 */
function alli_read_more( $url ) {
	$out = '<p><a class="post_more_link" href="' . $url . '">' . __( 'Read More', ALLI_TEXTDOMAIN ) . '</a></p>';
	return $out;
}
endif;

if ( !function_exists( 'alli_full_read_more' ) ) :
/**
 *
 */
function alli_full_read_more( $more_link, $more_link_text ) {
	global $post;
	$out = '<p><a class="post_more_link" href="' . esc_url( get_permalink( $post->ID ) ) . '#more-' . $post->ID . '">' . __( 'Read More', ALLI_TEXTDOMAIN ) . '</a></p>';
	return '[raw]' . $out . '[/raw]';
}
endif;

if ( !function_exists( 'alli_post_img_shadow_before' ) ) :
/**
 *
 */
function alli_post_img_shadow_before() {

}
endif;

if ( !function_exists( 'alli_post_img_shadow_after' ) ) :
/**
 *
 */
function alli_post_img_shadow_after() {

}
endif;

if ( !function_exists( 'alli_custom_post_layout' ) ) :
/**
 *
 */
function alli_custom_post_layout() {
	global $alli;
	
	if( $alli->layout['blog']['blog_layout'] == 'blog_layout2' ) {
		add_action( 'alli_before_entry', 'alli_post_title' );
		add_action( 'alli_before_entry', 'alli_post_meta' );
	} elseif( is_singular( 'portfolio' ) ) {
		add_action( 'alli_before_entry', 'alli_post_title' );
		add_action( 'alli_before_entry', 'alli_portfolio_date' );
	} else {
		add_action( 'alli_before_post', 'alli_post_title' );
		add_action( 'alli_before_post', 'alli_post_meta' );
	}
		
	add_action( 'alli_before_post', 'alli_post_image' );
}
endif;

if ( !function_exists( 'alli_post_meta' ) ) :
/**
 *
 */
function alli_post_meta( $args = array() ) {
	$defaults = array(
		'shortcode' => false,
		'echo' => true
	);
	
	$args = wp_parse_args( $args, $defaults );
	
	extract( $args );
	
	if( is_page() && !$shortcode ) return;
	
	global $alli;
	
	$out = '';
	$meta_options = alli_get_setting( 'disable_meta_options' );
	$_meta = ( is_array( $meta_options ) ) ? $meta_options : array();
	$meta_output = '';

	# Display post meta
	if( !in_array( 'date_meta', $_meta ) )
		$meta_output .= '[post_date text="Posted " format="M j Y"] ';

	if( !in_array( 'author_meta', $_meta ) )
		$meta_output .= '[post_author text="by "] ';

	if( !in_array( 'categories_meta', $_meta ) )
		$meta_output .= '[post_terms taxonomy="category" text=' . __( 'in&nbsp;', ALLI_TEXTDOMAIN ) . '] ';

	if( !in_array( 'comments_meta', $_meta ) )
		$meta_output .= '[post_comments text="with "] ';

	if( !empty( $meta_output ) )
		$out .='<p class="post_meta">' . $meta_output . '</p>';

	if( $echo )
		echo apply_atomic_shortcode( 'post_meta', $out );
	else
		return apply_atomic_shortcode( 'post_meta', $out );
}
endif;

if ( !function_exists( 'alli_post_meta_bottom' ) ) :
/**
 *
 */
function alli_post_meta_bottom( $args = array() ) {
	if( is_page() ) return;
	
	$out = '';
	$meta_options = alli_get_setting( 'disable_meta_options' );
	$_meta = ( is_array( $meta_options ) ) ? $meta_options : array();
	$meta_output = '';
	
	if( !in_array( 'tags_meta', $_meta ) )
		$meta_output .= '[post_terms text=' . __( '<em>Tags:</em>&nbsp;', ALLI_TEXTDOMAIN ) . '] ';

	if( !in_array( 'categories_meta', $_meta ) )
		$meta_output .= '[post_terms taxonomy="category" text=' . __( '<em>Categories:</em>&nbsp;', ALLI_TEXTDOMAIN ) . '] ';

	if( !empty( $meta_output ) )
		$out .='<p class="post_meta_bottom">' . $meta_output . '</p>';
	
	echo apply_atomic_shortcode( 'post_meta_bottom', $out );
}
endif;

if ( !function_exists( 'alli_widget_meta' ) ) :
/**
 *
 */
function alli_widget_meta() {
	return do_shortcode( '[post_date text="" format="F j, Y"]' );
}
endif;

if ( !function_exists( 'alli_before_post_sc' ) ) :
/**
 *
 */
function alli_before_post_sc( $filter_args ) {
	$out = '';
	
	if( in_array( 'post_list_image', $filter_args ) )
		$filter_args = array_merge( $filter_args, array( 'inline_width' => true ) );
	
	if( $filter_args['type'] == 'blog_grid' ) {
		
		if( strpos( $filter_args['disable'], 'title' ) === false )
			$out .= alli_post_title( $filter_args );
		
		if( strpos( $filter_args['disable'], 'meta' ) === false )
			$out .= alli_post_meta( $filter_args );
	}
	
	if( strpos( $filter_args['disable'], 'image' ) === false )
		$out .= alli_get_post_image( $filter_args );
	
	return $out;
}
endif;

if ( !function_exists( 'alli_before_entry_sc' ) ) :
/**
 *
 */
function alli_before_entry_sc( $filter_args ) {
	$out = '';
	
	if( $filter_args['type'] == 'blog_list' ) {
		
		if( strpos( $filter_args['disable'], 'title' ) === false )
			$out .= alli_post_title( $filter_args );
			
		if( strpos( $filter_args['disable'], 'meta' ) === false )
			$out .= alli_post_meta( $filter_args );
	}
	
	return $out;
}
endif;

if ( !function_exists( 'alli_comments_callback' ) ) :
/**
 *
 */
function alli_comments_callback( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;

	$comment_type = get_comment_type( $comment->comment_ID );
	$author = esc_html( get_comment_author( $comment->comment_ID ) );
	$url = esc_url( get_comment_author_url( $comment->comment_ID ) );
	$default_avatar = ( 'pingback' == $comment_type || 'trackback' == $comment_type )
	? THEME_IMAGES_ASSETS . "/gravatar_{$comment_type}.png"
	: THEME_IMAGES_ASSETS . '/gravatar_default.png';

	?><li <?php comment_class() ?> id="comment-<?php comment_ID() ?>">
		<div id="div-comment-<?php comment_ID() ?>"><?php

		/* Display gravatar */
		$avatar = get_avatar( get_comment_author_email( $comment->comment_ID ), apply_filters( "alli_avatar_size", '80' ), $default_avatar, $author );

		if ( $url )
			$avatar = '<a href="' . $url . '" rel="external nofollow" title="' . $author . '">' . $avatar . '</a>';
			
		?><div class="comment-author vcard"><?php
			echo $avatar;
		
			/* Display link and cite if URL is set. */
			if ( $url )
				echo '<cite class="fn" title="' . $url . '"><a href="' . $url . '" title="' . $author . '" class="url" rel="external nofollow">' . $author . '</a></cite>';
			else
				echo '<cite class="fn">' . $author . '</cite>';

			/* Display comment date */
			?><span class="date"><?php printf( __('%1$s', ALLI_TEXTDOMAIN ), get_comment_date( __( apply_filters( "alli_comment_date_format", 'm-d-Y' ) ) ) ); ?></span>
		
		</div>

		<div class="comment-text"><?php
			if ( $comment->comment_approved == '0' ) : ?>
				<p class="alert moderation"><?php _e( 'Your comment is awaiting moderation.', ALLI_TEXTDOMAIN ); ?></p>
				<?php endif; ?>

				<?php comment_text() ?>

				<div class="comment-meta commentmetadata"><?php
					comment_reply_link( array_merge( $args, array( 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) );
					edit_comment_link( __( 'Edit', ALLI_TEXTDOMAIN ), ' ' );
				?></div>
				
		</div><!-- .comment-text -->

		</div><!-- #div-comment-## -->
<?php
}
endif;

if ( !function_exists( 'alli_insignia_menu_script' ) ) :
/**
 *
 */
function alli_insignia_menu_script() {
	
	# Hide Insignia arrow on click
	$out = "jQuery('.main_navigation').click(function() {
	  jQuery('.menu_arrow').css('display','none');
	});";
	
	$out = '<script type="text/javascript">
	/* <![CDATA[ */
	' . $out . '
	/* ]]> */
	</script>';
	
	echo preg_replace( "/(\r\n|\r|\n)\s*/i", '', $out );
}
endif;

?>
