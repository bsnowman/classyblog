<?php
/*
 Plugin Name: Easy WP SEO
 Plugin URI: http://www.easywpseo.com
 Description: Just push a button, and Easy WP SEO analyzes your post, page, or custom post type for 23 proven on-page SEO factors. It displays an SEO and keyword density score that reveals how well your content is optimized for the search engines, and provides you with a detailed checklist of suggested SEO tweaks.
 Version: 1.6.2
 Author: Chris Landrum
 Author URI: http://www.easywpseo.com
 */

if (!function_exists ('is_admin'))
{
	header('Status: 403 Forbidden');
	header('HTTP/1.1 403 Forbidden');
	exit();
}
else
{
	// Minimum Requirements

	// Requires PHP 5.0+
	if (version_compare(PHP_VERSION, '5.0.0', '<'))
	{
		// Deactivate Plugin
		if(function_exists('deactivate_plugins'))
		{
			deactivate_plugins(plugin_basename(__FILE__), true);
		}

		// Display Error Message
		add_action('admin_notices', create_function('', "echo '<div id=\"message\" class=\"error\"><p><strong>ERROR:</strong> Sorry, but Easy WP SEO requires PHP 5.0 or newer. Your version is '.phpversion().'. Please, ask your web host to upgrade to PHP 5.0.</p></div>';"));
	}

	// Requires Wordpress 2.8.4+
	global $wp_version;
	if (version_compare( $wp_version, '2.8.4', '<' ))
	{
		// Deactivate Plugin
		if(function_exists('deactivate_plugins'))
		{
			deactivate_plugins(plugin_basename(__FILE__), true);
		}

		// Display Error Message
		add_action('admin_notices', create_function('', "echo '<div id=\"message\" class=\"error\"><p><strong>ERROR:</strong> Sorry, but Easy WP SEO requires Wordpress 2.8.4 or newer. Your version is $wp_version. Please, upgrade to the latest version of Wordpress.</p></div>';"));
	}

	global $blog_id;
	$opseoUploadDir = wp_upload_dir();

	// Constants
	define('OPSEO_NAME', 'Easy WP SEO');
	define('OPSEO_VERSION', '1.6.2');
	define('OPSEO_URL', 'http://www.easywpseo.com');
	define('OPSEO_PREFIX', 'onpageseo');
	define('OPSEO_SITE_URL', get_bloginfo('wpurl'));
	define('OPSEO_WP_ADMIN_URL', trailingslashit(OPSEO_SITE_URL).'wp-admin');
	define('OPSEO_PLUGIN_FULL_PATH', plugin_dir_path(__FILE__));
	define('OPSEO_PLUGIN_PATH', plugin_basename(__FILE__));
	define('OPSEO_PLUGIN_DIR_NAME', dirname(OPSEO_PLUGIN_PATH));
	define('OPSEO_POST_META_DATA', 'onpageseo_post_meta_data');
	define('OPSEO_PLUGIN_URL', plugins_url('', __FILE__));
	define('OPSEO_UPLOAD_PATH', $opseoUploadDir['basedir']);
	define('OPSEO_UPLOAD_URL', $opseoUploadDir['baseurl']);
	define('OPSEO_CACHE_PATH', trailingslashit(OPSEO_UPLOAD_PATH).OPSEO_PREFIX.'/'.$blog_id);
	define('OPSEO_CACHE_URL', trailingslashit(OPSEO_UPLOAD_URL).OPSEO_PREFIX.'/'.$blog_id);

	// Administrative
	if(is_admin())
	{
		$opseoArgs = array();

		// Include Admin Class
		require_once('onpageseo-admin.php');

		// Initialize New Object
		$onPageSEO = new onPageSEOAdmin($opseoArgs);
	}
	// Client
	else
	{
		// Get Plugin Settings
		$opseoOptions = get_option('onpageseo_options');

		if(is_array($opseoOptions) && isset($opseoOptions['decoration_type']) && $opseoOptions['decoration_type'] == 'client')
		{
			// Include Client Class
			require_once('onpageseo-client.php');

			// Initialize Client Object
			$client = new OnPageSEOClient(&$opseoOptions);
		}
	}
}
?>