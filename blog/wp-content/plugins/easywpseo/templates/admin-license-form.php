				<p>Enter your <?php echo OPSEO_NAME;?> License information:</p>

				<table class="form-table">

				<tr valign="top">
				<th scope="row"><label for="onpageseo_options[license_email]">Email Address</label></th>
				<td><input type="text" name="onpageseo_options[license_email]" value="<?php echo $this->options['license_email'];?>" class="large-text" /><br /><em>Enter the email address you used to purchase the software.</em></td>
				</tr>

				<tr valign="top" style="background-color:rgb(238,238,238);">
				<th scope="row"><label for="onpageseo_options[license_serial]">Serial Number</label></th>
				<td><input type="text" name="onpageseo_options[license_serial]" value="<?php echo $this->options['license_serial'];?>" class="large-text" /><br /><a href="http://www.easywpseo.com/forgot-serial-number/" target="_blank">Forgot your serial number?</a></td>
				</tr>

				</table>