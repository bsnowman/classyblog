			<div class="updated" style="margin-top: 20px;">
				<form name="clearkeywords" id="clearkeywords" method="post" action="<?php echo $_SERVER['PHP_SELF'];?>?page=onpageseo-manage-keywords">

					<table border="0" cellspacing="0" cellpadding="0" width="100%">
					<tr>
					<td valign="top" width="50%">

						<table border="0" cellspacing="0" cellpadding="0">
						<tr><td><h3 style="padding:0 !important;margin:10px 0 !important;">Import Keywords<a href="<?php echo OPSEO_PLUGIN_URL;?>/templates/admin-video.php?vidid=RVyeuHIowXM&#038;TB_iframe=1&width=640&height=925" class="thickbox" title="Import Keywords"><img src="<?php echo OPSEO_PLUGIN_URL;?>/images/help.png" alt="Help" title="Help" /></a></h3><p>Import keywords from other plugins (skips Easy WP SEO keywords that already exist).</p></td></tr>
						<tr>

						<td valign="top">

						<p><input type="hidden" name="updated" value="true" />
						<input onclick="return confirm('This will import all post/page keywords from SEOPressor. Continue?')" class="button" type="submit" name="<?php echo OPSEO_PREFIX;?>_import_seopressor" value="Import SEOPressor Keywords" style="margin-top: 5px !important;" />

						<input onclick="return confirm('This will import all post/page keywords from ClickBump SEO!. Continue?')" class="button" type="submit" name="<?php echo OPSEO_PREFIX;?>_import_clickbump" value="Import ClickBump SEO! Keywords" style="margin-top: 5px !important;" />

						<input onclick="return confirm('This will import all post/page keywords from WP SEO Beast. Continue?')" class="button" type="submit" name="<?php echo OPSEO_PREFIX;?>_import_seobeast" value="Import SEO Beast Keywords" style="margin-top: 5px !important;" />

						<input onclick="return confirm('This will import all post/page keywords from BloggerHigh SEO. Continue?')" class="button" type="submit" name="<?php echo OPSEO_PREFIX;?>_import_bloggerhigh" value="Import BloggerHigh SEO Keywords" style="margin-top: 5px !important;" /></p></td>

						</tr>
						</table>

					</td>

					<td valign="top" width="50%">

						<table border="0" cellspacing="0" cellpadding="0">
						<tr><td><h3 style="padding:0 !important;margin:10px 0 !important;">System Reset/Uninstall</h3><p>Warning: There is no way to recover this data later.</p></td></tr>
						<tr>

						<td valign="top">

						<p><input onclick="return confirm('This will clear all keywords and scores from all posts/pages. Continue?')" class="button" type="submit" name="<?php echo OPSEO_PREFIX;?>_clear_all_keywords" value="Clear All Keywords" style="margin-top: 5px !important;" />

						<input type="hidden" name="updated" value="true" /><input onclick="return confirm('This will reset all options to the default settings. Continue?')" class="button" type="submit" name="<?php echo OPSEO_PREFIX;?>_reset_options" value="Reset Options To Defaults" style="margin-top: 5px !important;" />

						<input onclick="return confirm('This will clear all keywords and scores from all posts/pages and uninstall the <?php echo OPSEO_NAME;?> plugin. Continue?')" class="button" type="submit" name="<?php echo OPSEO_PREFIX;?>_uninstall_plugin" value="Uninstall <?php echo OPSEO_NAME;?>" style="margin-top: 5px !important;" /></p></td>

						</tr>
						</table>

					</td>
					</tr>
					</table>


				</form>
			</div>