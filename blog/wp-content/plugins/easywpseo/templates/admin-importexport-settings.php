		<div id="opseo-import-export-tabs"> 
			<ul> 
				<li class="importexporttabs"><a href="#opseo-import" style="margin:0 !important;">Import Settings</a></li> 
				<li class="importexporttabs"<?php if($this->license->isLicenseError()){ echo ' style="display:none;"';}?>><a href="#opseo-export">Export Settings</a></li> 
			</ul> 
 
			<div id="opseo-import" class="import-export-tabs-panel">

				<h4 style="padding:0 0 5px 0 !important;margin:0 0 10px 0 !important;border-bottom:1px solid rgb(200,200,200);">Import Settings</h4> 

				<p>Import settings from an Easy WP SEO export file.</p>

				<form method="post" action="admin.php?page=onpageseo-settings" enctype="multipart/form-data">
					<p><input type="file" name="file" id="onpageseo-import-settings-file" /></p>
					<p><input onclick="return confirm('Warning: This will import the settings from an Easy WP SEO export file and overwrite your current settings. Continue?')" class="button-primary" type="submit" name="<?php echo OPSEO_PREFIX;?>_import_settings" value="Import" style="margin-top: 5px !important;" /></p>
				</form>

			</div>

			<div id="opseo-export" class="import-export-tabs-panel"<?php if($this->license->isLicenseError()){ echo ' style="display:none;"';}?>>

				<h4 style="padding:0 0 5px 0 !important;margin:0 0 10px 0 !important;border-bottom:1px solid rgb(200,200,200);">Export Settings</h4>

				<p>Export Easy WP SEO settings from your Wordpress site.</p>

				<form method="post" action="admin.php?page=onpageseo-settings">
					<input type="hidden" name="action" value="export-settings" />
					<p><input onclick="return confirm('This will export the Easy WP SEO settings. Continue?')" class="button-primary" type="submit" name="<?php echo OPSEO_PREFIX;?>_export_settings" value="Export" style="margin-top: 5px !important;" /></p>
				</form>

			</div>

		</div>