<?php
		// Post Meta Data Already Exists

				global $post;

				$lcMainKeyword = strtolower($this->postMeta['onpageseo_global_settings']['MainKeyword']);

					echo '<style>

						.opseo-report-h1
						{
							color: #0972a9 !important;
							margin: 10px 0 30px 0 !important;
						}

						.opseo-report-h2
						{
							color: #03486e !important;
							margin: 0 0 20px 0 !important;
						}

						.opseo-report-h2-margin
						{
							color: #03486e !important;
							margin: 20px 0 !important;
						}

						.opseo-report-h3
						{
							color: #03486e !important;
							margin: 0 0 10px 0 !important;
							padding-bottom: 0 0 3px 0 !important;
							border-bottom: 1px solid rgb(3,72,110);
						}

						.opseo-report-p
						{
							margin: 0 0 10px 0 !important;
							padding: 0 0 5px 0 !important;
						}

						.opseo-report-p-margin
						{
							margin: 0 0 20px 0 !important;
							padding: 0 0 5px 0 !important;
						}

						.opseo-report-p-last
						{
							margin: 0 0 30px 0 !important;
							padding: 0 0 5px 0 !important;
						}

					</style>';

					echo '<h1 class="opseo-report-h1">On-Page SEO Report</h1>

					<h2 class="opseo-report-h2">Post/Page Information</h2>

					<h3 class="opseo-report-h3">Permalink</h3>';

					global $pagenow;
					$permalink = '';

					// URL Analyzer
					if($type == 2) { $permalink = $opseoURL; }
					// Post or Page
					else { $permalink = get_permalink($this->postID); }

					echo '<p class="opseo-report-p-margin"><a href="'.$permalink.'">'.$permalink.'</a></p>

					<h3 class="opseo-report-h3">Title Tag</h3>';

					echo '<p class="opseo-report-p-margin">';
					for($iG = 0; $iG < sizeof($this->seoReport['Title']); $iG++)
					{
						echo '&bull;&nbsp;'.$this->seoReport['Title'][$iG].' (<i>'.strlen(utf8_decode($this->seoReport['Title'][$iG])).' characters</i>)';
					}
					echo '</p>';

					echo '<h3 class="opseo-report-h3">Meta Tags</h3>


					<table width="100%" cellspacing="0" cellpadding="0" border="0" style="margin:0 !important;padding:0 !important;width:100%;">
					<tr>
					<td valign="top" style="width:90px;"><p class="opseo-report-p"><b>Description:</b></p></td>
					<td valign="top"><p class="opseo-report-p">';

					for($iG = 0; $iG < sizeof($this->seoReport['DescriptionMetaTag']); $iG++)
					{
						echo '&bull;&nbsp;'.$this->seoReport['DescriptionMetaTag'][$iG].' (<i>'.strlen(utf8_decode($this->seoReport['DescriptionMetaTag'][$iG])).' characters</i>)';
					}

					echo '</p></td></tr></table>



					<table width="100%" cellspacing="0" cellpadding="0" border="0" style="margin:0 !important;padding:0 !important;">
					<tr>
					<td valign="top" style="width:90px;"><p class="opseo-report-p"><b>Keywords:</b></p></td>
					<td valign="top"><p class="opseo-report-p-margin">';

					for($iG = 0; $iG < sizeof($this->seoReport['KeywordsMetaTag']); $iG++)
					{
						echo '&bull;&nbsp;'.$this->seoReport['KeywordsMetaTag'][$iG].'<br />';
					}

					echo '</p></td></tr></table>




					<h3 class="opseo-report-h3">Heading Tags</h3>

					<table width="100%" cellspacing="0" cellpadding="0" border="0" style="margin:0 !important;padding:0 !important;">
					<tr>
					<td valign="top" style="width:35px;"><p class="opseo-report-p"><b>H1:</b></p></td>
					<td valign="top"><p class="opseo-report-p">';

					for($iG = 0; $iG < sizeof($this->seoReport['H1']); $iG++)
					{
						echo '&bull;&nbsp;'.$this->seoReport['H1'][$iG] . '<br />';
					}

					echo '</p></td></tr></table>

					<table width="100%" cellspacing="0" cellpadding="0" border="0" style="margin:0 !important;padding:0 !important;">
					<tr>
					<td valign="top" style="width:35px;"><p class="opseo-report-p"><b>H2:</b></p></td>
					<td valign="top"><p class="opseo-report-p">';

					for($iG = 0; $iG < sizeof($this->seoReport['H2']); $iG++)
					{
						echo '&bull;&nbsp;'.$this->seoReport['H2'][$iG] . '<br />';
					}

					echo '</p></td></tr></table>

					<table width="100%" cellspacing="0" cellpadding="0" border="0" style="margin:0 !important;padding:0 !important;">
					<tr>
					<td valign="top" style="width:35px;"><p class="opseo-report-p"><b>H3:</b></p></td>
					<td valign="top"><p class="opseo-report-p-margin">';

					for($iG = 0; $iG < sizeof($this->seoReport['H3']); $iG++)
					{
						echo '&bull;&nbsp;'.$this->seoReport['H3'][$iG] . '<br />';
					}

					echo '</p></td></tr></table>

					<h3 class="opseo-report-h3">Content</h3>';

					// Post or Page
					//if($type == 1)
					//{
					//	echo '<p class="opseo-report-p"><b>Post/Page Title:</b> '.$post->post_title.' (<i>'.strlen($post->post_title).' characters</i>)</p>';
					//}

					echo '<p class="opseo-report-p"><b>First 100 Words:</b> '.$this->seoReport['First100Words'].'</p>

					<p class="opseo-report-p-last"><b>Last 100 Words:</b> '.$this->seoReport['Last100Words'].'</p>

					<h2 class="opseo-report-h2">Readability</h2>';

					$gradeAverage = (int)(($this->postMeta['onpageseo_global_settings']['FleschGradeLevel'] + $this->postMeta['onpageseo_global_settings']['GunningFogScore'] + $this->postMeta['onpageseo_global_settings']['ColemanLiauIndex'] + $this->postMeta['onpageseo_global_settings']['SMOGIndex'] + $this->postMeta['onpageseo_global_settings']['AutomatedReadabilityIndex']) /  5);

					$ageGroup = '';
					if($gradeAverage < 1) { $ageGroup = 'under 6'; }
					elseif($gradeAverage == 1) { $ageGroup = '6 to 7'; }
					elseif($gradeAverage == 2) { $ageGroup = '7 to 8'; }
					elseif($gradeAverage == 3) { $ageGroup = '8 to 9'; }
					elseif($gradeAverage == 4) { $ageGroup = '9 to 10'; }
					elseif($gradeAverage == 5) { $ageGroup = '10 to 11'; }
					elseif($gradeAverage == 6) { $ageGroup = '11 to 12'; }
					elseif($gradeAverage == 7) { $ageGroup = '12 to 13'; }
					elseif($gradeAverage == 8) { $ageGroup = '13 to 14'; }
					elseif($gradeAverage == 9) { $ageGroup = '14 to 15'; }
					elseif($gradeAverage == 10) { $ageGroup = '15 to 16'; }
					elseif($gradeAverage == 11) { $ageGroup = '16 to 17'; }
					elseif($gradeAverage == 12) { $ageGroup = '17 to 18'; }
					else { $ageGroup = 'over 18'; }

					echo '<h3 class="opseo-report-h3">Analysis</h3>';

					echo '<p class="opseo-report-p-margin">This content is '.strtolower($this->postMeta['onpageseo_global_settings']['FleschLevel']).' with an average grade level of ' . $gradeAverage . ' and should be understood by '.$ageGroup.' year olds.</p>';

					echo '<h3 class="opseo-report-h3">Statistics</h3>';

					echo '<p class="opseo-report-p-margin">&bull;&nbsp;'.$this->postMeta['onpageseo_global_settings']['SentenceCount'].' sentences<br />&bull;&nbsp;'.$this->postMeta['onpageseo_global_settings']['WordCount'].' words<br />&bull;&nbsp;'.$this->postMeta['onpageseo_global_settings']['AverageWordsPerSentence'].' words per sentence<br />&bull;&nbsp;'.$this->postMeta['onpageseo_global_settings']['AverageSyllablesPerWord'].' syllables per word<br />&bull;&nbsp;'.$this->postMeta['onpageseo_global_settings']['ComplexWordsNumber'].' complex words (3+ syllables)<br />&bull;&nbsp;'.$this->postMeta['onpageseo_global_settings']['ComplexWordsPercentage'].'% of words are complex</p>';

					echo '<h3 class="opseo-report-h3">Readability Scores</h3>';

					echo '<p class="opseo-report-p"><b>Flesch-Kincaid Reading Ease:</b> '.$this->postMeta['onpageseo_global_settings']['FleschEase'].'</p>

					<p class="opseo-report-p"><b>Flesch-Kincaid Grade Level:</b> '.$this->postMeta['onpageseo_global_settings']['FleschGradeLevel'].'</p>

					<p class="opseo-report-p"><b>Gunning-Fog Score:</b> '.$this->postMeta['onpageseo_global_settings']['GunningFogScore'].'</p>

					<p class="opseo-report-p"><b>Coleman-Liau Index:</b> '.$this->postMeta['onpageseo_global_settings']['ColemanLiauIndex'].'</p>

					<p class="opseo-report-p"><b>SMOG Index:</b> '.$this->postMeta['onpageseo_global_settings']['SMOGIndex'].'</p>

					<p class="opseo-report-p-last"><b>Automated Readability Index:</b> '.$this->postMeta['onpageseo_global_settings']['AutomatedReadabilityIndex'].'</p>';


					// Primary Keyword
					echo '<h2 class="opseo-report-h2-margin">Primary Keyword &mdash; <i>'.$this->postMeta['onpageseo_global_settings']['MainKeyword'].'</i></h2>';
					$this->displaySEOKeywordReport($lcMainKeyword);

					// Secondary Keywords

					foreach($this->postMeta as $key=>$val)
					{
						if(($key != 'onpageseo_global_settings') && ($key != $lcMainKeyword))
						{
							echo '<h2 class="opseo-report-h2-margin">Secondary Keyword &mdash; <i>'.$this->postMeta[$key]['Keyword'].'</i></h2>';
							$this->displaySEOKeywordReport($key);
						}
					}

?>
