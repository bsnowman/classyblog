		<?php $this->adminHeader('onpageseo-settings', 'Settings'); ?>

		<form action="options.php" method="post">
		<?php settings_fields('onpageseo_settings');?>

		<div id="opseo-tabs" class="inside">
			<ul>
				<li class="opseotabs"><a href="#opseo-style" style="margin:0 !important;">Automatic Decoration</a></li>
				<li class="opseotabs"><a href="#opseo-misc">Miscellaneous Settings</a></li>
				<li class="opseotabs"><a href="#opseo-keyword">Keyword Settings</a></li>
				<li class="opseotabs"><a href="#opseo-password-protection">Password Protection</a></li>
				<li class="opseotabs"><a href="#opseo-copyscape">Copyscape Settings</a></li>
				<li class="opseotabs"<?php if($this->licenseHide){ echo ' style="display:none;"';}?>><a href="#opseo-license">License Settings</a></li>
			</ul>

			<div id="opseo-style" class="opseo-tabs-panel">

				<p>Here are the settings if you want to automatically decorate your primary keywords with bold, italic, or underline styling; insert your primary keywords into the ALT attribute of images; or add rel="nofollow" attributes to links:</p>

				<h4 style="padding:10px 0 5px 0 !important;margin:15px 0 10px 0 !important;border-bottom:1px solid rgb(200,200,200);">Automatic Decorations</h4>

				<table class="form-table">

				<tr valign="top">
				<th scope="row"><label for="onpageseo_options[decoration_type]">Decoration Type</label><br /><span style="font-size:12px;"><em>Method To Decorate Content</em></span></th>
				<td><select name="onpageseo_options[decoration_type]" id="onpageseo_options[decoration_type]">

					<option value="" label="Turned Off"<?php selected($this->options['decoration_type'], "");?>>Turned Off</option>
					<option value="client" label="Client-Side"<?php selected($this->options['decoration_type'], "client");?>>Client-Side</option>
					<option value="admin" label="Admin-Side"<?php selected($this->options['decoration_type'], "admin");?>>Admin-Side</option>

					</select></td>

				</tr>

				</table>


				<h4 style="padding:10px 0 5px 0 !important;margin:15px 0 10px 0 !important;border-bottom:1px solid rgb(200,200,200);">Keyword Decorations</h4>

				<table class="form-table">

				<tr valign="top" style="background-color:rgb(238,238,238);">
				<th scope="row"><label for="onpageseo_options[bold_keyword]">Bold Keyword </label></th>
				<td><input type="checkbox" name="onpageseo_options[bold_keyword]" value="1"<?php checked($this->options['bold_keyword'], 1);?> /> <label for="onpageseo_options[bold_keyword]">Automatically <strong>bold</strong> the keyword.</label></td>
				</tr>

				<tr valign="top" style="background-color:rgb(238,238,238);">

				<th scope="row"><label for="b">Bold Style</label></th>
				<td>

					<input type="radio" name="onpageseo_options[bold_style]" value="b"<?php checked($this->options['bold_style'], 'b');?> /> <label title="b">&lt;b&gt;...&lt;/b&gt;</label><br />
					<input type="radio" name="onpageseo_options[bold_style]" value="strong"<?php checked($this->options['bold_style'], 'strong');?> /> <label title="strong">&lt;strong&gt;...&lt;/strong&gt;</label><br />
					<input type="radio" name="onpageseo_options[bold_style]" value="fontweightbold"<?php checked($this->options['bold_style'], 'fontweightbold');?> /> <label title="fontweightbold">&lt;span style="font-weight:bold;"&gt;...&lt;/span&gt;</label>
				</td>
				</tr>

				<tr valign="top">
				<th scope="row"><label for="onpageseo_options[italic_keyword]">Italicize Keyword </label></th>
				<td><input type="checkbox" name="onpageseo_options[italic_keyword]" value="1"<?php checked($this->options['italic_keyword'], 1);?> /> <label for="onpageseo_options[italic_keyword]">Automatically <em>italicize</em> the keyword</label></td>

				</tr>

				<tr valign="top">
				<th scope="row"><label for="onpageseo_options[italic_style]">Italic Style</label></th>
				<td>
					<input type="radio" name="onpageseo_options[italic_style]" value="i"<?php checked($this->options['italic_style'], 'i');?> /> <label title="i">&lt;i&gt;...&lt;/i&gt;</label><br />
					<input type="radio" name="onpageseo_options[italic_style]" value="em"<?php checked($this->options['italic_style'], 'em');?> /> <label title="em">&lt;em&gt;...&lt;/em&gt;</label><br />
					<input type="radio" name="onpageseo_options[italic_style]" value="fontstyleitalic"<?php checked($this->options['italic_style'], 'fontstyleitalic');?> /> <label title="fontstyleitalic">&lt;span style="font-style:italic;"&gt;...&lt;/span&gt;</label>
				</td>
				</tr>


				<tr valign="top" style="background-color:rgb(238,238,238);">
				<th scope="row"><label for="onpageseo_options[underline_keyword]">Underline Keyword </label></th>
				<td><input type="checkbox" name="onpageseo_options[underline_keyword]" value="1"<?php checked($this->options['underline_keyword'], 1);?> /> <label for="onpageseo_options[underline_keyword]">Automatically <u>underline</u> the keyword</label></td>

				</tr>

				<tr valign="top" style="background-color:rgb(238,238,238);">
				<th scope="row"><label for="onpageseo_options[underline_style]">Underline Style</label></th>
				<td>
					<input type="radio" name="onpageseo_options[underline_style]" value="u"<?php checked($this->options['underline_style'], 'u');?> /> <label title="u">&lt;u&gt;...&lt;/u&gt;</label><br />
					<input type="radio" name="onpageseo_options[underline_style]" value="fontdecorationunderline"<?php checked($this->options['underline_style'], 'fontdecorationunderline');?> /> <label title="fontdecorationunderline">&lt;span style="text-decoration:underline;"&gt;...&lt;/span&gt;</label>
				</td>
				</tr>

				</table>


				<h4 style="padding:10px 0 5px 0 !important;margin:15px 0 10px 0 !important;border-bottom:1px solid rgb(200,200,200);">Images</h4>

				<table class="form-table">

				<tr valign="top">
				<th scope="row"><label for="onpageseo_options[image_alt]">Image ALT Attribute</label></th>
				<td><input type="checkbox" name="onpageseo_options[image_alt]" value="1"<?php checked($this->options['image_alt'], 1);?> /> <label for="onpageseo_options[image_alt]">Automatically add keyword to image ALT attribute</label></td>
				</tr>

				</table>

				<h4 style="padding:10px 0 5px 0 !important;margin:15px 0 10px 0 !important;border-bottom:1px solid rgb(200,200,200);">Links</h4>

				<table class="form-table">

				<tr valign="top" style="background-color:rgb(238,238,238);">
				<th scope="row"><label for="onpageseo_options[no_follow]">No Follow</label></th>
				<td><input type="checkbox" name="onpageseo_options[no_follow]" value="1"<?php checked($this->options['no_follow'], 1);?> /> <label for="onpageseo_options[no_follow]">Automatically add <code>rel="nofollow"</code> to external links</label></td>
				</tr>


				<tr valign="top">
				<th scope="row"><label for="onpageseo_options[no_follow]">No Follow (White List URLs)</label><br /><span style="font-size:12px;"><em>These Links Will Not Receive No Follow</em></span></th>
				<td><textarea name="onpageseo_options[no_follow_white_list]" id="onpageseo_options[no_follow_white_list]" rows="8" cols="70"><?php echo $this->options['no_follow_white_list'];?></textarea></td>
				</tr>


				<tr valign="top" style="background-color:rgb(238,238,238);">
				<th scope="row"><label for="onpageseo_options[link_target]">Open External Links In New Browser Window</label></th>
				<td><input type="checkbox" name="onpageseo_options[link_target]" value="1"<?php checked($this->options['link_target'], 1);?> /> <label for="onpageseo_options[link_target]">Automatically add <code>target="_blank"</code> to external links <i>(so URL opens in new browser window)</i></label></td>
				</tr>

				</table>

			</div>

			<div id="opseo-misc" class="opseo-tabs-panel">
			
				<p>Here is how to change some of the default values that affect the calculation of the on-page SEO score for posts and pages; and determine how many posts appear on the "Manage Keywords" screen, how many posts or pages appear on the "Internal Links" section and images appear on the "Images" section under the Misc tab on the Post/Page Editor screen metabox:</p>


				<h4 style="padding:10px 0 5px 0 !important;margin:15px 0 10px 0 !important;border-bottom:1px solid rgb(200,200,200);">On-Page SEO Factors</h4>

				<table class="form-table">

				<tr valign="top" style="background-color:rgb(238,238,238);">
				<th scope="row"><label for="onpageseo_options[title_factor]">Title Tag</label></th>
				<td><input type="checkbox" name="onpageseo_options[title_factor]" value="1"<?php checked($this->options['title_factor'], 1);?> /> <label for="onpageseo_options[title_factor]">Title contains keyword.</label><br />
				<input type="checkbox" name="onpageseo_options[title_beginning_factor]" value="1"<?php checked($this->options['title_beginning_factor'], 1);?> /> <label for="onpageseo_options[title_beginning_factor]">Title begins with keyword.</label><br />
				<input type="checkbox" name="onpageseo_options[title_words_factor]" value="1"<?php checked($this->options['title_words_factor'], 1);?> /> <label for="onpageseo_options[title_words_factor]">Title contains at least <?php echo $this->options['title_length_minimum'];?> words.</label><br />
				<input type="checkbox" name="onpageseo_options[title_characters_factor]" value="1"<?php checked($this->options['title_characters_factor'], 1);?> /> <label for="onpageseo_options[title_characters_factor]">Title contains up to <?php echo $this->options['title_length_maximum'];?> characters.</label></td>
				</tr>

				<tr valign="top">
				<th scope="row"><label for="onpageseo_options[url_factor]">URL</label></th>
				<td><input type="checkbox" name="onpageseo_options[url_factor]" value="1"<?php checked($this->options['url_factor'], 1);?> /> <label for="onpageseo_options[url_factor]">Permalink contains your keyword.</label></td>
				</tr>

				<tr valign="top" style="background-color:rgb(238,238,238);">
				<th scope="row"><label for="onpageseo_options[description_meta_factor]">Meta Tags</label></th>
				<td><input type="checkbox" name="onpageseo_options[description_meta_factor]" value="1"<?php checked($this->options['description_meta_factor'], 1);?> /> <label for="onpageseo_options[description_meta_factor]">Description meta tag contains keyword.</label><br />
				<input type="checkbox" name="onpageseo_options[description_chars_meta_factor]" value="1"<?php checked($this->options['description_chars_meta_factor'], 1);?> /> <label for="onpageseo_options[description_chars_meta_factor]">Description meta tag contains up to <?php echo $this->options['description_meta_tag_maximum'];?> characters.</label><br />
				<input type="checkbox" name="onpageseo_options[description_beginning_meta_factor]" value="1"<?php checked($this->options['description_beginning_meta_factor'], 1);?> /> <label for="onpageseo_options[description_beginning_meta_factor]">Description meta tag begins with keyword.</label><br />
				<input type="checkbox" name="onpageseo_options[keywords_meta_factor]" value="1"<?php checked($this->options['keywords_meta_factor'], 1);?> /> <label for="onpageseo_options[keywords_meta_factor]">Keywords meta tag contains keyword.</label></td>
				</tr>

				<tr valign="top">
				<th scope="row"><label for="onpageseo_options[h1_factor]">Heading Tags</label></th>
				<td><input type="checkbox" name="onpageseo_options[h1_factor]" value="1"<?php checked($this->options['h1_factor'], 1);?> /> <label for="onpageseo_options[h1_factor]">H1 tag contains keyword.</label><br />
				<input type="checkbox" name="onpageseo_options[h1_beginning_factor]" value="1"<?php checked($this->options['h1_beginning_factor'], 1);?> /> <label for="onpageseo_options[h1_beginning_factor]">H1 tag begins with keyword.</label><br />
				<input type="checkbox" name="onpageseo_options[h2_factor]" value="1"<?php checked($this->options['h2_factor'], 1);?> /> <label for="onpageseo_options[h2_factor]">H2 tag contains keyword.</label><br />
				<input type="checkbox" name="onpageseo_options[h3_factor]" value="1"<?php checked($this->options['h3_factor'], 1);?> /> <label for="onpageseo_options[h3_factor]">H3 tag contains keyword.</label></td>
				</tr>


				<tr valign="top" style="background-color:rgb(238,238,238);">
				<th scope="row"><label for="onpageseo_options[content_words_factor]">Content</label></th>
				<td><input type="checkbox" name="onpageseo_options[content_words_factor]" value="1"<?php checked($this->options['content_words_factor'], 1);?> /> <label for="onpageseo_options[content_words_factor]">Content contains at least <?php echo $this->options['post_content_length'];?> words.</label><br />
				<input type="checkbox" name="onpageseo_options[content_kw_density_factor]" value="1"<?php checked($this->options['content_kw_density_factor'], 1);?> /> <label for="onpageseo_options[content_kw_density_factor]">Content has <?php echo $this->options['keyword_density_minimum'];?>-<?php echo $this->options['keyword_density_maximum'];?>% keyword density.</label><br />
				<input type="checkbox" name="onpageseo_options[content_first_factor]" value="1"<?php checked($this->options['content_first_factor'], 1);?> /> <label for="onpageseo_options[content_first_factor]">Content contains keyword in the first 50-100 words.</label><br />
				<input type="checkbox" name="onpageseo_options[content_alt_factor]" value="1"<?php checked($this->options['content_alt_factor'], 1);?> /> <label for="onpageseo_options[content_alt_factor]">Content contains at least one image with keyword in ALT attribute.</label><br />
				<input type="checkbox" name="onpageseo_options[content_bold_factor]" value="1"<?php checked($this->options['content_bold_factor'], 1);?> /> <label for="onpageseo_options[content_bold_factor]">Content contains at least one bold keyword.</label><br />
				<input type="checkbox" name="onpageseo_options[content_italic_factor]" value="1"<?php checked($this->options['content_italic_factor'], 1);?> /> <label for="onpageseo_options[content_italic_factor]">Content contains at least one italicized keyword.</label><br />
				<input type="checkbox" name="onpageseo_options[content_underline_factor]" value="1"<?php checked($this->options['content_underline_factor'], 1);?> /> <label for="onpageseo_options[content_underline_factor]">Content contains at least one underlined keyword.</label><br />
				<input type="checkbox" name="onpageseo_options[content_external_link_factor]" value="1"<?php checked($this->options['content_external_link_factor'], 1);?> /> <label for="onpageseo_options[content_external_link_factor]">Content contains keyword in anchor text of at least one external link.</label><br />
				<input type="checkbox" name="onpageseo_options[content_internal_link_factor]" value="1"<?php checked($this->options['content_internal_link_factor'], 1);?> /> <label for="onpageseo_options[content_internal_link_factor]">Content contains keyword in anchor text of at least one internal link.</label><br />
				<input type="checkbox" name="onpageseo_options[content_last_factor]" value="1"<?php checked($this->options['content_last_factor'], 1);?> /> <label for="onpageseo_options[content_last_factor]">Content contains keyword in the last 50-100 words.</label>
				<input type="hidden" name="onpageseo_options[factor_update]" value="1" /></td>
				</tr>

				</table>





				<h4 style="padding:10px 0 5px 0 !important;margin:15px 0 10px 0 !important;border-bottom:1px solid rgb(200,200,200);">Score Settings</h4>

				<table class="form-table">


				<tr valign="top">
				<th scope="row"><label for="onpageseo_options[keyword_density_minimum]">Keyword Density</label><br /><span style="font-size:12px;"><em>Minimum Score</em></span></th>
				<td><input name="onpageseo_options[keyword_density_minimum]" type="text" id="onpageseo_options[keyword_density_minimum]" value="<?php echo $this->options['keyword_density_minimum'];?>" class="small-text" />%</td>
				</tr>

				<tr valign="top" style="background-color:rgb(238,238,238);">
				<th scope="row"><label for="onpageseo_options[keyword_density_maximum]">Keyword Density</label><br /><span style="font-size:12px;"><em>Maximum Score</em></span></th>
				<td> <input name="onpageseo_options[keyword_density_maximum]" type="text" id="onpageseo_options[keyword_density_maximum]" value="<?php echo $this->options['keyword_density_maximum'];?>" class="small-text" />%</td>
				</tr>


				<tr valign="top">
				
				<th scope="row"><label for="onpageseo_options[keyword_density_formula]">Keyword Density</label><br /><span style="font-size:12px;"><em>Formula</em></span></th>
				<td><input type="radio" name="onpageseo_options[keyword_density_formula]" value="1"<?php checked($this->options['keyword_density_formula'], '1');?> /> <label title="keyword_density1">(Total Keywords / Total Words) * 100</label><br />
					<input type="radio" name="onpageseo_options[keyword_density_formula]" value="2"<?php checked($this->options['keyword_density_formula'], '2');?> /> <label title="keyword_density2">(Total Keywords / (Total Words / Total Words In Keyword Phrase)) * 100</label>
				</td>
				</tr>

				<tr valign="top" style="background-color:rgb(238,238,238);">
				<th scope="row"><label for="onpageseo_options[keyword_density_type]">Keyword Density</label><br /><span style="font-size:12px;"><em>Content To Analyze For Keyword Density Score</em></span></th>
				<td><select name="onpageseo_options[keyword_density_type]" id="onpageseo_options[keyword_density_type]">

					<option value="post" label="Post/Page Content"<?php selected($this->options['keyword_density_type'], "post");?>>Post/Page Content</option>
					<option value="full" label="Entire Document"<?php selected($this->options['keyword_density_type'], "full");?>>Entire Document</option>

					</select></td>

				</tr>


				<tr valign="top">
				<th scope="row"><label for="onpageseo_options[description_meta_tag_maximum]">Description Meta Tag</label><br /><span style="font-size:12px;"><em>Maximum Characters</em></span></th>
				<td><input name="onpageseo_options[description_meta_tag_maximum]" type="text" id="onpageseo_options[description_meta_tag_maximum]" value="<?php echo $this->options['description_meta_tag_maximum'];?>" class="small-text" /> characters</td>
				</tr>

				<tr valign="top" style="background-color:rgb(238,238,238);">
				<th scope="row"><label for="onpageseo_options[post_content_length]">Post Content</label><br /><span style="font-size:12px;"><em>Minimum Words</em></span></th>
				<td><input name="onpageseo_options[post_content_length]" type="text" id="onpageseo_options[post_content_length]" value="<?php echo $this->options['post_content_length'];?>" class="small-text" /> words</td>
				</tr>

				<tr valign="top">
				<th scope="row"><label for="onpageseo_options[title_length_minimum]">Title Length</label><br /><span style="font-size:12px;"><em>Minimum Words</em></span></th>
				<td><input name="onpageseo_options[title_length_minimum]" type="text" id="onpageseo_options[title_length_minimum]" value="<?php echo $this->options['title_length_minimum'];?>" class="small-text" /> words</td>
				</tr>

				<tr valign="top" style="background-color:rgb(238,238,238);">
				<th scope="row"><label for="onpageseo_options[title_length_maximum]">Title Length</label><br /><span style="font-size:12px;"><em>Maximum Characters</em></span></th>
				<td><input name="onpageseo_options[title_length_maximum]" type="text" id="onpageseo_options[title_length_maximum]" value="<?php echo $this->options['title_length_maximum'];?>" class="small-text" /> characters</td>
				</tr>

				</table>


				<h4 style="padding:10px 0 5px 0 !important;margin:15px 0 10px 0 !important;border-bottom:1px solid rgb(200,200,200);">Timeout Settings</h4>

				<table class="form-table">

				<tr valign="top">
				<th scope="row"><label for="onpageseo_options[request_timeout]">Request Timeout</label><br /><span style="font-size:12px;"><em>The Number of Seconds to Wait for a Connection (Set to a Higher Number if You Experience "Timeout" Errors)</em></span></th>
				<td><input name="onpageseo_options[request_timeout]" type="text" id="onpageseo_options[request_timeout]" value="<?php echo $this->options['request_timeout'];?>" class="small-text" /> seconds</td>
				</tr>

				</table>


				<h4 style="padding:10px 0 5px 0 !important;margin:15px 0 10px 0 !important;border-bottom:1px solid rgb(200,200,200);">Display Settings</h4>

				<table class="form-table">
				<tr valign="top" style="background-color:rgb(238,238,238);">
				<th scope="row"><label for="onpageseo_options[posts_per_page]">Posts Per Page</label><br /><span style="font-size:12px;"><em>Number Of Posts/Pages To Display Per Page</em></span></th>
				<td><input name="onpageseo_options[posts_per_page]" type="text" id="onpageseo_options[posts_per_page]" value="<?php echo $this->options['posts_per_page'];?>" class="small-text" /> posts</td>
				</tr>
				</table>


				<h4 style="padding:10px 0 5px 0 !important;margin:15px 0 10px 0 !important;border-bottom:1px solid rgb(200,200,200);">Internal Links</h4>

				<table class="form-table">
				<tr valign="top">
				<th scope="row"><label for="onpageseo_options[internal_links_posts_per_page]">Maximum Number Of Posts To Display</label><br /><span style="font-size:12px;"><em>Maximum Number Of Internal Posts/Pages To Display</em></span></th>
				<td><input name="onpageseo_options[internal_links_posts_per_page]" type="text" id="onpageseo_options[internal_links_posts_per_page]" value="<?php echo $this->options['internal_links_posts_per_page'];?>" class="small-text" /> posts</td>
				</tr>
				</table>

				<h4 style="padding:10px 0 5px 0 !important;margin:15px 0 10px 0 !important;border-bottom:1px solid rgb(200,200,200);">Internal Images</h4>

				<table class="form-table">
				<tr valign="top" style="background-color:rgb(238,238,238);">
				<th scope="row"><label for="onpageseo_options[internal_images_per_page]">Maximum Number Of Images To Display</label><br /><span style="font-size:12px;"><em>Maximum Number Of Internal Images To Display</em></span></th>
				<td><input name="onpageseo_options[internal_images_per_page]" type="text" id="onpageseo_options[internal_images_per_page]" value="<?php echo $this->options['internal_images_per_page'];?>" class="small-text" /> images</td>
				</tr>
				</table>


				<h4 style="padding:10px 0 5px 0 !important;margin:15px 0 10px 0 !important;border-bottom:1px solid rgb(200,200,200);">Posts/Pages Columns</h4>

				<table class="form-table">

				<tr valign="top">
				<th scope="row"><label for="onpageseo_options[posts_columns_score]">Add Columns To Posts/Pages</label></th>
				<td><input type="checkbox" name="onpageseo_options[posts_columns_score]" value="1"<?php checked($this->options['posts_columns_score'], 1);?> /> <label for="onpageseo_options[posts_columns_score]">On-Page SEO Score</label><br />
				<input type="checkbox" name="onpageseo_options[posts_columns_keyword]" value="1"<?php checked($this->options['posts_columns_keyword'], 1);?> /> <label for="onpageseo_options[posts_columns_keyword]">Primary Keyword</label><br />
				<input type="checkbox" name="onpageseo_options[posts_columns_kw_density]" value="1"<?php checked($this->options['posts_columns_kw_density'], 1);?> /> <label for="onpageseo_options[posts_columns_kw_density]">Keyword Density</label></td>
				</tr>

				</table>


			</div>

			<div id="opseo-keyword" class="opseo-tabs-panel">

				<p>Here's how to adjust the settings for the Latent Semantic Indexing (LSI) feature:</p>

				<h4 style="padding:10px 0 5px 0 !important;margin:15px 0 10px 0 !important;border-bottom:1px solid rgb(200,200,200);">LSI Keyword Settings</h4>

				<table class="form-table">

				<tr valign="top">
				<th scope="row"><label for="onpageseo_options[lsi_keyword_region_bing]">Default Region/Language</label><br /><span style="font-size:12px;"><em>Default Search</em></span></th>
				<td><select name="onpageseo_options[lsi_keyword_region_bing]" id="onpageseo_options[lsi_keyword_region_bing]">

					<option value="ar-XA" label="Arabia (Arabic)"<?php selected($this->options['lsi_keyword_region_bing'], "ar-XA");?>>Arabia (Arabic)</option>
					<option value="bg-BG" label="Bulgaria (Bulgarian)"<?php selected($this->options['lsi_keyword_region_bing'], "bg-BG");?>>Bulgaria (Bulgarian)</option>
					<option value="cs-CZ" label="Czech Republic (Czech)"<?php selected($this->options['lsi_keyword_region_bing'], "cs-CZ");?>>Czech Republic (Czech)</option>
					<option value="da-DK" label="Denmark (Danish)"<?php selected($this->options['lsi_keyword_region_bing'], "da-DK");?>>Denmark (Danish)</option>
					<option value="de-AT" label="Austria (German)"<?php selected($this->options['lsi_keyword_region_bing'], "de-AT");?>>Austria (German)</option>
					<option value="de-CH" label="Switzerland (German)"<?php selected($this->options['lsi_keyword_region_bing'], "de-CH");?>>Switzerland (German)</option>
					<option value="de-DE" label="Germany (German)"<?php selected($this->options['lsi_keyword_region_bing'], "de-DE");?>>Germany (German)</option>
					<option value="el-GR" label="Greece (Greek)"<?php selected($this->options['lsi_keyword_region_bing'], "el-GR");?>>Greece (Greek)</option>
					<option value="en-AU" label="Australia (English)"<?php selected($this->options['lsi_keyword_region_bing'], "en-AU");?>>Australia (English)</option>
					<option value="en-CA" label="Canada (English)"<?php selected($this->options['lsi_keyword_region_bing'], "en-CA");?>>Canada (English)</option>
					<option value="en-AU" label="Australia (English)"<?php selected($this->options['lsi_keyword_region_bing'], "en-AU");?>>Australia (English)</option>					<option value="en-GB" label="United Kingdom (English)"<?php selected($this->options['lsi_keyword_region_bing'], "en-GB");?>>United Kingdom (English)</option>
					<option value="en-ID" label="Indonesia (English)"<?php selected($this->options['lsi_keyword_region_bing'], "en-ID");?>>Indonesia (English)</option>					<option value="en-IE" label="Ireland (English)"<?php selected($this->options['lsi_keyword_region_bing'], "en-IE");?>>Ireland (English)</option>					<option value="en-IN" label="India (English)"<?php selected($this->options['lsi_keyword_region_bing'], "en-IN");?>>India (English)</option>					<option value="en-MY" label="Malaysia (English)"<?php selected($this->options['lsi_keyword_region_bing'], "en-MY");?>>Malaysia (English)</option>					<option value="en-NZ" label="New Zealand (English)"<?php selected($this->options['lsi_keyword_region_bing'], "en-NZ");?>>New Zealand (English)</option>					<option value="en-PH" label="Philippines (English)"<?php selected($this->options['lsi_keyword_region_bing'], "en-PH");?>>Philippines (English)</option>					<option value="en-SG" label="Singapore (English)"<?php selected($this->options['lsi_keyword_region_bing'], "en-SG");?>>Singapore (English)</option>					<option value="en-US" label="United States (English)"<?php selected($this->options['lsi_keyword_region_bing'], "en-US");?>>United States (English)</option>					<option value="en-XA" label="Arabia (English)"<?php selected($this->options['lsi_keyword_region_bing'], "en-XA");?>>Arabia (English)</option>					<option value="en-ZA" label="South Africa (English)"<?php selected($this->options['lsi_keyword_region_bing'], "en-ZA");?>>South Africa (English)</option>
					<option value="es-AR" label="Argentina (Spanish)"<?php selected($this->options['lsi_keyword_region_bing'], "es-AR");?>>Argentina (Spanish)</option>
					<option value="es-CL" label="Chile (Spanish)"<?php selected($this->options['lsi_keyword_region_bing'], "es-CL");?>>Chile (Spanish)</option>
					<option value="es-ES" label="Spain (Spanish)"<?php selected($this->options['lsi_keyword_region_bing'], "es-ES");?>>Spain (Spanish)</option>					<option value="es-MX" label="Mexico (Spanish)"<?php selected($this->options['lsi_keyword_region_bing'], "es-MX");?>>Mexico (Spanish)</option>					<option value="es-US" label="United States (Spanish)"<?php selected($this->options['lsi_keyword_region_bing'], "es-US");?>>United States (Spanish)</option>					<option value="es-XL" label="Latin America (Spanish)"<?php selected($this->options['lsi_keyword_region_bing'], "es-XL");?>>Latin America (Spanish)</option>
					<option value="et-EE" label="Estonia (Estonian)"<?php selected($this->options['lsi_keyword_region_bing'], "et-EE");?>>Estonia (Estonian)</option>
					<option value="fi-FI" label="Finland (Finnish)"<?php selected($this->options['lsi_keyword_region_bing'], "fi-FI");?>>Finland (Finish)</option>
					<option value="fr-BE" label="Belgium (French)"<?php selected($this->options['lsi_keyword_region_bing'], "fr-BE");?>>Belgium (French)</option>
					<option value="fr-CA" label="Canada (French)"<?php selected($this->options['lsi_keyword_region_bing'], "fr-CA");?>>Canada (French)</option>					<option value="fr-CH" label="Switzerland (French)"<?php selected($this->options['lsi_keyword_region_bing'], "fr-CH");?>>Switzerland (French)</option>					<option value="fr-FR" label="France (French)"<?php selected($this->options['lsi_keyword_region_bing'], "fr-FR");?>>France (French)</option>
					<option value="he-IL" label="Israel (Hebrew)"<?php selected($this->options['lsi_keyword_region_bing'], "he-IL");?>>Israel (Hebrew)</option>
					<option value="hr-HR" label="Croatia (Croatian)"<?php selected($this->options['lsi_keyword_region_bing'], "hr-HR");?>>Croatia (Croatian)</option>
					<option value="hu-HU" label="Hungary (Hungarian)"<?php selected($this->options['lsi_keyword_region_bing'], "hu-HU");?>>Hungary (Hungarian)</option>
					<option value="it-IT" label="Italy (Italian)"<?php selected($this->options['lsi_keyword_region_bing'], "it-IT");?>>Italy (Italian)</option>
					<option value="ja-JP" label="Japan (Japanese)"<?php selected($this->options['lsi_keyword_region_bing'], "ja-JP");?>>Japan (Japanese)</option>
					<option value="ko-KR" label="Korea (Korean)"<?php selected($this->options['lsi_keyword_region_bing'], "ko-KR");?>>Korea (Korean)</option>
					<option value="lt-LT" label="Lithuania (Lithuanian)"<?php selected($this->options['lsi_keyword_region_bing'], "lt-LT");?>>Lithuania (Lithuanian)</option>
					<option value="lv-LV" label="Latvia (Latvian)"<?php selected($this->options['lsi_keyword_region_bing'], "lv-LV");?>>Latvia (Latvian)</option>
					<option value="nb-NO" label="Norway (Norwegian)"<?php selected($this->options['lsi_keyword_region_bing'], "nb-NO");?>>Norway (Norwegian)</option>
					<option value="nl-BE" label="Belgium (Dutch)"<?php selected($this->options['lsi_keyword_region_bing'], "nl-BE");?>>Belgium (Dutch)</option>
					<option value="nl-NL" label="Netherlands (Dutch)"<?php selected($this->options['lsi_keyword_region_bing'], "nl-NL");?>>Netherlands (Dutch)</option>
					<option value="pl-PL" label="Poland (Polish)"<?php selected($this->options['lsi_keyword_region_bing'], "pl-PL");?>>Poland (Polish)</option>
					<option value="pt-BR" label="Brazil (Portuguese)"<?php selected($this->options['lsi_keyword_region_bing'], "pt-BR");?>>Brazil (Portuguese)</option>
					<option value="pt-PT" label="Portugal (Portuguese)"<?php selected($this->options['lsi_keyword_region_bing'], "pt-PT");?>>Portugal (Portuguese)</option>
					<option value="ro-RO" label="Romania (Romanian)"<?php selected($this->options['lsi_keyword_region_bing'], "ro-RO");?>>Romania (Romanian)</option>
					<option value="ru-RU" label="Russia (Russian)"<?php selected($this->options['lsi_keyword_region_bing'], "ru-RU");?>>Russia (Russian)</option>
					<option value="sk-SK" label="Slovak Republic (Slovak)"<?php selected($this->options['lsi_keyword_region_bing'], "sk-SK");?>>Slovak Republic (Slovak)</option>
					<option value="sl-SL" label="Slovenia (Slovenian)"<?php selected($this->options['lsi_keyword_region_bing'], "sl-SL");?>>Slovenia (Slovenian)</option>
					<option value="sv-SE" label="Sweden (Swedish)"<?php selected($this->options['lsi_keyword_region_bing'], "sv-SE");?>>Sweden (Swedish)</option>
					<option value="th-TH" label="Thailand (Thai)"<?php selected($this->options['lsi_keyword_region_bing'], "th-TH");?>>Thailand (Thai)</option>
					<option value="tr-TR" label="Turkey (Turkish)"<?php selected($this->options['lsi_keyword_region_bing'], "tr-TR");?>>Turkey (Turkish)</option>
					<option value="uk-UA" label="Ukraine (Ukrainian)"<?php selected($this->options['lsi_keyword_region_bing'], "uk-UA");?>>Ukraine (Ukrainian)</option>
					<option value="zh-CN" label="China (Chinese)"<?php selected($this->options['lsi_keyword_region_bing'], "zh-CN");?>>China (Chinese)</option>
					<option value="zh-HK" label="Hong Kong SAR (Chinese)"<?php selected($this->options['lsi_keyword_region_bing'], "zh-HK");?>>Hong Kong SAR (Chinese)</option>
					<option value="zh-TW" label="Taiwan (Chinese)"<?php selected($this->options['lsi_keyword_region_bing'], "zh-TW");?>>Taiwan (Chinese)</option>

					</select></td>

				</tr>



				</table>

			</div>


			<div id="opseo-password-protection" class="opseo-tabs-panel">

				<p style="background-color:rgb(255,235,232);border:1px solid rgb(204,0,0);padding:10px;"><strong>IMPORTANT:</strong> Make sure you change the permissions of the cookie file to make it writable to Wordpress, but unreadable to the public. I recommend you chmod the file to 700.</p>

				<p>Is this a membership site? Do you want to analyze content that is password-protected? Is your site in "maintenance" mode? If so, enter your Administrator account information to grant Easy WP SEO access to all protected content:</p>

				<h4 style="padding:10px 0 5px 0 !important;margin:15px 0 10px 0 !important;border-bottom:1px solid rgb(200,200,200);">Password Protection</h4>

				<table class="form-table">

				<tr valign="top" style="background-color:rgb(238,238,238);">
				<th scope="row"><label for="onpageseo_options[password_activation]">Activate/Deactivate</label></th>
				<td>
					<input type="radio" name="onpageseo_options[password_activation]" value="activated"<?php checked($this->options['password_activation'], 'activated');?> /> <label title="activated">Activated</label><br />
					<input type="radio" name="onpageseo_options[password_activation]" value="deactivated"<?php checked($this->options['password_activation'], 'deactivated');?> /> <label title="deactivated">Deactivated</label>
				</td>
				</tr>

				</table>

				<h4 style="padding:10px 0 5px 0 !important;margin:15px 0 10px 0 !important;border-bottom:1px solid rgb(200,200,200);">Access Information</h4>

				<table class="form-table">

				<tr valign="top">
				<th scope="row"><label for="onpageseo_options[password_username]">Administrator Username</label></th>
				<td><input type="text" name="onpageseo_options[password_username]" value="<?php echo $this->options['password_username'];?>" class="large-text" /><br /><em>Enter your Wordpress Administrator username.</em></td>
				</tr>

				<tr valign="top" style="background-color:rgb(238,238,238);">
				<th scope="row"><label for="onpageseo_options[password_password]">Administrator Password</label></th>
				<td><input type="text" name="onpageseo_options[password_password]" value="<?php echo $this->options['password_password'];?>" class="large-text" /><br /><em>Enter your Wordpress Administrator password.</em></td>
				</tr>

				</table>

				<h4 style="padding:10px 0 5px 0 !important;margin:15px 0 10px 0 !important;border-bottom:1px solid rgb(200,200,200);">cURL Cookie Settings</h4>

				<table class="form-table">

				<tr valign="top" style="background-color:rgb(238,238,238);">
				<th scope="row"><label for="onpageseo_options[password_file_path]">Cookie File Location </label><?php if(is_file($this->options['password_file_path']) && ($this->pwProtectionLoggedIn || (filesize($this->options['password_file_path']) > 0))) { echo '<br /><em><span style="color:green;font-size:12px;">The cookie file already contains your access information!</span></em>'; }?></em>
				<td><input type="text" name="onpageseo_options[password_file_path]" value="<?php echo $this->options['password_file_path'];?>" class="large-text" /><br /><em>cURL will save your cookie information in this file.</em></td>
				</tr>

				</table>

			</div>

			<div id="opseo-copyscape" class="opseo-tabs-panel">

				<p>Enter your Copyscape Premium account information:</p>

				<h4 style="padding:10px 0 5px 0 !important;margin:15px 0 10px 0 !important;border-bottom:1px solid rgb(200,200,200);">Access Information</h4>

				<table class="form-table">

				<tr valign="top">
				<th scope="row"><label for="onpageseo_options[copyscape_username]">Copyscape Username</label></th>
				<td><input type="text" name="onpageseo_options[copyscape_username]" value="<?php echo $this->options['copyscape_username'];?>" class="large-text" /><br /><em>Enter your Copyscape Premium username.</em></td>
				</tr>

				<tr valign="top" style="background-color:rgb(238,238,238);">
				<th scope="row"><label for="onpageseo_options[copyscape_api_key]">Copyscape API Key</label></th>
				<td><input type="text" name="onpageseo_options[copyscape_api_key]" value="<?php echo $this->options['copyscape_api_key'];?>" class="large-text" /><br /><a href="http://www.copyscape.com" target="_blank">Create a Copyscape Premium account.</a></td>
				</tr>

				</table>

				<h4 style="padding:10px 0 5px 0 !important;margin:15px 0 10px 0 !important;border-bottom:1px solid rgb(200,200,200);">Copyscape Settings</h4>

				<table class="form-table">

				<tr valign="top">
				<th scope="row"><label for="onpageseo_options[copyscape_role]">Minimum User Role</label><br /><span style="font-size:12px;"><em>Required To Access Copyscape</em></span></th>
				<td><select name="onpageseo_options[copyscape_role]" id="onpageseo_options[copyscape_role]">

					<option value="administrator" label="Administrator"<?php selected($this->options['copyscape_role'], "administrator");?>>Administrator</option>
					<option value="editor" label="Editor"<?php selected($this->options['copyscape_role'], "editor");?>>Editor</option>
					<option value="author" label="Author"<?php selected($this->options['copyscape_role'], "author");?>>Author</option>
					<option value="contributor" label="Contributor"<?php selected($this->options['copyscape_role'], "contributor");?>>Contributor</option>
					<option value="subscriber" label="Subscriber"<?php selected($this->options['copyscape_role'], "subscriber");?>>Subscriber</option>

					</select></td>

				</tr>


				<tr valign="top" style="background-color:rgb(238,238,238);">
				<th scope="row"><label for="onpageseo_options[copyscape_confirm]">Confirm Each Copyscape Search </label></th>
				<td><input type="checkbox" name="onpageseo_options[copyscape_confirm]" value="1"<?php checked($this->options['copyscape_confirm'], 1);?> /> <label for="onpageseo_options[copyscape_confirm]">Must click "OK" button on confirmation box before every Copyscape search.</label></td>
				</tr>

				</table>

			</div>

			<div id="opseo-license" class="opseo-tabs-panel"<?php if($this->licenseHide){ echo ' style="display:none;"';}?>>
			
				<?php include_once('admin-license-form.php');?>

			</div>

		</div>

		<div class="opseo-submit">
			<input type="hidden" name="onpageseo_options[old_license_email]" value="<?php echo $this->options['license_email'];?>" />
			<input type="hidden" name="onpageseo_options[old_license_serial]" value="<?php echo $this->options['license_serial'];?>" />
			<input type="submit" name="Submit" class="button-primary" value="Save Changes" />
		</div>

	</form>


		<?php if(!$this->license->isLicenseError()){$this->licenseFooter();}?>


	</div>





<script type="text/javascript">

jQuery(document).ready(function() {

	var opseoTabs = jQuery('#opseo-tabs li.opseotabs');
	var opseoTabsContents = jQuery('.opseo-tabs-panel');

	jQuery(function(){
		opseoTabsContents.hide(); //hide all contents
		opseoTabs.removeClass('opseo-tabs-selected');
		jQuery(opseoTabsContents[0]).show();
		jQuery(opseoTabs[0]).addClass('opseo-tabs-selected');
	});

	opseoTabs.bind('click',function() {
		opseoTabsContents.hide(); //hide all contents
		opseoTabs.removeClass('opseo-tabs-selected');
		jQuery(opseoTabsContents[jQuery('#opseo-tabs li.opseotabs').index(this)]).show();
		jQuery(this).addClass('opseo-tabs-selected');

		return false;
	});

	var opseoImportExportTabs = jQuery('#opseo-import-export-tabs li.importexporttabs');
	var opseoImportExportTabsContents = jQuery('.import-export-tabs-panel');

	jQuery(function(){
		opseoImportExportTabsContents.hide(); //hide all contents
		opseoImportExportTabs.removeClass('opseo-tabs-selected');
		jQuery(opseoImportExportTabsContents[0]).show();
		jQuery(opseoImportExportTabs[0]).addClass('opseo-tabs-selected');
	});

	opseoImportExportTabs.bind('click',function() {
		opseoImportExportTabsContents.hide(); //hide all contents
		opseoImportExportTabs.removeClass('opseo-tabs-selected');
		jQuery(opseoImportExportTabsContents[jQuery('#opseo-import-export-tabs li.importexporttabs').index(this)]).show();
		jQuery(this).addClass('opseo-tabs-selected');

		return false;
	});
});

</script>