<?php
			global $pagenow;

			echo '<div id="suggestion-tabs">';

			echo '<ul>
					<li class="suggestiontabs" style="margin-bottom:0 !important;padding-bottom:0 !important;"><a href="#suggestion-1" style="font-weight:normal !important;">Title</a></li><li class="suggestiontabs" style="margin-bottom:0 !important;padding-bottom:0 !important;margin-left:2px !important;"><a href="#suggestion-2" style="font-weight:normal !important;">URL</a></li><li class="suggestiontabs" style="margin-bottom:0 !important;padding-bottom:0 !important;margin-left:2px !important;"><a href="#suggestion-3" style="font-weight:normal !important;">Meta</a></li><li class="suggestiontabs" style="margin-bottom:0 !important;padding-bottom:0 !important;margin-left:2px !important;"><a href="#suggestion-4" style="font-weight:normal !important;">Heading</a></li><li class="suggestiontabs" style="margin-bottom:0 !important;padding-bottom:0 !important;margin-left:2px !important;"><a href="#suggestion-5" style="font-weight:normal !important;">Content</a></li>
				</ul>';


			echo '<div id="suggestion-1" class="suggestion-tabs-panel">';
				$factorExists = 1;
				echo '<ol>';

				// Title contains keyword.
				if(isset($this->options['title_factor']))
				{
					echo '<li id="opseokeywordtitle" class="onpageseoscorelifalse"><p>Title contains keyword. <a href="'.OPSEO_PLUGIN_URL.'/templates/admin-video.php?vidid=Vi8NBTwu1b8&#038;TB_iframe=1" class="thickbox" title="Add Your Keyword to the Title"><img src="'.OPSEO_PLUGIN_URL.'/images/help.png" alt="Help" title="Help" /></a></p></li>';
					$factorExists = 0;
				}

				// Title begins with keyword.
				if(isset($this->options['title_beginning_factor']))
				{
					echo '<li id="opseokeywordtitlebeginning" class="onpageseoscorelifalse"><p>Title begins with keyword. <a href="'.OPSEO_PLUGIN_URL.'/templates/admin-video.php?vidid=Vi8NBTwu1b8&#038;TB_iframe=1" class="thickbox" title="Add Your Keyword to the Title"><img src="'.OPSEO_PLUGIN_URL.'/images/help.png" alt="Help" title="Help" /></a></p></li>';
					$factorExists = 0;
				}

				// Title contains at least # words.
				if(isset($this->options['title_words_factor']))
				{
					echo '<li id="opseotitlewords" class="onpageseoscorelifalse"><p>Title contains at least '.$this->options['title_length_minimum'].' words. <a href="'.OPSEO_PLUGIN_URL.'/templates/admin-video.php?vidid=Vi8NBTwu1b8&#038;TB_iframe=1" class="thickbox" title="Add Your Keyword to the Title"><img src="'.OPSEO_PLUGIN_URL.'/images/help.png" alt="Help" title="Help" /></a></p></li>';
					$factorExists = 0;
				}

				// Title contains at least # characters.
				if(isset($this->options['title_characters_factor']))
				{
					echo '<li id="opseotitlechars" class="onpageseoscorelifalse" style="margin-bottom:0 !important;padding-bottom:0 !important;"><p>Title contains up to '.$this->options['title_length_maximum'].' characters. <a href="'.OPSEO_PLUGIN_URL.'/templates/admin-video.php?vidid=Vi8NBTwu1b8&#038;TB_iframe=1" class="thickbox" title="Add Your Keyword to the Title"><img src="'.OPSEO_PLUGIN_URL.'/images/help.png" alt="Help" title="Help" /></a></p></li>';
					$factorExists = 0;
				}

				echo '</ol>';

				if($factorExists)
				{
					echo '<p style="font-weight:normal !important;margin:0 !important;padding:0 !important;">Sorry, but all of the "Title" factors are deactivated.</p>';
				}

			echo '</div>';

			echo '<div id="suggestion-2" class="suggestion-tabs-panel">';
				$factorExists = 1;
				echo '<ol>';

				// Permalink contains keyword.
				if(isset($this->options['url_factor']))
				{
					echo '<li id="opseopermalink" class="onpageseoscorelifalse" style="margin-bottom:0 !important;padding-bottom:0 !important;"><p>Permalink contains your keyword. <a href="'.OPSEO_PLUGIN_URL.'/templates/admin-video.php?vidid=QpONHRDwboA&#038;TB_iframe=1" class="thickbox" title="Add Your Keyword to the Permalink"><img src="'.OPSEO_PLUGIN_URL.'/images/help.png" alt="Help" title="Help" /></a></p></li>';
					$factorExists = 0;
				}

				echo '</ol>';

				if($factorExists)
				{
					echo '<p style="font-weight:normal !important;margin:0 !important;padding:0 !important;">Sorry, but all of the "URL" factors are deactivated.</p>';
				}

			echo '</div>';

			echo '<div id="suggestion-3" class="suggestion-tabs-panel">';
				$factorExists = 1;
				echo '<ol>';

				// Description meta tag contains keyword.
				if(isset($this->options['description_meta_factor']))
				{
					echo '<li id="opseodescriptionmetatag" class="onpageseoscorelifalse"><p>Description meta tag contains keyword. <a href="'.OPSEO_PLUGIN_URL.'/templates/admin-video.php?vidid=MEdkvOrYzuM&#038;TB_iframe=1" class="thickbox" title="Add Your Keyword to the Description Meta Tag"><img src="'.OPSEO_PLUGIN_URL.'/images/help.png" alt="Help" title="Help" /></a></p></li>';
					$factorExists = 0;
				}

				// Description meta tag contains up to # characters.
				if(isset($this->options['description_chars_meta_factor']))
				{
					echo '<li id="opseometataglength" class="onpageseoscorelifalse"><p>Description meta tag contains up to '.$this->options['description_meta_tag_maximum'].' characters. <a href="'.OPSEO_PLUGIN_URL.'/templates/admin-video.php?vidid=MEdkvOrYzuM&#038;TB_iframe=1" class="thickbox" title="Add Your Keyword to the Description Meta Tag"><img src="'.OPSEO_PLUGIN_URL.'/images/help.png" alt="Help" title="Help" /></a></p></li>';
					$factorExists = 0;
				}

				// Description meta tag begins with keyword.
				if(isset($this->options['description_beginning_meta_factor']))
				{
					echo '<li id="opseometatagbeginning" class="onpageseoscorelifalse"><p>Description meta tag begins with keyword. <a href="'.OPSEO_PLUGIN_URL.'/templates/admin-video.php?vidid=MEdkvOrYzuM&#038;TB_iframe=1" class="thickbox" title="Add Your Keyword to the Description Meta Tag"><img src="'.OPSEO_PLUGIN_URL.'/images/help.png" alt="Help" title="Help" /></a></p></li>';
					$factorExists = 0;
				}

				// Keywords meta tag contains keyword.
				if(isset($this->options['keywords_meta_factor']))
				{
					echo '<li id="opseokeywordsmetatag" class="onpageseoscorelifalse" style="margin-bottom:0 !important;padding-bottom:0 !important;"><p>Keywords meta tag contains keyword. <a href="'.OPSEO_PLUGIN_URL.'/templates/admin-video.php?vidid=MEdkvOrYzuM&#038;TB_iframe=1" class="thickbox" title="Add Your Keyword to the Keywords Meta Tag"><img src="'.OPSEO_PLUGIN_URL.'/images/help.png" alt="Help" title="Help" /></a></p></li>';
					$factorExists = 0;
				}

				echo '</ol>';

				if($factorExists)
				{
					echo '<p style="font-weight:normal !important;margin:0 !important;padding:0 !important;">Sorry, but all of the "Meta" factors are deactivated.</p>';
				}

			echo '</div>';

			echo '<div id="suggestion-4" class="suggestion-tabs-panel">';
				$factorExists = 1;
				echo '<ol>';

				// H1 tag contains keyword.
				if(isset($this->options['h1_factor']))
				{
					echo '<li id="opseoh1" class="onpageseoscorelifalse"><p>H1 tag contains your keyword. <a href="'.OPSEO_PLUGIN_URL.'/templates/admin-video.php?vidid=2q3yHa64c3Y&#038;TB_iframe=1" class="thickbox" title="Add Your Keyword to an H1 Tag"><img src="'.OPSEO_PLUGIN_URL.'/images/help.png" alt="Help" title="Help" /></a></p></li>';
					$factorExists = 0;
				}

				// H1 tag begins with keyword.
				if(isset($this->options['h1_beginning_factor']))
				{
					echo '<li id="opseoh1beginning" class="onpageseoscorelifalse"><p>H1 tag begins with keyword. <a href="'.OPSEO_PLUGIN_URL.'/templates/admin-video.php?vidid=2q3yHa64c3Y&#038;TB_iframe=1" class="thickbox" title="Add Your Keyword to an H1 Tag"><img src="'.OPSEO_PLUGIN_URL.'/images/help.png" alt="Help" title="Help" /></a></p></li>';
					$factorExists = 0;
				}

				// H2 tag contains keyword.
				if(isset($this->options['h2_factor']))
				{
					echo '<li id="opseoh2" class="onpageseoscorelifalse"><p>H2 tag contains your keyword. <a href="'.OPSEO_PLUGIN_URL.'/templates/admin-video.php?vidid=zUMjLTFwqYo&#038;TB_iframe=1" class="thickbox" title="Add Your Keyword to an H2 Tag"><img src="'.OPSEO_PLUGIN_URL.'/images/help.png" alt="Help" title="Help" /></a></p></li>';
					$factorExists = 0;
				}

				// H3 tag contains keyword.
				if(isset($this->options['h3_factor']))
				{
					echo '<li id="opseoh3" class="onpageseoscorelifalse"><p>H3 tag contains your keyword. <a href="'.OPSEO_PLUGIN_URL.'/templates/admin-video.php?vidid=9xXLw704Of4&#038;TB_iframe=1" class="thickbox" title="Add Your Keyword to an H3 Tag"><img src="'.OPSEO_PLUGIN_URL.'/images/help.png" alt="Help" title="Help" /></a></p></li>';
					$factorExists = 0;
				}

				echo '</ol>';

				if($factorExists)
				{
					echo '<p style="font-weight:normal !important;margin:0 !important;padding:0 !important;">Sorry, but all of the "Heading" factors are deactivated.</p>';
				}

			echo '</div>';


			echo '<div id="suggestion-5" class="suggestion-tabs-panel">';
				$factorExists = 1;
				echo '<ol>';

				// Content contains at least # words.
				if(isset($this->options['content_words_factor']))
				{
					echo '<li id="opseopostwords" class="onpageseoscorelifalse"><p>Content contains at least '.$this->options['post_content_length'].' words. <a href="'.OPSEO_PLUGIN_URL.'/templates/admin-video.php?vidid=O4l1HU7JMuE&#038;TB_iframe=1" class="thickbox" title="Add the Minimum Words to Your Content"><img src="'.OPSEO_PLUGIN_URL.'/images/help.png" alt="Help" title="Help" /></a></p></li>';
					$factorExists = 0;
				}

				// Content has #-#% keyword density.
				if(isset($this->options['content_kw_density_factor']))
				{
					echo '<li id="opseokeyworddensity" class="onpageseoscorelifalse"><p>Content has '.$this->options['keyword_density_minimum'].'-'.$this->options['keyword_density_maximum'].'% keyword density. <a href="'.OPSEO_PLUGIN_URL.'/templates/admin-video.php?vidid=uPGt8XIxDGc&#038;TB_iframe=1" class="thickbox" title="Manage Your Keyword Densities"><img src="'.OPSEO_PLUGIN_URL.'/images/help.png" alt="Help" title="Help" /></a></p></li>';
					$factorExists = 0;
				}

				// Content contains keyword in first 50-100 words.
				if(isset($this->options['content_first_factor']))
				{
					echo '<li id="opseofirst100words" class="onpageseoscorelifalse"><p>Content contains keyword in the first 50-100 words. <a href="'.OPSEO_PLUGIN_URL.'/templates/admin-video.php?vidid=QoHrf-fIue4&#038;TB_iframe=1" class="thickbox" title="Add Your Keyword to the First 50-100 Words"><img src="'.OPSEO_PLUGIN_URL.'/images/help.png" alt="Help" title="Help" /></a></p></li>';
					$factorExists = 0;
				}

				// Content contains contains at least one image with keyword in ALT attribute.
				if(isset($this->options['content_alt_factor']))
				{
					echo '<li id="opseoimagealt" class="onpageseoscorelifalse"><p>Content contains at least one image with keyword in ALT attribute. <a href="'.OPSEO_PLUGIN_URL.'/templates/admin-video.php?vidid=fOaqqomIOT4&#038;TB_iframe=1" class="thickbox" title="Insert Your Keyword Into an Image ALT Attribute"><img src="'.OPSEO_PLUGIN_URL.'/images/help.png" alt="Help" title="Help" /></a></p></li>';
					$factorExists = 0;
				}

				// Content contains at least one bold keyword.
				if(isset($this->options['content_bold_factor']))
				{
					echo '<li id="opseobold" class="onpageseoscorelifalse"><p>Content contains at least one bold keyword. <a href="'.OPSEO_PLUGIN_URL.'/templates/admin-video.php?vidid=iSYxJeyyyIU&#038;TB_iframe=1" class="thickbox" title="Bold at Least One Keyword"><img src="'.OPSEO_PLUGIN_URL.'/images/help.png" alt="Help" title="Help" /></a></p></li>';
					$factorExists = 0;
				}

				// Content contains at least one italicized keyword.
				if(isset($this->options['content_italic_factor']))
				{
					echo '<li id="opseoitalic" class="onpageseoscorelifalse"><p>Content contains at least one italicized keyword. <a href="'.OPSEO_PLUGIN_URL.'/templates/admin-video.php?vidid=iSYxJeyyyIU&#038;TB_iframe=1" class="thickbox" title="Italicize at Least One Keyword"><img src="'.OPSEO_PLUGIN_URL.'/images/help.png" alt="Help" title="Help" /></a></p></li>';
					$factorExists = 0;
				}

				// Content contains at least one underlined keyword.
				if(isset($this->options['content_underline_factor']))
				{
					echo '<li id="opseounderline" class="onpageseoscorelifalse"><p>Content contains at least one underlined keyword. <a href="'.OPSEO_PLUGIN_URL.'/templates/admin-video.php?vidid=iSYxJeyyyIU&#038;TB_iframe=1" class="thickbox" title="Underline at Least One Keyword"><img src="'.OPSEO_PLUGIN_URL.'/images/help.png" alt="Help" title="Help" /></a></p></li>';
					$factorExists = 0;
				}

				// Content contains keyword in anchor text of at least one external link.
				if(isset($this->options['content_external_link_factor']))
				{
					echo '<li id="opseoexternalanchortext" class="onpageseoscorelifalse"><p>Content contains keyword in anchor text of at least one external link. <a href="'.OPSEO_PLUGIN_URL.'/templates/admin-video.php?vidid=GieCCQg7LJY&#038;TB_iframe=1" class="thickbox" title="Add Your Keyword as External Link Anchor Text"><img src="'.OPSEO_PLUGIN_URL.'/images/help.png" alt="Help" title="Help" /></a></p></li>';
					$factorExists = 0;
				}

				// Content contains keyword in anchor text of at least one internal link.
				if(isset($this->options['content_internal_link_factor']))
				{
					echo '<li id="opseointernalanchortext" class="onpageseoscorelifalse"><p>Content contains keyword in anchor text of at least one internal link. <a href="'.OPSEO_PLUGIN_URL.'/templates/admin-video.php?vidid=GieCCQg7LJY&#038;TB_iframe=1" class="thickbox" title="Add Your Keyword as Internal Link Anchor Text"><img src="'.OPSEO_PLUGIN_URL.'/images/help.png" alt="Help" title="Help" /></a></p></li>';
					$factorExists = 0;
				}

				// Content contains keyword in last 50-100 words.
				if(isset($this->options['content_last_factor']))
				{
					echo '<li id="opseolast100words" class="onpageseoscorelifalse"><p>Content contains keyword in the last 50-100 words. <a href="'.OPSEO_PLUGIN_URL.'/templates/admin-video.php?vidid=QoHrf-fIue4&#038;TB_iframe=1" class="thickbox" title="Add Your Keyword to the Last 50-100 Words"><img src="'.OPSEO_PLUGIN_URL.'/images/help.png" alt="Help" title="Help" /></a></p></li>';
					$factorExists = 0;
				}

				echo '</ol>';

				if($factorExists)
				{
					echo '<p style="font-weight:normal !important;margin:0 !important;padding:0 !important;">Sorry, but all of the "Content" factors are deactivated.</p>';
				}

			echo '</div>';

			echo '</div>';

			if(!empty($this->postMeta) && is_array($this->postMeta) && isset($this->postMeta['onpageseo_global_settings']))
			{
				echo '<div style="width:100% !important;text-align:center !important;margin:10px 0 0 0 !important;padding:0 !important;"><input type="button" class="button" value="Display SEO Report" title="Display SEO Report" id="display-seo-report-button" /></div>';

				echo '<div style="width:100% !important;text-align:center !important;margin:10px 0 0 0 !important;padding:0 !important;display:none !important;"><a class="button thickbox" title="Display SEO Report" id="display-seo-report" href="#?TB_inline=1&#038;inlineId=myOnPageContent&width=640&height=1024" onclick="return false;" style="font-weight:normal !important;"><strong>Display SEO Report</strong></a></div>';

				echo '<div id="onpageseoreportloader" style="position:relative !important;width:243px !important;text-align:center !important;padding:5px 0 10px 0 !important;font-size:10px !important;">Generating<br /><img src="'.OPSEO_PLUGIN_URL.'/images/ajax_spin.gif" alt="Loading" title="Loading" style="height:16px !important;width:16px !important;border:0 !important;padding-top:3px !important;" /></div>';

				echo '<input type="hidden" id="onpageseo-analyze-url" name="onpageseo-analyze-url" value="" />';

				// URL Analyzer
				if(false !== strpos($pagenow, 'admin'))
				{ 
					echo '<input type="hidden" id="onpageseo-post-id" name="onpageseo-post-id" value="'.$_REQUEST['id'].'" />';
					echo '<input type="hidden" id="onpageseo-analyze-type" name="onpageseo-analyze-type" value="2" />';
				}
				// Post or Page
				else
				{
					echo '<input type="hidden" id="onpageseo-post-id" name="onpageseo-post-id" value="'.$this->postID.'" />';
					echo '<input type="hidden" id="onpageseo-analyze-type" name="onpageseo-analyze-type" value="1" />';
				}

				echo '<div id="myOnPageContent" style="display:none;"></div>';
			}
?>
